
import com.zhiyun.hxzy.SpringBootApp;
import com.zhiyun.hxzy.dao.HomeWorkDao;
import com.zhiyun.hxzy.domain.Homework;
import com.zhiyun.hxzy.domain.Student;
import com.zhiyun.hxzy.service.HomeWorkService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootApp.class)
public class HomeworkTest {
    @Autowired
    private HomeWorkService homeWorkService;
    @Autowired
    private HomeWorkDao homeWorkDao;
    @Test
    public void find3(){
        System.out.println(homeWorkDao.findAll());
    }
}
