<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>学生详情</title>
    <meta name="description" content="订单详情">
    <meta name="keywords" content="订单详情">

    <!-- Tell the browser to be responsive to screen width -->
    <meta
            content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"
            name="viewport">

    <link rel=“stylesheet”
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/ionicons/css/ionicons.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/iCheck/square/blue.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/morris/morris.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/treeTable/jquery.treetable.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/treeTable/jquery.treetable.theme.default.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/select2/select2.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/adminLTE/css/AdminLTE.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/adminLTE/css/skins/_all-skins.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/css/style.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/ionslider/ion.rangeSlider.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/ionslider/ion.rangeSlider.skinNice.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap-slider/slider.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

    <!-- 页面头部 -->
    <jsp:include page="header.jsp"></jsp:include>
    <!-- 导航侧栏 -->
    <jsp:include page="sidebar.jsp"></jsp:include>
    <!-- 内容区域 -->
    <div class="content-wrapper">
        <!-- 正文区域 -->
        <section class="content"> <!--订单信息-->
            <!--游客信息/--> <!--联系人信息-->
            <div class="panel panel-default">
                <div class="panel-heading">学生信息</div>
                <div class="row data-type">
                    <div>头像</div>
                    <div><a href="${student.header}"></a></div>

                    <div class="col-md-2 title">学号</div>
                    <div class="col-md-4 data text">${student.id }</div>

                    <div class="col-md-2 title">姓名</div>
                    <div class="col-md-4 data text">${student.name}</div>

                    <div class="col-md-2 title">年龄</div>
                    <div class="col-md-4 data text">${student.age}</div>

                    <div class="col-md-2 title">性别</div>
                    <div class="col-md-4 data text">${student.sex}</div>

                    <div class="col-md-2 title">联系电话</div>
                    <div class="col-md-4 data text">${student.phoneNum}</div>

                    <div class="col-md-2 title">班级</div>
                    <div class="col-md-4 data text">${student.classes.className}</div>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">就业信息
                    <div class="btn-group">
                        <c:if test="${empty student.employment}">
                            <button type="button" class="btn btn-default" title="新建"
                                    onclick="location.href='${pageContext.request.contextPath}/admin/employment/transfer?id=${student.id}'">
                                <i class="fa fa-file-o"></i> 添加就业信息
                            </button>
                        </c:if>
                        <c:if test="${not empty student.employment}">
                            <button type="button" class="btn btn-default"
                                    onclick="location.href='${pageContext.request.contextPath}/admin/employment/update1?id=${student.id}'">
                                <i class="fa fa-file-o"></i> 修改就业信息
                            </button>
                            <button type="button" class="btn btn-default"
                                    onclick="javascript:delEmplayment(${student.id})">
                                <i class="fa fa-file-o"></i> 删除就业信息
                            </button>
                            <button type="button" class="btn btn-default"
                                    onclick="location.href='${pageContext.request.contextPath}/admin/employment/toEmploymentStar?id=${student.id}'">
                                <i class="fa fa-file-o"></i> 设置/取消就业明星
                            </button>
                        </c:if>
                    </div>
                </div>
                <c:if test="${not empty student.employment}">
                    <!--数据列表-->
                    <table id="dataList1" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                        <tr>
                            <th>头像</th>
                            <th>职位</th>
                            <th>公司</th>
                            <th>描述</th>
                            <th>是否就业明星</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><img src="${student.employment.header}" height="40px" width="40px"></td>
                            <td>${student.employment.position}</td>
                            <td>${student.employment.company}</td>
                            <td>${student.employment.workDesc}</td>
                            <td>${student.employment.star ? "是":"否"}</td>
                        </tr>
                        </tbody>
                    </table>
                </c:if>
                <!--数据列表/-->
            </div>


            <div class="panel panel-default">
                <div class="panel-heading">作业</div>
                <!--数据列表-->
                <table id="dataList"
                       class="table table-bordered table-striped table-hover dataTable">
                    <thead>
                    <tr>
                        <th class="">作业编号</th>
                        <th class="">作业名</th>
                        <th class="">作业描述</th>
                        <th class="">标签</th>
                        <th class="">内容</th>
                        <th class="">作业类型</th>
                        <th class="">是否批改</th>
                        <th class="">分数</th>
                        <th class="">星级</th>
                        <th class="">标签评价</th>
                        <th class="">评语</th>
                        <th class="">操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="homework" items="${student.homeworks}">
                        <tr>
                            <td>${homework.id}</td>
                            <td>${homework.workName }</td>
                            <td>${homework.instruction }</td>
                            <td>${homework.tags }</td>
                            <td>${homework.content}</td>
                            <td>${homework.typeStr }</td>
                            <td>${homework.cherkedStr }</td>
                            <td>${homework.score }</td>
                            <td>${homework.star }</td>
                            <td>${homework.tagEvaluate }</td>
                            <td>${homework.descEvaluate }</td>
                            <td class="text-center">
                                <a href="${pageContext.request.contextPath}/admin/img/findImgByHomeWorkId?homeWorkId=${homework.id}"
                                   class="btn bg-olive btn-xs">详情</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <!--数据列表/-->
            </div>
            <div class="box-tools text-center">
                <button type="button" class="btn bg-default"
                        onclick="history.back(-1);">返回
                </button>
            </div>
            <!--工具栏/-->
        </section>
        <!-- 正文区域 /-->
    </div>
    <!-- 内容区域 /-->
    <!-- 底部导航 -->
    <jsp:include page="footer.jsp"></jsp:include>
</div>
<script
        src="${pageContext.request.contextPath}/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap/js/bootstrap.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/raphael/raphael-min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/morris/morris.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/sparkline/jquery.sparkline.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/knob/jquery.knob.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/daterangepicker/moment.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/daterangepicker/daterangepicker.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/daterangepicker/daterangepicker.zh-CN.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/datepicker/locales/bootstrap-datepicker.zh-CN.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/fastclick/fastclick.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/iCheck/icheck.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/adminLTE/js/app.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/treeTable/jquery.treetable.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/select2/select2.full.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.zh-CN.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/locale/bootstrap-markdown.zh.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/js/markdown.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/js/to-markdown.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/ckeditor/ckeditor.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/input-mask/jquery.inputmask.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/chartjs/Chart.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.resize.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.pie.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.categories.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/ionslider/ion.rangeSlider.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-slider/bootstrap-slider.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>

<script>
    $(document).ready(function () {
        // 选择框
        $(".select2").select2();
        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    function delEmplayment(id) {
        if(confirm("确认删除就业信息？")){
            location.href='${pageContext.request.contextPath}/admin/student/delEmployment?id='+id;
        }
    }
    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

    $(document).ready(function () {
        // 激活导航位置
        setSidebarActive("order-manage");
        // 列表按钮
        $("#dataList td input[type='checkbox']").iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '20%'
        });
        // 全选操作
        $("#selall").click(function () {
            var clicks = $(this).is(':checked');
            if (!clicks) {
                $("#dataList td input[type='checkbox']").iCheck("uncheck");
            } else {
                $("#dataList td input[type='checkbox']").iCheck("check");
            }
            $(this).data("clicks", !clicks);
        });
    });
</script>
</body>


</html>