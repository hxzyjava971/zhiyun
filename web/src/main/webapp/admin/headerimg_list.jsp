<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- 页面meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>轮播图</title>
<meta name="description" content="轮播图">
<meta name="keywords" content="轮播图">

<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"
	name="viewport">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/ionicons/css/ionicons.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/iCheck/square/blue.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/morris/morris.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/datepicker/datepicker3.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/treeTable/jquery.treetable.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/treeTable/jquery.treetable.theme.default.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/select2/select2.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/colorpicker/bootstrap-colorpicker.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/adminLTE/css/AdminLTE.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/adminLTE/css/skins/_all-skins.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}admin/css/style.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/ionslider/ion.rangeSlider.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/ionslider/ion.rangeSlider.skinNice.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/bootstrap-slider/slider.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/admin/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css">
</head>

<body class="hold-transition skin-black sidebar-mini">

	<div class="wrapper">
		<!-- 页面头部 -->
		<jsp:include page="header.jsp"></jsp:include>
		<!-- 页面头部 /-->
		<!-- 导航侧栏 -->
		<jsp:include page="sidebar.jsp"></jsp:include>
		<!-- 导航侧栏 /-->

		<!-- 内容区域 -->
		<div class="content-wrapper">
			<!-- 内容头部 -->
			<section class="content-header">
			<h1>
				轮播图 <small>全部轮播图</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${pageContext.request.contextPath}/index.jsp"><i
						class="fa fa-dashboard"></i> 首页</a></li>
				<li><a href="${pageContext.request.contextPath}/log/whole">轮播图管理</a></li>

				<li class="active">全部轮播图</li>
			</ol>
			</section>
			<!-- 内容头部 /-->
			<!-- 正文区域 -->
			<section class="content"> <!-- .box-body -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">列表</h3>
				</div>
				<div class="box-body">
					<!-- 数据表格 -->
					<div class="table-box">
						<!--工具栏-->
						<form method="post" id="form1">
						<div class="pull-left">
							<div class="form-group form-inline">
								<div class="btn-group">
									<button type="button" class="btn btn-default" title="新建"
											onclick="location.href='${pageContext.request.contextPath}/admin/headerimg_add.jsp'">
										<i class="fa fa-file-o"></i> 新建
									</button>
									<button type="button" class="btn btn-default" title="刷新"
										onclick="window.location.reload();">
										<i class="fa fa-refresh"></i> 刷新
									</button>
								</div>
							</div>
						</div>
						<!--工具栏/-->
						<!--数据列表-->
						<table id="dataList"
							class="table table-bordered table-striped table-hover dataTable">
							<thead>
								<tr>
									<th class="" style="padding-right: 0px"><input id="selall" type="checkbox" class="icheckbox_square-blue"></th>
									<th class="" hidden>轮播图路径</th>
									<th class="">轮播图名字</th>
									<th class="">轮播图</th>
									<th class="">轮播图状态</th>
									<th class="">操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${headerImgs.content}" var="headerImg">
									<tr>
										<td><input name="ids" type="checkbox" id="itemselected" value="${headerImg.id}"></td>
										<td hidden>${headerImg.url}</td>
										<td>${headerImg.name}</td>
										<td><img src="${headerImg.url}" height="100px"></td>
										<td>${headerImg.statusStr}</td>
										<td>
											<a href="javascript:del(${headerImg.id})" class="btn bg-olive btn-xs">删除</a>
											<a href="${pageContext.request.contextPath}/admin/headerImg/updateHeaderImg2?id=${headerImg.id}" class="btn bg-olive btn-xs">修改</a>
											<a href="${pageContext.request.contextPath}/admin/headerImg/updateHeaderImg?ids=${headerImg.id}" class="btn bg-olive btn-xs">开启/关闭</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>

						</table>
						<!--数据列表/-->
						</form>
					</div>
					<!-- 数据表格 /-->

				</div>
				<!-- /.box-body -->

				<!-- .box-footer-->
				<div class="box-footer">
					<div class="pull-left">
						<div class="form-group form-inline">
							总共${headerImgs.totalPages}页，共${headerImgs.totalElements}条数据。 每页
							<select class="form-control" id="changePageSize" onchange="changePageSize()">
								<option value="5">5</option>
								<option value="10">10</option>
								<option value="15">15</option>
							</select> 条
						</div>
					</div>

					<div class="box-tools pull-right">
						<ul id="pageLimit"></ul>
					</div>
				</div>
				<!-- /.box-footer-->
			</div>
			</section>
			<!-- 正文区域 /-->

		</div>
		<!-- 内容区域 /-->

		<!-- 底部导航 -->
		<footer class="main-footer">
			<jsp:include page="footer.jsp"></jsp:include>
		</footer>
	</div>

	<script
		src="${pageContext.request.contextPath}/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/raphael/raphael-min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/morris/morris.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/sparkline/jquery.sparkline.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/knob/jquery.knob.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/daterangepicker/moment.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/daterangepicker/daterangepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/daterangepicker/daterangepicker.zh-CN.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/datepicker/locales/bootstrap-datepicker.zh-CN.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/fastclick/fastclick.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/iCheck/icheck.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/adminLTE/js/app.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/treeTable/jquery.treetable.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/select2/select2.full.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.zh-CN.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/locale/bootstrap-markdown.zh.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/js/markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/js/to-markdown.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/ckeditor/ckeditor.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/input-mask/jquery.inputmask.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/datatables/jquery.dataTables.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/chartjs/Chart.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.resize.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.pie.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.categories.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/ionslider/ion.rangeSlider.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/bootstrap-slider/bootstrap-slider.js"></script>
	<script
		src="${pageContext.request.contextPath}/admin/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
	<script src="${pageContext.request.contextPath}/admin/js/bootstrap-paginator.js"></script>
	<script>
        function del(id){
            if(confirm("确认删除？")){
                location.href = "${pageContext.request.contextPath}/admin/headerImg/deleteHeaderImg?ids="+id+"";
			}
        }

        function changePageSize() {
            //获取下拉框的值
            var pageSize=$("#changePageSize").val();
            //向服务器发送请求，改变每页显示条数
            location.href = "${pageContext.request.contextPath}/admin/headerImg/whole?pageNum=0&pageSize="+pageSize;
        }
		$(document).ready(function() {
            $("#changePageSize").val(${headerImgs.size})
			// 选择框
			$(".select2").select2();

			// WYSIHTML5编辑器
			$(".textarea").wysihtml5({
				locale : 'zh-CN'
			});
		});

		// 设置激活菜单
		function setSidebarActive(tagUri) {
			var liObj = $("#" + tagUri);
			if (liObj.length > 0) {
				liObj.parent().parent().addClass("active");
				liObj.addClass("active");
			}
		}

		$(document).ready(function() {
			// 激活导航位置
			setSidebarActive("order-manage");
			// 列表按钮
			$("#dataList td input[type='checkbox']").iCheck({
				checkboxClass : 'icheckbox_square-blue',
				increaseArea : '20%'
			});
			// 全选操作 
			$("#selall").click(function() {
				var clicks = $(this).is(':checked');
				if (!clicks) {
					$("#dataList td input[type='checkbox']").iCheck("uncheck");
				} else {
					$("#dataList td input[type='checkbox']").iCheck("check");
				}
				$(this).data("clicks", !clicks);
			});

            // 配置分页插件
            $('#pageLimit').bootstrapPaginator({
                currentPage: ${headerImgs.number+1},//当前页。
                totalPages: ${headerImgs.totalPages},//总页数。
                bootstrapMajorVersion: 3,//当前版本
                numberOfPages:10,//显示的页数
                tooltipTitles:function(){},// 鼠标移入不显示样式
                itemTexts: function (type, page, current) {//自定义按钮文本。
                    switch (type) {
                        case "first": return "首页";
                        case "prev": return "上一页";
                        case "next": return "下一页";
                        case "last": return "末页";
                        case "page": return page;
                    }
                },
                onPageClicked: function (event, originalEvent, type, page) {//给按钮绑定事件，page为按钮上的数字。
                    location.href = "${pageContext.request.contextPath}/admin/headerImg/whole?pageNum="+page+"&pageSize=${headerImgs.size}";
                }
            });
            $("li").css("cursor","pointer");
		});
	</script>
</body>

</html>