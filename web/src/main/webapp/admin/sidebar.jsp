<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="${pageContext.request.contextPath}/admin/img/header.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <a><i class="fa fa-circle text-success"></i><security:authentication property="principal.username"/></a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li id="admin-index"><a href="${pageContext.request.contextPath}/admin/main.jsp"><i
                    class="fa fa-dashboard"></i><span>首页</span></a>
            </li>
            <security:authorize access="hasRole('ROLE_SUPERADMIN')">
                <%-- 系统管理 --%>
                <li class="treeview" id="system">
                    <a href="#">
                        <i class="fa fa-cogs"></i><span>系统管理</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li id="user-setting">
                            <a href="${pageContext.request.contextPath}/admin/admin/all">
                                <i class="fa fa-circle-o"></i>用户管理
                            </a>
                        </li>
                        <li id="role-setting">
                            <a href="${pageContext.request.contextPath}/admin/role/all">
                                <i class="fa fa-circle-o"></i>角色管理
                            </a>
                        </li>
                        <li id="dictionary-setting">
                            <a href="${pageContext.request.contextPath}/admin/dictionary/findAll">
                                <i class="fa fa-circle-o"></i>数据字典
                            </a>
                        </li>
                        <li id="log-setting">
                            <a href="${pageContext.request.contextPath}/admin/log/whole">
                                <i class="fa fa-circle-o"></i>日志管理
                            </a>
                        </li>
                        <li id="weblog-setting">
                            <a href="${pageContext.request.contextPath}/admin/weblog/whole">
                                <i class="fa fa-circle-o"></i>用户日志管理
                            </a>
                        </li>
                        <li id="programmer-setting">
                            <a href="${pageContext.request.contextPath}/admin/programmer/all">
                                <i class="fa fa-circle-o"></i>程序员管理
                            </a>
                        </li>
                    </ul>
                </li>
            </security:authorize>
            <%-- 学校管理 --%>
            <li class="treeview" id="school">
                <a href="#">
                    <i class="fa fa-cube"></i><span>学校数据</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li id="school-manage">
                        <a href="${pageContext.request.contextPath}/admin/school/all">
                            <i class="fa fa-circle-o"></i>学校管理
                        </a>
                    </li>
                    <li id="classes-manage">
                        <a href="${pageContext.request.contextPath}/admin/classes/all">
                            <i class="fa fa-circle-o"></i>班级管理
                        </a>
                    </li>
                    <li id="teacher-manage">
                        <a href="${pageContext.request.contextPath}/admin/teacher/all">
                            <i class="fa fa-circle-o"></i>老师管理
                        </a>
                    </li>
                    <li id="student-manage">
                        <a href="${pageContext.request.contextPath}/admin/student/findAll">
                            <i class="fa fa-circle-o"></i>学生管理
                        </a>
                    </li>
                    <security:authorize access="hasRole('ROLE_SUPERADMIN')">
                        <li id="job-manage">
                            <a href="${pageContext.request.contextPath}/admin/homework/findAll">
                                <i class="fa fa-circle-o"></i>作业管理
                            </a>
                        </li>
                    </security:authorize>
                </ul>
            </li>
            <security:authorize access="hasRole('ROLE_SUPERADMIN')">
                <%-- 网站管理 --%>
                <li class="treeview" id="web">
                    <a href="#">
                        <i class="fa fa-cube"></i><span>网站管理</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        <li id="headerImg-manage">
                            <a href="${pageContext.request.contextPath}/admin/headerImg/whole">
                                <i class="fa fa-circle-o"></i>轮播图管理
                            </a>
                        </li>
                        <li id="goldTeacher-manage">
                            <a href="${pageContext.request.contextPath}/admin/goldTeacher/all">
                                <i class="fa fa-circle-o"></i>金牌教师管理
                            </a>
                        </li>
                        <li id="employment-manage">
                            <a href="${pageContext.request.contextPath}/admin/employment/all">
                                <i class="fa fa-circle-o"></i>就业明星管理
                            </a>
                        </li>
                    </ul>
                </li>
            </security:authorize>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<script src="${pageContext.request.contextPath}/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script>
    $(function () {
        // 向html本地存储单击信息
        $(".treeview").click(function () {
            sessionStorage.menu = $(this).attr("id");
        })
        $(".treeview-menu li").click(function () {
            sessionStorage.menu2 = $(this).attr("id");
        })
        // 给已选菜单赋值
        var menu = sessionStorage.menu;
        var menu2 = sessionStorage.menu2;
        $("#" + menu).addClass("active");
        $("#" + menu2 + " a").css("color", "white");
    })
</script>