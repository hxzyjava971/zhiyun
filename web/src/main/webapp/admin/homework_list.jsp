<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>作业信息</title>
    <meta name="description" content="作业管理">
    <meta name="keywords" content="作业信息">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/ionicons/css/ionicons.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/iCheck/square/blue.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/morris/morris.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/treeTable/jquery.treetable.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/treeTable/jquery.treetable.theme.default.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/select2/select2.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/adminLTE/css/AdminLTE.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/adminLTE/css/skins/_all-skins.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/css/style.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/ionslider/ion.rangeSlider.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/ionslider/ion.rangeSlider.skinNice.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap-slider/slider.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css">
</head>

<body class="hold-transition skin-black sidebar-mini">

<div class="wrapper">
    <!-- 页面头部 -->
    <jsp:include page="header.jsp"></jsp:include>
    <!-- 页面头部 /-->
    <!-- 导航侧栏 -->
    <jsp:include page="sidebar.jsp"></jsp:include>
    <!-- 导航侧栏 /-->
    <!-- 内容区域 -->
    <div class="content-wrapper">

        <!-- 内容头部 -->
        <section class="content-header">
            <!-- 内容头部 -->
            <section class="content-header">
                <h1>
                    作业管理
                </h1>
            </section>
            </h1>
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/index.jsp"><i
                        class="fa fa-dashboard"></i> 首页</a></li>
                <li><a href="${pageContext.request.contextPath}/log/whole">作业管理</a></li>
                <li class="active">全部作业</li>
            </ol>
        </section>
        <!-- 内容头部 /-->

        <!-- 正文区域 -->
        <section class="content"> <!-- .box-body -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">列表</h3>
                </div>
                <div class="box-body">
                    <form action="" name="temp" method="post">
                        <!-- 数据表格 -->
                        <div class="table-box">
                            <!--工具栏-->
                            <div class="pull-left">
                                <div class="form-group form-inline">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default" title="刷新"
                                                onclick="window.location.reload();">
                                            <i class="fa fa-refresh"></i>刷新
                                        </button>
                                        <input value="删除" class="btn btn-default" type="submit"
                                               onclick='if (confirm("你确认要删除吗？")){window.temp.action="${pageContext.request.contextPath}/admin/homework/del";temp.submit()}else { return false}'>
                                    </div>
                                </div>
                            </div>

                            <div class="pull-right" style="width: 600px">
                                <div class="pull-right" style="width: 100%">
                                    <select id="xiaoqu" class="pull-left form-control select2" style="width: 33%;" name="emplDepartment" onchange="x()">
                                        <c:if test="${nowSchool==null}">
                                            <option>--请选择校区--</option>
                                            <c:forEach items="${schools}" var="school">
                                                <option value="${school.id}">${school.schoolName}</option>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${nowSchool!=null}">
                                            <c:forEach items="${schools}" var="school">
                                                <c:if test="${school.id==schoolId}">
                                                    <option selected value="${school.id}">${school.schoolName}</option>
                                                </c:if>
                                                <c:if test="${school.id!=schoolId}">
                                                    <option value="${school.id}">${school.schoolName}</option>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                    <select id="banji" class="pull-left form-control select2"  style="width: 33%;"  name="emplDepartment" onchange="x1()">
                                        <c:if test="${nowClasses != null}">
                                            <option selected
                                                    value="${nowClasses.id}">${nowClasses.className}</option>
                                            <c:forEach items="${classes}" var="class1">
                                                <c:if test="${nowClasses.id != class1.id}">
                                                    <option value="${class1.id}">${class1.className}</option>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                    <select id="xuesheng" class="pull-left form-control select2" style="width: 33%;"  name="emplDepartment" onchange="x2()">
                                        <c:if test="${nowStudents != null}">
                                            <c:forEach items="${students}" var="student">
                                                <c:if test="${nowStudents.id==student.id}">
                                                    <option selected value="${student.id}">${student.name}</option>
                                                </c:if>
                                                <c:if test="${nowStudents.id!=student.id}">
                                                    <option value="${student.id}">${student.name}</option>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                </div>
                            </div>
                            <!--工具栏/-->

                            <!--数据列表-->
                            <table id="dataList"
                                   class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                <tr>
                                    <th class="" style="padding-right:0px;">
                                        <input id="ck" type="checkbox" class="icheckbox_square-blue">
                                    </th>
                                    <th class="sorting_asc sorting_asc_disabled text-center">作业名</th>
                                    <th class="sorting_desc sorting_desc_disabled text-center">是否批改</th>
                                    <th class="sorting_desc sorting_desc_disabled text-center">分数</th>
                                    <th class="sorting_desc sorting_desc_disabled text-center">评价详情</th>
                                    <th class="sorting_desc sorting_desc_disabled text-center">封面图片</th>
                                    <th class="sorting_desc sorting_desc_disabled text-center">作业提交时间</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${homeworks.content}" var="homework">
                                    <tr>
                                        <td><input name="ids" type="checkbox" value="${homework.id}"></td>
                                        <td>${homework.workName}</td>
                                        <td>${homework.cherkedStr}</td>
                                        <td>${homework.score}</td>
                                        <td>${homework.descEvaluate}</td>
                                        <td><img id="img1" width="200px" height="100" src="${homework.coverImg}">
                                        </td>
                                        <td>${homework.timeStr}</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            <!--数据列表/-->
                        </div>
                    </form>
                    <!-- 数据表格 /-->
                </div>
                <!-- /.box-body -->

                <!-- .box-footer-->
                <div class="box-footer">
                    <div class="pull-left">
                        <div class="form-group form-inline">
                            总共${homeworks.totalPages}页，共${homeworks.totalElements}条数据。 每页
                            <select class="form-control" id="changePageSize" onchange="changePageSize()">
                                <option value="5" selected>5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                            </select>条
                        </div>
                    </div>
                    <div class="box-tools pull-right">
                        <ul id="pageLimit"></ul>
                    </div>
                </div>
                <!-- /.box-footer-->
            </div>
        </section>
        <!-- 正文区域 /-->
    </div>
    <!-- 内容区域 /-->
    <!-- 底部导航 -->
    <footer class="main-footer">
        <jsp:include page="footer.jsp"></jsp:include>
    </footer>
</div>


<script
        src="${pageContext.request.contextPath}/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap/js/bootstrap.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/raphael/raphael-min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/morris/morris.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/sparkline/jquery.sparkline.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/knob/jquery.knob.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/daterangepicker/moment.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/daterangepicker/daterangepicker.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/daterangepicker/daterangepicker.zh-CN.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/datepicker/locales/bootstrap-datepicker.zh-CN.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/fastclick/fastclick.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/iCheck/icheck.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/adminLTE/js/app.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/treeTable/jquery.treetable.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/select2/select2.full.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.zh-CN.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/locale/bootstrap-markdown.zh.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/js/markdown.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/js/to-markdown.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/ckeditor/ckeditor.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/input-mask/jquery.inputmask.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/chartjs/Chart.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.resize.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.pie.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.categories.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/ionslider/ion.rangeSlider.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-slider/bootstrap-slider.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/js/bootstrap-paginator.js"></script>

<script>
    function x() {
        var schoolId = $("#xiaoqu option:selected").val();
        $.ajax({
            url: "/admin/school/fcbs?schoolId=" + schoolId,
            type: "GET",
            success: function (data) {
                $("#banji").empty();
                var str = "<option>-- 请选择班级 --</option>";
                for (i = 0; i < data.length; i++) {
                    str += "<option value=\"" + data[i].id + "\" onclick=\"x1('" + data[i].id + "')\">" + data[i].className + "</option>"
                }
                $("#banji").html(str);
            }
        })
    }

    function x1() {
        var classId = $("#banji option:selected").val();
        $.ajax({
            url: "/admin/student/findByClassId?classId=" + classId,
            type: "GET",
            success: function (data) {
                console.log(data);
                $("#student").empty();
                var str = "<option>-- 请选择学生 --</option>";
                for (i = 0; i < data.length; i++) {
                    str += "<option value=\"" + data[i].id + "\" onclick=\"x1()\">" + data[i].name + "</option>"
                }
                $("#xuesheng").html(str);
            }
        })
    }

    function x2() {
        var schoolId = $("#xiaoqu option:selected").val();
        var classId = $("#banji option:selected").val();
        var studentId = $("#xuesheng option:selected").val();
        location.href = "${pageContext.request.contextPath}/admin/homework/findAll?schoolId=" + schoolId + "&classId=" + classId + "&studentId=" + studentId;
    }

    function changePageSize() {
        //获取下拉框的值
        var pageSize = $("#changePageSize").val();
        //向服务器发送请求，改变每页显示条数
        location.href = "${pageContext.request.contextPath}/admin/homework/findAll?pageNum=0&pageSize=" + pageSize + "&schoolId=${schoolId}" + "&classId=${classId}" + "&studentId=${studentId}";
    }

    $(document).ready(function () {
        $("#changePageSize").val(${homeworks.size})
        // 选择框
        // $(".select2").select2();
        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

    $(document).ready(function () {
        // 激活导航位置
        setSidebarActive("order-manage");
        // 列表按钮
        $("#dataList td input[type='checkbox']").iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '20%'
        });
        // 全选操作
        $("#selall").click(function () {
            var clicks = $(this).is(':checked');
            if (!clicks) {
                $("#dataList td input[type='checkbox']").iCheck("uncheck");
            } else {
                $("#dataList td input[type='checkbox']").iCheck("check");
            }
            $(this).data("clicks", !clicks);
        });


        // 配置分页插件
        $('#pageLimit').bootstrapPaginator({
            currentPage: ${homeworks.number+1},//当前页。
            totalPages: ${homeworks.totalPages},//总页数。
            bootstrapMajorVersion: 3,//当前版本
            numberOfPages: 10,//显示的页数
            tooltipTitles: function () {
            },// 鼠标移入不显示样式
            itemTexts: function (type, page, current) {//自定义按钮文本。
                switch (type) {
                    case "first":
                        return "首页";
                    case "prev":
                        return "上一页";
                    case "next":
                        return "下一页";
                    case "last":
                        return "末页";
                    case "page":
                        return page;
                }
            },
            onPageClicked: function (event, originalEvent, type, page) {//给按钮绑定事件，page为按钮上的数字。
                location.href = "${pageContext.request.contextPath}/admin/homework/findAll?pageNum=" + page + "&pageSize=${homeworks.size}" + "&schoolId=${schoolId}" + "&classId=${classId}" + "&studentId=${studentId}";
            }
        });
        $("li").css("cursor", "pointer");
    });


</script>
</body>

</html>