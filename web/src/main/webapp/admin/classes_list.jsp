<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>--%>
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>班级管理</title>
    <meta name="description" content="班级管理">
    <meta name="keywords" content="班级管理">

    <!-- Tell the browser to be responsive to screen width -->
    <meta
            content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"
            name="viewport">

    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/ionicons/css/ionicons.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/iCheck/square/blue.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/morris/morris.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/treeTable/jquery.treetable.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/treeTable/jquery.treetable.theme.default.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/select2/select2.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/adminLTE/css/AdminLTE.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/adminLTE/css/skins/_all-skins.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/css/style.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/ionslider/ion.rangeSlider.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/ionslider/ion.rangeSlider.skinNice.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/admin/plugins/bootstrap-slider/slider.css">
<style>
    #schoolName{
        margin-left: 132px;
    }
</style>
</head>

<body class="hold-transition skin-black sidebar-mini">

<div class="wrapper">
    <jsp:include page="header.jsp"></jsp:include>
    <jsp:include page="sidebar.jsp"></jsp:include>
    <!-- 内容区域 -->
    <div class="content-wrapper">
        <!-- 内容头部 -->
        <section class="content-header">
            <h1>
                班级管理
                <small>全部班级</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="${pageContext.request.contextPath}/index.jsp"><i
                        class="fa fa-dashboard"></i> 首页</a></li>
                <li><a
                        href="${pageContext.request.contextPath}/role/findAll.do">班级管理</a></li>
                <li class="active">全部班级</li>
            </ol>
        </section>
        <!-- 内容头部 /-->
        <!-- 正文区域 -->
        <section class="content"> <!-- .box-body -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">列表</h3>
                </div>

                <div class="box-body">
                    <form method="post">
                        <!-- 数据表格 -->
                        <div class="table-box">
                            <!--工具栏-->
                            <div class="pull-left">
                                <div class="form-group form-inline">
                                    <div class="btn-group">
                                        <%--<security:authorize access="hasAnyRole('ROLE_ADMIN')">--%>
                                        <button type="button" class="btn btn-default" title="新建"
                                                onclick="location.href='${pageContext.request.contextPath}/admin/classes/findAllSchoolAndType'">
                                            <i class="fa fa-file-o"></i> 新建
                                        </button>
                                        <%--</security:authorize>--%>
                                        <a type="button" class="btn btn-default" title="刷新" onclick="location.reload()">
                                            <i class="fa fa-refresh"></i> 刷新
                                        </a>
                                        <input value="删除" class="btn btn-default" type="submit" onclick='del()'>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group col-md-2 col-lg-3 pull-right">
                                <select id="schoolName" class="form-control" style="width: 225px">
                                    <c:if test="${schoolName == null}">
                                        <option selected value="null">请选择校区</option>
                                    </c:if>
                                    <c:forEach items="${schools}" var="school">
                                        <c:if test="${school.schoolName == schoolName}">
                                            <option value="${schoolName}" selected>${schoolName}</option>
                                        </c:if>
                                        <c:if test="${school.schoolName != schoolName}">
                                            <option value="${school.schoolName}">${school.schoolName}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>

                                <%--<input type="text" class="form-control" placeholder="请输入校区" id="" value="${schoolName}">--%>
                                <span class="input-group-btn">
                                    <a class="btn btn-info btn-search" onclick="findLike1()">查询</a>
                                </span>
                            </div>

                            <!--工具栏/-->
                            <!--数据列表-->
                            <table id="dataList"
                                   class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                <tr>
                                    <th class="" style="padding-right:0px;">
                                        <input id="ck" type="checkbox" class="icheckbox_square-blue">
                                    </th>
                                    <th>班级名字</th>
                                    <th>班级类型</th>
                                    <th>开班时间</th>
                                    <th>结业时间</th>
                                    <th>班主任</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${classesList.content}" var="classes">
                                    <tr>
                                        <td><input name="ids" class="ids" type="checkbox" value="${classes.id}"></td>
                                        <td>${classes.className}</td>
                                        <td>${classes.type }</td>
                                        <td>${classes.startTimeStr }</td>
                                        <td>${classes.endTimeStr }</td>
                                        <td>${classes.headTeacher }</td>
                                        <td class="text-center">
                                            <a href="${pageContext.request.contextPath}/admin/classes/findTeacherByClassesId?classesId=${classes.id}"
                                               class="btn bg-olive btn-xs">教授老师</a>
                                            <a href="${pageContext.request.contextPath}/admin/classes/findById?id=${classes.id}"
                                               class="btn bg-olive btn-xs">修改</a>
                                            <a href="${pageContext.request.contextPath}/admin/classes/findByNotTeacher?classesId=${classes.id}"
                                               class="btn bg-olive btn-xs">添加老师</a>
                                                <%--</security:authorize>--%>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            <!--数据列表/-->
                        </div>
                        <!-- 数据表格 /-->
                    </form>
                </div>
                <!-- /.box-body -->

                <!-- .box-footer-->
                <div class="box-footer">
                    <div class="pull-left">
                        <div class="form-group form-inline">
                            总共${classesList.totalPages} 页，共${classesList.totalElements} 条数据。 每页
                            <select class="form-control" id="changePageSize" onchange="changePageSize()">
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                            </select> 条
                        </div>
                    </div>
                    <div class="box-tools pull-right">
                        <ul id="pageLimit"></ul>
                    </div>
                </div>
                <!-- /.box-footer-->

            </div>

            <!-- /.box-footer-->


        </section>
        <!-- 正文区域 /-->
    </div>
    <!-- @@close -->
    <!-- 内容区域 /-->
    <!-- 底部导航 -->
    <footer class="main-footer">
        <jsp:include page="footer.jsp"></jsp:include>
    </footer>

</div>

<script src="${pageContext.request.contextPath}/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="${pageContext.request.contextPath}/admin/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/raphael/raphael-min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/morris/morris.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/sparkline/jquery.sparkline.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/knob/jquery.knob.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/daterangepicker/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/daterangepicker/daterangepicker.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/daterangepicker/daterangepicker.zh-CN.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/datepicker/locales/bootstrap-datepicker.zh-CN.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/fastclick/fastclick.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/iCheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/adminLTE/js/app.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/treeTable/jquery.treetable.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/select2/select2.full.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.zh-CN.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/locale/bootstrap-markdown.zh.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/js/markdown.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/bootstrap-markdown/js/to-markdown.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/ckeditor/ckeditor.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/input-mask/jquery.inputmask.js"></script>
<script
        src="${pageContext.request.contextPath}/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/chartjs/Chart.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/flot/jquery.flot.categories.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/ionslider/ion.rangeSlider.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/plugins/bootstrap-slider/bootstrap-slider.js"></script>
<script src="${pageContext.request.contextPath}/admin/js/bootstrap-paginator.js"></script>
<script>
    //按校区搜索
    function findLike1() {
        var pageSize = $("#changePageSize").val();
        var schoolName = $("#schoolName").val();
        location.href = "${pageContext.request.contextPath}/admin/classes/schoolName?schoolName=" + schoolName + "&pageNum=0&pageSize=" + pageSize;
    }

    //删除班级
    function del() {
        var flag = false;
        $.each($(".ids"), function () {//判读是否选择了班级
            if ($(this).prop("checked") == true) {
                flag = true;
            }
        });
        if (flag) {//true 为选择   false为未选择进行提示
            if (confirm("你确认要删除吗？")) {
                var str = "";
                for (var i = 0; i < $(".ids").length; i++) {
                    if ($(".ids")[i].checked == true) {
                        str += ("&ids=" + $(".ids")[i].value);
                    }
                }
                $.get("${pageContext.request.contextPath}/admin/classes/del?1=1" + str, function (data) {
                    if (data.result) {
                        // alert(1)
                        location.reload();
                    } else {
                        alert(data.message)
                    }
                })
            }
        } else {
            alert("您还未选择要删除的班级！")
        }
    }

    function changePageSize() {
        //获取下拉框的值
        var pageSize = $("#changePageSize").val();

        //向服务器发送请求，改变每页显示条数
        location.href = "${pageContext.request.contextPath}/admin/classes/schoolName?pageNum=0&pageSize=" + pageSize + "&schoolName=${schoolName}";
    }

    $(document).ready(function () {
        $("#changePageSize").val(${classesList.size})
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

    $(document).ready(function () {

        // 激活导航位置
        setSidebarActive("order-manage");

        // 列表按钮
        $("#dataList td input[type='checkbox']").iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '20%'
        });
        // 全选操作
        $("#selall").click(function () {
            var clicks = $(this).is(':checked');
            if (!clicks) {
                $("#dataList td input[type='checkbox']").iCheck("uncheck");
            } else {
                $("#dataList td input[type='checkbox']").iCheck("check");
            }
            $(this).data("clicks", !clicks);
        });

        // 配置分页插件
        $('#pageLimit').bootstrapPaginator({
            currentPage:${classesList.number+1},//当前页。
            totalPages: ${classesList.totalPages},//总页数。
            bootstrapMajorVersion: 3,//当前版本
            numberOfPages: 10,//显示的页数
            tooltipTitles: function () {
            },// 鼠标移入不显示样式
            itemTexts: function (type, page, current) {//自定义按钮文本。
                switch (type) {
                    case "first":
                        return "首页";
                    case "prev":
                        return "上一页";
                    case "next":
                        return "下一页";
                    case "last":
                        return "末页";
                    case "page":
                        return page;
                }
            },
            onPageClicked: function (event, originalEvent, type, page) {//给按钮绑定事件，page为按钮上的数字。
                location.href = "${pageContext.request.contextPath}/admin/classes/schoolName?pageNum=" + page + "&pageSize=${classesList.size}&schoolName=${schoolName}";
            }
        });
        $("li").css("cursor", "pointer");
    });


</script>
</body>

</html>