<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="Author" contect="http://www.webqin.net">
    <title>忘记密码3</title>
    <!--动画-->
    <link rel="stylesheet" href="/user/css/animate.css" type="text/css"/>
    <!---->
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link type="text/css" href="/user/css/css.css" rel="stylesheet"/>

</head>

<body>
<!--top-->
<div class="yxq">
    <a href="../页面/index.html">
        <div class="bz">
            <img src="/user/img/biaozhi.jpg"/>
        </div>
    </a>
    <ul class="nav">

    </ul>
</div>


<!---->

<div class="content">
    <div class="wow fadeInRight bjtx" data-wow-duration="3s">
        <img src="/user/img/beij.png"/>
    </div>
    <div class="for-liucheng">
        <div class="wow fadeInRightBig liulist "></div>
        <div class="wow fadeInRightBig liulist "></div>
        <div class="wow fadeInRightBig liulist for-cur"></div>
        <div class="liutextbox">
            <div class="wow fadeInRightBig liutext "><strong>验证手机</strong><br/><em>1</em></div>
            <div class="wow fadeInRightBig liutext" data-wow-delay="0.6s"><strong>设置新密码</strong><br/><em>2</em>
            </div>
            <div class="wow fadeInRightBig liutext" data-wow-delay="1s"><strong>密码重置完成</strong><br/><em>3</em></div>
        </div>
    </div><!--for-liucheng/-->
    <form action="" method="get" class="forget-pwd">
        <div class="tp">
            <img src="/user/img/zhuce.png"/>
            <h2 class="gx">恭喜您，重置密码成功！</h2>

        </div>
        <div class="subtijiao"><input type="button"  value="去登陆体验吧" onclick="tiaozhuan()"/></div>
    </form><!--forget-pwd/-->
    <!--  <div class="successs">
       <h3>恭喜您，修改成功！</h3>
      </div>-->
</div><!--web-width/-->
</div><!--content/-->
<!--bottom-->
<div class="bottom">
    <ul class="tm">
        <li>智云</li>
        <li>帮助中心</li>
        <li>学习中心</li>
        <li class="yq">友情链接</li>
    </ul>
    <h6>DT人才培训基地（太原中心）&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;晋ICP备16009028号&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;咨询热线：400-7777-699&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;地址：太原市高新区平阳南路龙兴街万立科技大厦17层&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;版权所有：华信智原</h6>
</div>
<!---->
<!--动画-->
<script src="/user/js/wow.min.js"></script>
<script>
    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))) {
        new WOW().init();
    }

    function tiaozhuan(){
        window.location.href = "${pageContext.request.contextPath}/user/student_login.jsp";
    }

</script>
<!---->
</body>
</html>
