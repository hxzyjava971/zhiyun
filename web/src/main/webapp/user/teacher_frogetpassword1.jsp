<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%--<html xmlns="http://www.w3.org/1999/xhtml">--%>
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">

    <script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>老师_修改密码</title>
    <link rel="shortcut icon" href="images/favicon.ico"/>
    <link type="text/css" href="/user/css/css.css" rel="stylesheet"/>

    <script type="text/javascript" src="/user/js/jquery-1.8.3-min.js"></script>
    <script type="text/javascript" src="/user/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="/user/js/bootstrap.min.js"></script>

    <!--动画-->
    <link rel="stylesheet" href="/user/css/animate.css" type="text/css"/>
    <!---->

</head>
<script type="text/javascript">

</script>
<body>
<!--top-->
<div class="yxq">
    <a href="../页面/index.html">
        <div class="wow bounceInLeft bz">
            <img src="img/biaozhi.jpg"/>
        </div>
    </a>
    <ul class="wow bounceInRight nav">
        <%--<li>老师入口</li>--%>
        <%--<li class="na">学生入口</li>--%>
    </ul>
</div>


<!---->
<div class="content">
    <div class="wow fadeInRight bjtx" data-wow-duration="3s">
        <img src="img/beij.png"/>
    </div>
    <div class="web-width">
        <div class="for-liucheng">
            <div class="wow fadeInRightBig liulist for-cur"></div>
            <div class="wow fadeInRightBig liulist"></div>
            <div class="wow fadeInRightBig liulist"></div>
            <div class="liutextbox">
                <div class="wow fadeInRightBig liutext for-cur"><strong>验证手机</strong><br/><em>1</em></div>
                <div class="wow fadeInRightBig liutext" data-wow-delay="0.6s"><strong>设置新密码</strong><br/><em>2</em>
                </div>
                <div class="wow fadeInRightBig liutext" data-wow-delay="1s"><strong>密码重置完成</strong><br/><em>3</em></div>
            </div>
        </div><!--for-liucheng/-->

        <%--<form action="forgetPwd2.jsp" method="get" class="wow zoomInLeft forget-pwd" data-wow-delay="1.2s" data-wow-duration="1.5s">--%>
        <form id="formFindPassword" method="post" class="wow zoomInLeft forget-pwd"
              data-wow-delay="1.2s" data-wow-duration="1.5s"
              action="${pageContext.request.contextPath}/user/student/findPassword">
            <dl>

                <p id="prompt" style="color: red;margin-left: 150px;margin-top: -15px;position: absolute"></p>
                </dd>
                <div class="clears"></div>
            </dl>

            <dl class="sel-yzsj" style="margin: 20px 0px">
                <dt>手机号：</dt>
                <dd><input type="text" id="phoneNum" name="phoneNum"/></dd>
                <div class="clears"></div>
            </dl>

            <dl style="margin: 20px 0px">
                <dt>手机校验码：</dt>
                <%--<dd class="xy"><input type="text" class="xym" /> <button class="xym_1" onclick="sendCheckCode()" id="btn1">获取验证码</button></dd>--%>
                <dd class="xy">
                    <input type="text" class="xym" name="code" id="phoneCheckCode" maxlength="4"/>
                    <button type="button" id="btn1" class="xym_1" style="float: right" >获取验证码</button>
                    <%--<button type="button" id="btn1" class="xym_1" onclick="sendCheckCode()">获取验证码</button>--%>
                </dd>
                <div class="clears"></div>
            </dl>
            <div class="subtijiao"><input type="button" id="nextBtn" value="下一步" /></div>
            <%--<h5 class="zdl">我已有账号，<a href="#">登录</a></h5>--%>
            <%--<h5 class="zdl">注册遇到问题，请<a href="#">咨询在线客服</a></h5>--%>
        </form><!--forget-pwd/-->
    </div><!--web-width/-->
</div><!--content/-->

<!--bottom-->
<div class="bottom">
    <ul class="tm">
        <li>智云</li>
        <li>帮助中心</li>
        <li>学习中心</li>
        <li class="yq">友情链接</li>
    </ul>
    <h6>DT人才培训基地（太原中心）&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;晋ICP备16009028号&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;咨询热线：400-7777-699&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;地址：太原市高新区平阳南路龙兴街万立科技大厦17层&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;版权所有：华信智原</h6>
</div>
<!---->
<!--动画-->
<script src="/user/js/wow.min.js"></script>

<script>
    $(function () {
        $("#phoneNum").blur(checkPhoneNum);
        $("#phoneCheckCode").blur(checkphoneCheckCode);
    })

    function checkPhoneNum() {
        var phoneNum = $("#phoneNum").val();
        var reg = /^1(3|4|5|6|7|8|9)\d{9}$/;
        var flag = reg.test(phoneNum);
        if (phoneNum == "") {
            $("#prompt").html("手机号不能为空");
        } else {
            if (flag) {
                $("#phoneNum").css("border", "1px solid rgba(255, 255, 255, 0.4)");
                $("#btn1").removeAttr("disabled");
            } else {
                $("#prompt").html("请填写正确的手机号");
            }
        }
        return flag;
    }

    function checkphoneCheckCode() {
        var phoneCheckCode = $("#phoneCheckCode").val();
        var reg = /[0-9]{4}/;
        var flag = reg.test(phoneCheckCode);
        if (phoneCheckCode == "") {
            $("#prompt").html("验证码不能为空");
        } else {
            if (flag) {
                $("#phoneCheckCode").css("border", "1px solid rgba(255, 255, 255, 0.4)");
                $("#nextBtn").removeAttr("disabled");
            } else {
                $("#prompt").html("请输入正确的验证码");
            }
        }
        return flag;
    }

    //异步获取手机验证码
    $("#btn1").click(function () {
        if (checkPhoneNum()) {
            var phoneNum = $("#phoneNum").val();
            $.post("${pageContext.request.contextPath}/user/teacher/LoginSendPhoneCode", {"phoneNum": phoneNum}, function (data) {
                if (data.result) {
                    countDown();
                } else if (!data.result) {
                    $("#prompt").html(data.message);
                }
            }, "json");
        }
    })

    //异步访问controller 下一步
    $("#nextBtn").click(function () {
        var phoneNum = $("#phoneNum").val();
        var phoneCheckCode = $("#phoneCheckCode").val();
        var flag=checkPhoneNum();
        var flag1=checkphoneCheckCode();
        if (flag == false) {
            $("#phoneNum").css("border", "1px solid red");
            $("#prompt").html("请填写正确的手机号");
        }
        if (flag1 == false) {
            $("#phoneCheckCode").css("border", "1px solid red");
            $("#prompt").html("请输入正确的验证码");
        }
        if (flag == true && flag1 == true) {
            $.post("${pageContext.request.contextPath}/user/teacher/findPassword", {
                "phoneNum": phoneNum,
                "phoneCheckCode": phoneCheckCode
            }, function (data) {
                if (data.result) {
                    window.location.href = "${pageContext.request.contextPath}/user/teacher_forgetpassword2.jsp";
                } else if (!data.result) {
                    $("#prompt").html(data.message);
                }
            }, "json");
        }else if (flag == false && flag1 == false) {
            $("#phoneNum").css("border", "1px solid red");
            $("#phoneCheckCode").css("border", "1px solid red");
            $("#prompt").html("请输入正确的手机号和验证码");
        }
    })
    function countDown() {
        let count = 60;
        const countDown = setInterval(() => {
            if (count === 0) {
                $('.xym_1').text('重新发送').removeAttr('disabled');
                $('.xym_1').css({
                    // background: '#ff9400',
                    color: '#fff',
                });
                clearInterval(countDown);
            } else {
                $('.xym_1').attr('disabled', true);
                $('.xym_1').css({
                    background: '#d8d8d8',
                    color: '#fff',
                });
                $('.xym_1').text(count + 's后重新获取验证码');
            }
            count--;
        }, 1000);
    }

    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))) {
        new WOW().init();
    }

</script>
<!---->

</body>
</html>
