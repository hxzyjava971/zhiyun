<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>个人信息</title>
    <!--作业-->
    <!--动画效果-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/animate.css" type="text/css">
    <!--动画效果-->
    <!--fycj-->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/zxf_page.css">
    <!--cj-->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/style (4).css">
    <!--cj-->
    <link href="${pageContext.request.contextPath}/user/css/style-a.css" rel="stylesheet" type="text/css"/>
    <!--分页代码-->
    <link type="text/css" href="${pageContext.request.contextPath}/user/css/style (2).css" rel="stylesheet"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/jquery.min (4).js"></script>
    <!--分页代码1-->
    <!--切换-->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/assets/style.css">
    <!--自己-->
    <link href="${pageContext.request.contextPath}/user/css/style1.css" rel="stylesheet" type="text/css"/>
    <style>
        #collectionFenYe {
            position: relative;
            left: 14px;
            top: -44px;
        }

        #collection {
            display: inline-block;
        }

        #yeShu {
            float: left;
            margin-left: 250px;
        }

        .ta_1 li {
            cursor: pointer;
        }

        .img_a {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .xyt {
            border: 1px solid #ccc;
        }

        #btn {
            position: absolute;
            left: 178px;
            color: #ff2222;
            background: #f1f1f1;
            border: none;
            outline: none;
            width: 90px;
        }

        .tx_1 .img_a {
            border-radius: 50%;
        }
    </style>
</head>
<body>
<!--动画-->
<script src="${pageContext.request.contextPath}/user/js/wow.min.js"></script>
<script>
    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))) {
        new WOW().init();
    }
    ;
</script>
<!--动画-->
<!--top-->
<jsp:include page="top.jsp"></jsp:include>
<!--banner-->
<div class="beijing">
    <div class="yxq">
        <div class="bj_1">
            <div class="wow swing bj1" data-wow-iteration="2"
                 style="background:url(${student.header}) 100% 100% / 100% no-repeat; "></div>
            <div class="bj1_1">
                <h2 class="wow bounceInUp ">${student.name}</h2>
            </div>
        </div>
        <div class="wow flipInY bj_2">${student.introduction}</div>
    </div>
</div>

<!--关注-->
<div class="tab">
    <div class="box">
        <ul class=" bounceInLeft menus" data-wow-delay="1s">
            <li class="bg">我的作业</li>
            <li id="myConcern">我的关注</li>
            <li id="myFans">我的粉丝</li>
            <%--<li>我的消息</li>--%>
            <li>我的收藏</li>
        </ul>
        <div class="right">
            <div class="scroll">
                <div class="tab_right">
                    <div class="ypq">
                        <div class="ypt">
                            <div class="bounceInRight yptrr" data-wow-delay="1s" id="daipigai">
                                <a href="javascript:searchHomework(1, 4)">待批改</a>
                            </div>
                            <div class="bounceInRight yptll" data-wow-delay="1s" id="yipigai">
                                <a href="javascript:searchHomeworkAlready(1, 4)">已批改</a>
                            </div>
                        </div>
                        <ul class="yp">
                            <c:forEach items="${homeworks.content}" var="homework">
                                <li class=" bounceInRight ypzy" data-wow-delay="1.2s">
                                    <div class="ypl">
                                        <img src="${homework.coverImg}" class="img_a"/>
                                    </div>
                                    <div class="ypr ">
                                        <div class="ypry">
                                            <div class="ypryl">${homework.typeStr}:${homework.workName}</div>
                                            <div class="ypryr ypryr_a">
                                                <a href="${pageContext.request.contextPath}/user/homework/updateNotCommentHomeworkByStudentPre?homeworkId=${homework.id}">修改</a>
                                                | <a href="javascript:del(${homework.id})">删除</a>
                                            </div>
                                        </div>
                                        <div class="ypre ypre_a">
                                            <span>提交人：${student.name}</span>
                                            <span>提交时间：${homework.timeStr}</span>
                                        </div>
                                        <div class="ypri">
                                            <div class="yprir" id="dududu">
                                                <div class="ypril_a_a">${homework.tags}</div>
                                                <input type="hidden" value="${homework.content}" id="content">
                                                <div class="ypril_a_a">${homework.typeStr}</div>
                                            </div>
                                        </div>
                                        <div class="yprw">
                                            <div class="yprwl">作业介绍：</div>
                                            <div class="yprwr_a">
                                                    ${homework.instruction}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                    <div class="box-footer fenye">
                        <div class="pull-left">
                            <div class="form-group form-inline">
                                总共<span id="studentPage"></span>页，共<span id="studentSize"></span>条数据。
                            </div>
                        </div>
                        <div>
                            <ul id="pageLimit" style="margin: 0px"></ul>
                        </div>
                    </div>
                    <script src="${pageContext.request.contextPath}/user/js/jquery.min_b.js"
                            type="text/javascript"></script>
                    <script src="${pageContext.request.contextPath}/user/js/zxf_page.js"
                            type="text/javascript"></script>
                    <!--fycj-->
                </div>
                <div class="tab_right">
                    <div class="tm_1">关注</div>
                    <hr/>
                    <ul class="ta_1" id="showConcern">

                    </ul>
                    <div class="box-footer fenye">
                        <div class="pull-left">
                            <div class="form-group form-inline">
                                总共<span id="pageConcern"></span>页，共<span id="countConcern"></span>条数据。
                            </div>
                        </div>
                        <div>
                            <ul id="pageLimitConcern" style="margin: 0px"></ul>
                        </div>
                    </div>
                </div>
                <div class="tab_right">
                    <div class="tm_1">粉丝</div>
                    <hr/>
                    <ul class="ta_1" id="showFans">

                    </ul>
                    <div class="box-footer fenye">
                        <div class="pull-left">
                            <div class="form-group form-inline">
                                总共<span id="pageFans"></span>页，共<span id="countFans"></span>条数据。
                            </div>
                        </div>
                        <div>
                            <ul id="pageLimitFans" style="margin: 0px"></ul>
                        </div>
                    </div>
                </div>
                <%--<div class="tab_right">--%>
                    <%--<div class="tm_1">消息</div>--%>
                    <%--<hr/>--%>
                    <%--<ul class="ta_1">--%>
                        <%--<li class="xinx">--%>
                            <%--<div class="ljt">--%>
                                <%--<div class="lj"></div>--%>
                            <%--</div>--%>
                            <%--<div class="tx3">--%>
                                <%--<div class="tx2"><img--%>
                                        <%--src="${pageContext.request.contextPath}/user/image_page/wzt_2.jpg"/></div>--%>
                                <%--<div class="xb">1</div>--%>
                            <%--</div>--%>
                            <%--<div class="wznr">--%>
                                <%--<div class="bt2"><strong>王小源老师</strong>&nbsp;&nbsp;--%>
                                    <%--<span>与你的私信</span>&nbsp;&nbsp;--%>
                                    <%--<a>08/29&nbsp;11:05</a></div>--%>
                                <%--<p>8月15日上传的作品【品牌海报设计】已完成批改，批改分为90分，星级评分4颗星，点评内容如下：明暗对比强烈、主体物突出</p>--%>

                            <%--</div>--%>
                        <%--</li>--%>
                        <%--<li class="xinx">--%>
                            <%--<div class="ljt">--%>
                                <%--<div class="lj"></div>--%>
                            <%--</div>--%>
                            <%--<div class="tx3">--%>
                                <%--<div class="tx2"><img--%>
                                        <%--src="${pageContext.request.contextPath}/user/image_page/wzt_2.jpg"/></div>--%>

                            <%--</div>--%>
                            <%--<div class="wznr">--%>
                                <%--<div class="bt2"><strong>王小源老师</strong>&nbsp;&nbsp;--%>
                                    <%--<span>与你的私信</span>&nbsp;&nbsp;--%>
                                    <%--<a>08/10&nbsp;17:12</a></div>--%>
                                <%--<p>8月03日上传的作品【宠物APP项目策划方案】已完成批改，批改分为85分，星级评分3.5颗星，点评内容如下：主体物突出、色彩干净、点缀色合理</p>--%>

                            <%--</div>--%>
                            <%--<!--  <div class="jt1">--%>
                                  <%--<img src="${pageContext.request.contextPath}/user/image_page/right.png"/>--%>
                              <%--</div>-->--%>
                        <%--</li>--%>
                        <%--<li class="xinx">--%>
                            <%--<div class="ljt">--%>
                                <%--<div class="lj"></div>--%>
                            <%--</div>--%>
                            <%--<div class="tx3">--%>
                                <%--<div class="tx2"><img src="${pageContext.request.contextPath}/user/image_page/tx2.jpg"/>--%>
                                <%--</div>--%>
                            <%--</div>--%>
                            <%--<div class="wznr">--%>
                                <%--<div class="bt2"><strong>智云管家</strong>&nbsp;&nbsp;--%>
                                    <%--<span>通知</span>&nbsp;&nbsp;--%>
                                    <%--<a>07/18&nbsp;08:05</a></div>--%>
                                <%--<p>--%>
                                    <%--本阶段成绩优秀，各方面都完成的很好，得到老师的赞扬，恭喜你完成平面软件阶段的学习，于本月25日将进入下一阶段【APP项目制作】的学习。但也有不足之处，望下阶段好好努力。</p>--%>
                            <%--</div>--%>
                            <%--<!-- <div class="jt1">--%>
                                 <%--<img src="${pageContext.request.contextPath}/user/image_page/right.png"/>--%>
                             <%--</div>-->--%>
                        <%--</li>--%>
                    <%--</ul>--%>
                <%--</div>--%>
                <div class="tab_right">
                    <div class="tm_1">收藏</div>
                    <hr/>
                    <div>
                        <ul class="ta_1" id="collection">
                        </ul>
                    </div>

                    <div class="box-footer fenye" id="collectionFenYe">
                        <div class="pull-left">
                            <div class="form-group form-inline" id="fenYeContent">

                            </div>
                        </div>
                        <div id="yeShu">
                            <ul id="pageLimit1" style="margin: 0px"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<script src="${pageContext.request.contextPath}/user/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/user/assets/index.js"></script>
<!--bottom-->
<jsp:include page="bottom.jsp"></jsp:include>
<%--<script src="${pageContext.request.contextPath}/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>--%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/admin/plugins/bootstrap/css/bootstrap.min.css">
<script src="${pageContext.request.contextPath}/admin/js/bootstrap-paginator.js"></script>
<script src="${pageContext.request.contextPath}/user/js/jqthumb.min.js"></script>
<script type="text/javascript">
    function imgNotChange() {
        // 图片不变形配置
        $('.img_a').jqthumb({
            classname: 'jqthumb',
            width: 210,
            height: 227,
            showoncomplete: true
        });
    }

    //查看我的收藏
    $(function () {
        $.get("${pageContext.request.contextPath}/user/collection/findCollectionByStudentId", function (collections) {
            var str = "";
            var content = collections.content;
            for (var i = 0; i < content.length; i++) {
                str += ' <li class="xyzp scj" >\n' +
                    // '                                <div class="ljt3">\n' +
                    // '                                    <div class="lj" onclick="cancel(' + content[i].homework.id + ')"></div>\n' +
                    // '                                </div>\n' +
                    '                                <div class="xyt" onclick="collectionDesc(' + content[i].homework.id + ')">\n' +
                    '                                    <img class = "img_a" src="${pageContext.request.contextPath}' + content[i].homework.coverImg + '"/>\n' +
                    '                                </div>\n' +
                    '                                <div class="xylb" >\n' +
                    '                                    <div class="xyxq scz" onclick="collectionDesc(' + content[i].homework.id + ')">\n' +
                    '                                        <h2>' + content[i].homework.workName + '</h2>\n' +
                    '                                        <div class="xq">\n' +
                    '                                            <ul class="dz">\n' +
                    '                                                <li class="dz_1" >\n' +
                    '                                                    <img   src="${pageContext.request.contextPath}/user/image_page/yanjing.png"/>\n' +
                    '                                                    <p>' + content[i].homework.clicks + '</p>\n' +
                    '                                                </li>\n' +
                    '                                                <li class="dz_1">\n' +
                    '                                                    <img src="${pageContext.request.contextPath}/user/image/shoucangxiao.png"/>\n' +
                    '                                                    <p>' + content[i].homework.collections + '</p>\n' +
                    '                                                </li>\n' +
                    '                                                <li class="dz_1">\n' +
                    '                                                    <img src="${pageContext.request.contextPath}/user/image_page/dianzan.png"/>\n' +
                    '                                                    <p>' + content[i].homework.thumbsUp + '</p>\n' +
                    '                                                </li>\n' +
                    '                                            </ul>\n' +
                    '                                            <div class="sc1">' + content[i].homework.tags + '</div>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="tx">\n' +
                    '                                        <div class="tx1">\n' +
                    '                                            <div class="tx_1">\n' +
                    <%--'                                                <c:if test="${content[i].homework.students[0].header == 'http://192.168.6.16:81/zhiyun/upload//user/image/headerImg.jpg'}">\n' +--%>
                    <%--'                                                    <img class="img_a" src="${pageContext.request.contextPath}/user/image/headerImg.jpg"/>\n' +--%>
                    <%--'                                                </c:if>\n' +--%>
                    <%--'                                                <c:if test="${content[i].homework.students[0].header != 'http://192.168.6.16:81/zhiyun/upload//user/image/headerImg.jpg'}">\n' +--%>
                    '                                                    <img class="img_a" src="${pageContext.request.contextPath}' + content[i].homework.students[0].header + '"/>\n' +
                    <%--'                                                </c:if>\n' +--%>
                    '                                            </div>\n' +
                    '                                            <p class="li_1 sc_1">' + content[i].homework.students[0].name + '</p>\n' +
                    '                                         <button id="btn"  onclick="cancel(' + content[i].homework.id + ')">取消收藏</button>' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                </div>\n' +
                    '                            </li>';
            }
            imgNotChange();
            $("#collection").html(str);
            $("#fenYeContent").html("总共" + collections.totalPages + "页，共" + collections.totalElements + "条数据。")
            $('#pageLimit1').bootstrapPaginator({
                currentPage: collections.number + 1,//当前页。
                totalPages: collections.totalPages,//总页数。
                bootstrapMajorVersion: 3,//当前版本
                numberOfPages: 10,//显示的页数
                tooltipTitles: function () {
                },// 鼠标移入不显示样式
                itemTexts: function (type, page, current) {//自定义按钮文本。
                    switch (type) {
                        case "first":
                            return "首页";
                        case "prev":
                            return "上一页";
                        case "next":
                            return "下一页";
                        case "last":
                            return "末页";
                        case "page":
                            return page;
                    }
                },
                onPageClicked: function (event, originalEvent, type, page) {//给按钮绑定事件，page为按钮上的数字。
                    scroll(0, 0);
                    $.get(
                        "${pageContext.request.contextPath}/user/collection/findCollectionByStudentId",
                        {"pageNum": page},
                        function (collections) {
                            var str = "";
                            var content = collections.content;
                            for (var i = 0; i < content.length; i++) {
                                str += ' <li class="xyzp scj" >\n' +
                                    // '                                <div class="ljt3">\n' +
                                    // '                                    <div class="lj" onclick="cancel(' + content[i].homework.id + ')"></div>\n' +
                                    // '                                </div>\n' +
                                    '                                <div class="xyt" onclick="collectionDesc(' + content[i].homework.id + ')">\n' +
                                    '                                    <img class="img_a" src="${pageContext.request.contextPath}' + content[i].homework.coverImg + '"/>\n' +
                                    '                                </div>\n' +
                                    '                                <div class="xylb" >\n' +
                                    '                                    <div class="xyxq scz" onclick="collectionDesc(' + content[i].homework.id + ')">\n' +
                                    '                                        <h2>' + content[i].homework.workName + '</h2>\n' +
                                    '                                        <div class="xq">\n' +
                                    '                                            <ul class="dz">\n' +
                                    '                                                <li class="dz_1">\n' +
                                    '                                                    <img src="${pageContext.request.contextPath}/user/image_page/yanjing.png"/>\n' +
                                    '                                                    <p>' + content[i].homework.clicks + '</p>\n' +
                                    '                                                </li>\n' +
                                    '                                                <li class="dz_1">\n' +
                                    '                                                    <img src="${pageContext.request.contextPath}/user/image/shoucangxiao.png"/>\n' +
                                    '                                                    <p>' + content[i].homework.collections + '</p>\n' +
                                    '                                                </li>\n' +
                                    '                                                <li class="dz_1">\n' +
                                    '                                                    <img src="${pageContext.request.contextPath}/user/image_page/dianzan.png"/>\n' +
                                    '                                                    <p>' + content[i].homework.thumbsUp + '</p>\n' +
                                    '                                                </li>\n' +
                                    '                                            </ul>\n' +
                                    '                                            <div class="sc1">' + content[i].homework.tags + '</div>\n' +
                                    '                                        </div>\n' +
                                    '                                    </div>\n' +
                                    '                                    <div class="tx">\n' +
                                    '                                        <div class="tx1">\n' +
                                    '                                            <div class="tx_1">\n' +
                                    <%--'                                                <c:if test="${content[i].homework.students[0].header == null}">\n' +--%>
                                    <%--'                                                    <img class="img_a" src="${pageContext.request.contextPath}/user/image_page/guanzhu/sc1_1.jpg"/>\n' +--%>
                                    <%--'                                                </c:if>\n' +--%>
                                    <%--'                                                <c:if test="${content[i].homework.students[0].header != null}">\n' +--%>
                                    '                                                    <img class="img_a" src="${pageContext.request.contextPath}' + content[i].homework.students[0].header + '"/>\n' +
                                    <%--'                                                </c:if>\n' +--%>
                                    '                                            </div>\n' +
                                    '                                            <p class="li_1 sc_1">' + content[i].homework.students[0].name + '</p>\n' +
                                    '                                            <button id="btn"  onclick="cancel(' + content[i].homework.id + ')">取消收藏</button>' +
                                    '                                        </div>\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                            </li>';

                            }
                            str += "<input type='hidden' id='pageNum' value=" + page + ">"
                            $("#collection").html(str);

                        })
                }
            });
        })
    })

    function changePageSize() {
        //获取下拉框的值
        var pageSize = $("#changePageSize").val();
        //向服务器发送请求，改变每页显示条数
        location.href = "${pageContext.request.contextPath}/user/homework/findNotCommentHomeworkByStudent?studentId=${student.id}&pageNum=1&pageSize=" + pageSize;
    }

    $("li").css("cursor", "pointer");

    function x() {
        var yourString = $("#content").val();
        var result = yourString.split(",");
        for (var i = 0; i < result.length; i++) {
            $("#dududu").append("<div class=\"ypril_a_a\" >" + result[i] + "</div>")
        }
        imgNotChange();
    }

    $(function () {
        var yourString = $("#content").val();
        var result = yourString.split(",");
        for (var i = 0; i < result.length; i++) {
            $("#dududu").append("<div class=\"ypril_a_a\" >" + result[i] + "</div>")
        }
        imgNotChange();
        searchHomework(1, 4);
        $("#daipigai a").click(function () {
            $("#daipigai").addClass("yptrr");
            $("#daipigai").removeClass("yptll");

            $("#yipigai").addClass("yptll");
            $("#yipigai").removeClass("yptrr");
        })

        $("#yipigai a").click(function () {
            $("#yipigai").addClass("yptrr");
            $("#yipigai").removeClass("yptll");

            $("#daipigai").addClass("yptll");
            $("#daipigai").removeClass("yptrr");
        })
    })

    function del(id) {
        if (confirm("确定删除吗？")) {
            location.href = "${pageContext.request.contextPath}/user/homework/delNotCommentHomeworkByStudent?homeworkId=" + id;
        }
    }

    $("#myConcern").mouseover(function () {
        searchConcern(1, 9);
    })

    function searchConcern(pageNum, pageSize) {
        var id = ${student.id};
        $.get("${pageContext.request.contextPath}/user/student/findConcern",
            {"id": id, "pageNum": pageNum, "pageSize": pageSize}, function (data) {//接收一个page对象
                var str = "";
                var content = data.content;//page对象的content属性为list<Concern>
                for (var i = 0; i < content.length; i++) {//遍历集合
                    var concern = content[i];//每项为一个concern对象
                    var stu = concern.toFollow;//找到被关注列
                    var count1 = stu.homeworkLength;//该学生的作品数量
                    var fans = stu.fans;//该学生的粉丝数量
                    var classes = stu.classes;//给学生的班级
                    var className = classes.className;//班级名称
                    var school = classes.school;//学校
                    var schoolName = school.schoolName;//学校名称
                    str += '<li class="li_2">\n' +
                        '                            <div >\n' +
                        '                                <div></div>\n' +
                        '                            </div>\n' +
                        '                            <div class="tx_2"><img src="' + stu.header + '"/></div>\n' +
                        '                            <h4 class="mz">' + stu.name + '</h4>\n' +
                        '                            <h5 class="li_3">' + schoolName + '&nbsp;｜&nbsp;' + className + '\n' +
                        '                                <ul class="ul1">\n' +
                        '                                    <li><span>创作</span>&nbsp;' + count1 + '</li>\n' +
                        '                                    <li><span>粉丝</span>&nbsp;' + fans + '</li>\n' +
                        '                                </ul>\n' +
                        '                            </h5>\n' +
                        '                        </li>';
                }
                $("#showConcern").html(str);
                $("#pageConcern").html(data.totalPages);
                $("#countConcern").html(data.totalElements);
                $(".lj").click(function () {

                })
                $('#pageLimitConcern').bootstrapPaginator({
                    bootstrapMajorVersion: 3, // bootstrap版本
                    currentPage: data.number + 1, // 当前页
                    totalPages: data.totalPages, // 总页数
                    numberOfPages: 5, // 最多显示多少页
                    itemTexts: function (type, page, current) {
                        switch (type) {
                            case "first":
                                return "首页";
                            case "prev":
                                return "上一页";
                            case "next":
                                return "下一页";
                            case "last":
                                return "末页";
                            case "page":
                                return page;
                        }
                    },
                    onPageClicked: function (event, originalEvent, type, page) { // 点击页码执行的方法
                        searchConcern(page, 9);
                        window.scrollTo(0, 0);
                    }
                });
            }, "json")
    }

    $("#myFans").mouseover(function () {
        searchFans(1, 9);
    })

    function searchFans(pageNum, pageSize) {
        var id = ${student.id};
        $.get("${pageContext.request.contextPath}/user/student/findFans",
            {"id": id, "pageNum": pageNum, "pageSize": pageSize}, function (data) {
                // console.log(data);
                var concernPage = data.concernPage;
                var str = "";
                var concernVoList = data.concernVoList;//page对象的content属性为list<Concern>

                for (var i = 0; i < concernVoList.length; i++) {//遍历集合
                    var concernVo = concernVoList[i];//每项为一个concernVo对象
                    var concern = concernVo.concern;
                    var stu = concern.follow;//找到关注列
                    var classes = stu.classes;//给学生的班级
                    var className = classes.className;//班级名称
                    var school = classes.school;//学校
                    var schoolName = school.schoolName;//学校名称
                    if (concernVo.status == true) {//已关注
                        str += '<li class="fs">\n' +
                            '                            <div class="tb txt">\n' +
                            '                                <img src="' + stu.header + '"/>\n' +
                            '                            </div>\n' +
                            '                            <h3 class="zbs h3_1">' + stu.nickName + '</h3>\n' +
                            '                            <ul class="ul2_1">\n' +
                            '                                <li class="uli">\n' +
                            '                                    <h5>作品</h5>\n' +
                            '                                    <h2>' + stu.homeworkLength + '</h2>\n' +
                            '                                </li>\n' +
                            '                                <li class="uli_1">\n' +
                            '                                    <h5>粉丝</h5>\n' +
                            '                                    <h2>' + stu.fans + '</h2>\n' +
                            '                                </li>\n' +
                            '                                <h4></h4>\n' +
                            '                            </ul>\n' +
                            '                            <h5 class="jj1">' + schoolName + '｜' + className + '\n' +
                            '                                <ul class="gz sx">\n' +
                            '                                    <li class="li31 toConcern" value="' + stu.id + '">已关注</li>\n' +
                            '                                    <li class="li2">私信</li>\n' +
                            '                                </ul>\n' +
                            '                            </h5>\n' +
                            '                        </li>';
                    } else {//未关注
                        str += '<li class="fs">\n' +
                            '                            <div class="tb txt">\n' +
                            '                                <img src="' + stu.header + '"/>\n' +
                            '                            </div>\n' +
                            '                            <h3 class="zbs h3_1">' + stu.nickName + '</h3>\n' +
                            '                            <ul class="ul2_1">\n' +
                            '                                <li class="uli">\n' +
                            '                                    <h5>作品</h5>\n' +
                            '                                    <h2>' + stu.homeworkLength + '</h2>\n' +
                            '                                </li>\n' +
                            '                                <li class="uli_1">\n' +
                            '                                    <h5>粉丝</h5>\n' +
                            '                                    <h2>' + stu.fans + '</h2>\n' +
                            '                                </li>\n' +
                            '                                <h4></h4>\n' +
                            '                            </ul>\n' +
                            '                            <h5 class="jj1">' + schoolName + '｜' + className + '\n' +
                            '                                <ul class="gz sx">\n' +
                            '                                    <li class="li3 toConcern" value="' + stu.id + '" >关注</li>\n' +
                            '                                    <li class="li2">私信</li>\n' +
                            '                                </ul>\n' +
                            '                            </h5>\n' +
                            '                        </li>';
                    }

                }
                $("#showFans").html(str);
                $("#pageFans").html(concernPage.totalPages);
                $("#countFans").html(concernPage.totalElements);

                $('#pageLimitFans').bootstrapPaginator({
                    bootstrapMajorVersion: 3, // bootstrap版本
                    currentPage: concernPage.number + 1, // 当前页
                    totalPages: concernPage.totalPages, // 总页数
                    numberOfPages: 5, // 最多显示多少页
                    itemTexts: function (type, page, current) {
                        switch (type) {
                            case "first":
                                return "首页";
                            case "prev":
                                return "上一页";
                            case "next":
                                return "下一页";
                            case "last":
                                return "末页";
                            case "page":
                                return page;
                        }
                    },
                    onPageClicked: function (event, originalEvent, type, page) { // 点击页码执行的方法
                        searchFans(page, 9);
                        window.scrollTo(0, 0);
                    }
                });
                $(".toConcern").click(function () {
                    var follow = ${sessionScope.student.id};
                    var toFollow = $(this).val();
                    var html = $(this).html();
                    if (html == "关注") {
                        //改变文字
                        $(this).html("已关注")
                        $(this).removeClass("li3");
                        $(this).addClass("li31");
                        //发送ajax请求
                        $.get("${pageContext.request.contextPath}/user/student/concern", {
                            "follow": follow,
                            "toFollow": toFollow
                        }, function (data) {

                            // $(this).attr("class",)
                        }, "json")
                    } else {
                        //改变文字
                        $(this).html("关注")
                        $(this).removeClass("li31");
                        $(this).addClass("li3");
                        //发送ajax请求
                        $.get("${pageContext.request.contextPath}/user/student/cancel", {
                            "follow": follow,
                            "toFollow": toFollow
                        }, function (data) {

                        }, "json")
                    }
                })
            }, "json")
    }

    function cancel(to_follow) {
        var follow = ${empty sessionScope.student.id ? 0 : sessionScope.student.id};
        var toFollow = to_follow;
        $.get("${pageContext.request.contextPath}/user/student/cancel", {
            "follow": follow,
            "toFollow": toFollow
        }, function (data) {
            location.reload();
        }, "json")
    }

    // 待批改
    function searchHomework(page, pageSize) {
        scroll(0, 0);
        $.get("${pageContext.request.contextPath}/user/homework/findNotCommentHomeworkByStudentYiBu",
            {"studentId":${student.id}, "pageNum": page, "pageSize": pageSize},
            function (data) {
                $(".yp").html("")
                var data2 = eval(data).content;
                for (var i in data2) {
                    $(".yp").append(" <li class=\"bounceInRight ypzy\" data-wow-delay=\"0.2s\">\n" +
                        "                <div class=\"ypl\">\n" +
                        "                <img src=" + data2[i].coverImg + " class=\"img_a\"/>\n" +
                        "                </div>\n" +
                        "                <div class=\"ypr \">\n" +
                        "                <div class=\"ypry\">\n" +
                        "                <div class=\"ypryl\"><a>" + data2[i].typeStr + ":" + data2[i].workName + "</a></div>\n" +
                        "            <div class=\"ypryr ypryr_a\">" +
                        "<a href='${pageContext.request.contextPath}/user/homework/updateNotCommentHomeworkByStudentPre?homeworkId=" + data2[i].id + "'>修改</a>\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                <div class=\"ypre ypre_a\">\n" +
                        "                <span>提交人：${student.name}</span>\n" +
                        "            <span>提交时间：" + data2[i].timeStr + "</span>\n" +
                        "            </div>\n" +
                        "\n" +
                        "            <div class=\"ypri\">\n" +
                        "\n" +
                        "                <div class=\"yprir\" id=\"dududu\">\n" +
                        "                <div class=\"ypril_a_a\">" + data2[i].tags + "</div>\n" +
                        "                <input type=\"hidden\" value='" + data2[i].content + "'id=\"content\">\n" +
                        "                <div class=\"ypril_a_a\">" + data2[i].typeStr + "</div>\n" +
                        "\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                <div class=\"yprw\">\n" +
                        "                <div class=\"yprwl\">作业介绍：</div>\n" +
                        "            <div class=\"yprwr_a\">\n" +
                        "                " + data2[i].instruction + "\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                </li>")
                }
                x();
                $("#studentPage").html(data.totalPages);
                $("#studentSize").html(data.totalElements);
                $('#pageLimit').bootstrapPaginator({
                    currentPage: data.number + 1,//当前页。
                    totalPages: data.totalPages,//总页数。
                    bootstrapMajorVersion: 3,//当前版本
                    numberOfPages: 10,//显示的页数
                    tooltipTitles: function () {
                    },// 鼠标移入不显示样式
                    itemTexts: function (type, page, current) {//自定义按钮文本。
                        switch (type) {
                            case "first":
                                return "首页";
                            case "prev":
                                return "上一页";
                            case "next":
                                return "下一页";
                            case "last":
                                return "末页";
                            case "page":
                                return page;
                        }
                    },
                    onPageClicked: function (event, originalEvent, type, page) {//给按钮绑定事件，page为按钮上的数字。
                        searchHomework(page, pageSize)
                    }
                });
            }
        );
    }

    // 已批改
    function searchHomeworkAlready(page, pageSize) {
        scroll(0, 0);
        $.get("${pageContext.request.contextPath}/user/homework/findAlreadyCommentHomeworkByStudentYiBu",
            {"studentId":${student.id}, "pageNum": page, "pageSize": pageSize},
            function (data) {
                $(".yp").html("");
                var data2 = eval(data).content;
                for (var i in data2) {
                    var str = " <li class=\"bounceInRight ypzy\" data-wow-delay=\"0.2s\" style=\"height: 266px\">\n" +
                        "                <div class=\"ypl\">\n" +
                        "                <img src=" + data2[i].coverImg + " class=\"img_a\"/>\n" +
                        "                </div>\n" +
                        "                <div class=\"ypr \">\n" +
                        "                <div class=\"ypry\">\n" +
                        "                <div class=\"ypryl\"><a>" + data2[i].typeStr + ":" + data2[i].workName + "</a>\n" +
                        "                </div>\n" +
                        "                <div class=\"ypryr\">云值：\n" +
                        "                <span>" + data2[i].score + "</span>分\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                <div class=\"ypre ypre_a\">\n" +
                        "                <span>批改人：" + data2[i].teacher.name + "</span>\n" +
                        "                <span>批改时间：" + data2[i].commentTimeStr + "</span>\n" +
                        "                </div>" +
                        "                <div class=\"yprs\">\n" +
                        "                <div class=\"yprsl\">星级点评：</div>\n" +
                        "                <div class=\"yprsr\">\n";
                    for (j = 0; j < data2[i].star; j++) {
                        str += "<img src='${pageContext.request.contextPath}/user/image/ypzy_xx.jpg' no-repeat 80px0px>"
                    }
                    str +=
                        "                 </div>\n" +
                        "                  </div>\n" +
                        "            <div class=\"ypri\">\n" +
                        "                <div class=\"yprir\" id=\"dududu\">\n" +
                        "                <div class=\"ypril_a_a\">" + data2[i].tags + "</div>\n" +
                        "                <input type=\"hidden\" value='" + data2[i].content + "'id=\"content\">\n" +
                        "                <div class=\"ypril_a_a\">" + data2[i].typeStr + "</div>\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                <span class=\"yprw\">\n" +
                        "                <div class=\"yprwl\">教师点评：</div>\n" +
                        "                <span>\n" +
                        "                " + data2[i].descEvaluate + "\n" +
                        "                </span>\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                </li>";
                    $(".yp").append(str)
                }
                x();
                $("#studentPage").html(data.totalPages);
                $("#studentSize").html(data.totalElements);
                $('#pageLimit').bootstrapPaginator({
                    currentPage: data.number + 1,//当前页。
                    totalPages: data.totalPages,//总页数。
                    bootstrapMajorVersion: 3,//当前版本
                    numberOfPages: 10,//显示的页数
                    tooltipTitles: function () {
                    },// 鼠标移入不显示样式
                    itemTexts: function (type, page, current) {//自定义按钮文本。
                        switch (type) {
                            case "first":
                                return "首页";
                            case "prev":
                                return "上一页";
                            case "next":
                                return "下一页";
                            case "last":
                                return "末页";
                            case "page":
                                return page;
                        }
                    },
                    onPageClicked: function (event, originalEvent, type, page) {//给按钮绑定事件，page为按钮上的数字。
                        searchHomeworkAlready(page, pageSize)
                    }
                });
            }
        );
    }

    //查看收藏的作业详情
    function collectionDesc(id) {
        location.href = "/user/jobdetails/message?id=" + id;
    }

    //取消收藏
    function cancel(id) {
        var val = $("#pageNum").val();
        $.get("/user/collection/cancelCollection", {"homeworkId": id, "pageNum": val}, function (collections) {
            var str = "";
            var content = collections.content;
            for (var i = 0; i < content.length; i++) {
                str += ' <li class="xyzp scj" >\n' +
                    '                                <div class="xyt" onclick="collectionDesc(' + content[i].homework.id + ')">\n' +
                    '                                    <img class = "img_a" src="${pageContext.request.contextPath}' + content[i].homework.coverImg + '"/>\n' +
                    '                                </div>\n' +
                    '                                <div class="xylb" >\n' +
                    '                                    <div class="xyxq scz" onclick="collectionDesc(' + content[i].homework.id + ')">\n' +
                    '                                        <h2>' + content[i].homework.workName + '</h2>\n' +
                    '                                        <div class="xq">\n' +
                    '                                            <ul class="dz">\n' +
                    '                                                <li class="dz_1" >\n' +
                    '                                                    <img   src="${pageContext.request.contextPath}/user/image_page/yanjing.png"/>\n' +
                    '                                                    <p>' + content[i].homework.clicks + '</p>\n' +
                    '                                                </li>\n' +
                    '                                                <li class="dz_1">\n' +
                    '                                                    <img src="${pageContext.request.contextPath}/user/image/shoucangxiao.png"/>\n' +
                    '                                                    <p>' + content[i].homework.collections + '</p>\n' +
                    '                                                </li>\n' +
                    '                                                <li class="dz_1">\n' +
                    '                                                    <img src="${pageContext.request.contextPath}/user/image_page/dianzan.png"/>\n' +
                    '                                                    <p>' + content[i].homework.thumbsUp + '</p>\n' +
                    '                                                </li>\n' +
                    '                                            </ul>\n' +
                    '                                            <div class="sc1">' + content[i].homework.tags + '</div>\n' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                    <div class="tx">\n' +
                    '                                        <div class="tx1">\n' +
                    '                                            <div class="tx_1">\n' +
                    <%--'                                                <c:if test="${content[i].homework.students[0].header == null}">\n' +--%>
                    <%--'                                                    <img class="img_a" src="${pageContext.request.contextPath}/user/image_page/guanzhu/sc1_1.jpg"/>\n' +--%>
                    <%--'                                                </c:if>\n' +--%>
                    <%--'                                                <c:if test="${content[i].homework.students[0].header != null}">\n' +--%>
                    '                                                    <img class="img_a" src="${pageContext.request.contextPath}' + content[i].homework.students[0].header + '"/>\n' +
                    <%--'                                                </c:if>\n' +--%>
                    '                                            </div>\n' +
                    '                                            <p class="li_1 sc_1">' + content[i].homework.students[0].name + '</p>\n' +
                    '                                             <button id="btn"  onclick="cancel(' + content[i].homework.id + ')">取消收藏</button>' +
                    '                                        </div>\n' +
                    '                                    </div>\n' +
                    '                                </div>\n' +
                    '                            </li>';
            }
            imgNotChange();
            $("#collection").html(str);
            $("#fenYeContent").html("总共" + collections.totalPages + "页，共" + collections.totalElements + "条数据。");
            $('#pageLimit1').bootstrapPaginator({
                currentPage: collections.number + 1,//当前页。
                totalPages: collections.totalPages,//总页数。
                bootstrapMajorVersion: 3,//当前版本
                numberOfPages: 10,//显示的页数
                tooltipTitles: function () {
                },// 鼠标移入不显示样式
                itemTexts: function (type, page, current) {//自定义按钮文本。
                    switch (type) {
                        case "first":
                            return "首页";
                        case "prev":
                            return "上一页";
                        case "next":
                            return "下一页";
                        case "last":
                            return "末页";
                        case "page":
                            return page;
                    }
                },
                onPageClicked: function (event, originalEvent, type, page) {//给按钮绑定事件，page为按钮上的数字。
                    scroll(0, 0);
                    $.get(
                        "${pageContext.request.contextPath}/user/collection/findCollectionByStudentId",
                        {"pageNum": page},
                        function (collections) {
                            var str = "";
                            var content = collections.content;
                            for (var i = 0; i < content.length; i++) {

                                str += ' <li class="xyzp scj" >\n' +
                                    '                                <div class="xyt" onclick="collectionDesc(' + content[i].homework.id + ')">\n' +
                                    '                                    <img class="img_a" src="${pageContext.request.contextPath}' + content[i].homework.coverImg + '"/>\n' +
                                    '                                </div>\n' +
                                    '                                <div class="xylb" >\n' +
                                    '                                    <div class="xyxq scz" onclick="collectionDesc(' + content[i].homework.id + ')">\n' +
                                    '                                        <h2>' + content[i].homework.workName + '</h2>\n' +
                                    '                                        <div class="xq">\n' +
                                    '                                            <ul class="dz">\n' +
                                    '                                                <li class="dz_1">\n' +
                                    '                                                    <img src="${pageContext.request.contextPath}/user/image_page/yanjing.png"/>\n' +
                                    '                                                    <p>' + content[i].homework.clicks + '</p>\n' +
                                    '                                                </li>\n' +
                                    '                                                <li class="dz_1">\n' +
                                    '                                                    <img src="${pageContext.request.contextPath}/user/image/shoucangxiao.png"/>\n' +
                                    '                                                    <p>' + content[i].homework.collections + '</p>\n' +
                                    '                                                </li>\n' +
                                    '                                                <li class="dz_1">\n' +
                                    '                                                    <img src="${pageContext.request.contextPath}/user/image_page/dianzan.png"/>\n' +
                                    '                                                    <p>' + content[i].homework.thumbsUp + '</p>\n' +
                                    '                                                </li>\n' +
                                    '                                            </ul>\n' +
                                    '                                            <div class="sc1">' + content[i].homework.tags + '</div>\n' +
                                    '                                        </div>\n' +
                                    '                                    </div>\n' +
                                    '                                    <div class="tx">\n' +
                                    '                                        <div class="tx1">\n' +
                                    '                                            <div class="tx_1">\n' +
                                    <%--'                                                <c:if test="${content[i].homework.students[0].header == null}">\n' +--%>
                                    <%--'                                                    <img class="img_a" src="${pageContext.request.contextPath}/user/image_page/guanzhu/sc1_1.jpg"/>\n' +--%>
                                    <%--'                                                </c:if>\n' +--%>
                                    <%--'                                                <c:if test="${content[i].homework.students[0].header != null}">\n' +--%>
                                    '                                                    <img class="img_a" src="${pageContext.request.contextPath}' + content[i].homework.students[0].header + '"/>\n' +
                                    <%--'                                                </c:if>\n' +--%>
                                    '                                            </div>\n' +
                                    '                                            <p class="li_1 sc_1">' + content[i].homework.students[0].name + '</p>\n' +
                                    '                                            <button id="btn"  onclick="cancel(' + content[i].homework.id + ')">取消收藏</button>' +
                                    '                                        </div>\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                            </li>';
                            }
                            str += "<input type='hidden' id='pageNum' value=" + page + "/>"
                            $("#collection").html(str);
                        })
                }
            });
        })
    }
</script>

</body>
</html>

