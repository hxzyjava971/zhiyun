<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>排行榜</title>
    <!--作业轮播-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/jquery-1.7.2.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/script.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/style_zylb.css"/>
    <!--作业轮播--
    <!--动画效果-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/animate.css" type="text/css">
    <!--动画效果-->
    <link href="${pageContext.request.contextPath}/user/css/style-a.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<!--动画效果-->
<script src="${pageContext.request.contextPath}/user/js/wow.min.js"></script>
<script>
    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))) {
        new WOW().init();
    }
    ;
</script>
<!--动画效果-->
<!--tou-->
<jsp:include page="top.jsp"></jsp:include>
<!--ba-->
<div class="ba">
    <img src="${pageContext.request.contextPath}/user/image/banner.gif">
</div>
<!--ph-->
<div class="phq">
    <div class="phxq">
        <div class="phxy">
            <div class="phxl">
                <a class="wow bounceInLeft phsx" data-wow-delay="0.2s">筛选：</a>
                <a class="wow lightSpeedIn phsxxl" data-wow-delay="0.3s">太原校区</a>
                <a class="wow lightSpeedIn phsxxl_2" data-wow-delay="0.4s">UI</a>
                <a class="wow lightSpeedIn phsxxl_3" data-wow-delay="0.5s">班级
                    <ul class="ban">
                        <li class="banmr">全部班级</li>
                        <li>UI851</li>
                        <li>UI852</li>
                        <li>UI853</li>
                        <li>UI854</li>
                        <li>UI855</li>
                        <li>UI856</li>
                    </ul>
                </a>
                <a class="wow lightSpeedIn phsxxl_4" data-wow-delay="0.6s">日期</a>

            </div>
            <div class="wow swing kssx" data-wow-iteration="2">
                <a href="${pageContext.request.contextPath}/user/paihangbang.jsp"><img
                        src="${pageContext.request.contextPath}/user/image/kssx.png"/></a>
            </div>
            <a href="${pageContext.request.contextPath}/user/paihangbang.jsp">

                <div class="wow bounceInRight phxr" data-wow-delay="0.3s"><img
                        src="${pageContext.request.contextPath}/user/image/sx.png" width="18px" height="14px"/>刷新
                </div>
            </a>
            <div class="clear"></div>
        </div>
    </div>
    <!--phb-->
    <div class="phbq">
        <ul class="phby">
            <li class="wow bounceInUp" data-wow-delay="0.5s">
                <div class="fsbt">封神榜</div>
                <div class="fsbc">
                    <div class="fsbc_a de">
                        <div class="fsbc_at">李丹阳</div>
                        <div class="fsbc_ab">云值：98</div>
                    </div>
                    <div class="fsbc_a dy">
                        <div class="fsbc_bt">武波秀</div>
                        <div class="fsbc_bb">云值：99</div>
                    </div>
                    <div class="fsbc_a ds">
                        <div class="fsbc_ct">张扬飞</div>
                        <div class="fsbc_cb">云值：97</div>
                    </div>
                </div>
                <!--phmc-->
                <div class="phmc">
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">4</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zya_1tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">郭可颂</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">5</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zya_2tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">周奕彤</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">6</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">张可欣</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">7</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zya_4tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">冯峰</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">8</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zya_5tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">吴梅</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">9</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zya_6tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">李娜娜</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz phd">
                        <div class="gzl">
                            <div class="gzsz">10</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zyb_1tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">何家明</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr"></div>
                    </div>
                </div>
                <div class="phgd"><a href="${pageContext.request.contextPath}/user/paihangbang.jsp">查看更多排名></a></div>

            </li>
            <!--rqb-->
            <li class="wow bounceInUp" data-wow-delay="0.7s">
                <div class="fsbta">人气榜</div>
                <div class="rqbc">
                    <div class="fsbc_a de">
                        <div class="fsbc_at">刘俊俊</div>
                        <div class="fsbc_ab">云值：98</div>
                    </div>
                    <div class="fsbc_a dy">
                        <div class="fsbc_bt">何旻笙</div>
                        <div class="fsbc_bb">云值：99</div>
                    </div>
                    <div class="fsbc_a ds">
                        <div class="fsbc_ct">张一凡</div>
                        <div class="fsbc_cb">云值：97</div>
                    </div>
                </div>
                <!--phmc-->
                <div class="phmc">
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">4</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zya_5tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">张馨元</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_a"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">5</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zyc_2tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">薛凯</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_a"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">6</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zyc_3tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">崔鹏鹏</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_a"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">7</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zyc_4tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">张果丽</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_a"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">8</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zyc_5tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">赵琪琪</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_a"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">9</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zyc_6tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">阎姗姗</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_a"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz phd">
                        <div class="gzl">
                            <div class="gzsz">10</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zyd_1tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">杨国志</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_a"></div>
                    </div>
                </div>
                <div class="phgda"><a href="${pageContext.request.contextPath}/user/paihangbang.jsp">查看更多排名></a></div>

            </li>
            <!--llb-->
            <li class="wow bounceInUp ll" data-wow-delay="0.9s">
                <div class="fsbtb">流量榜</div>
                <div class="llbc">
                    <div class="fsbc_a de">
                        <div class="fsbc_at">何旻笙</div>
                        <div class="fsbc_ab">云值：98</div>
                    </div>
                    <div class="fsbc_a dy">
                        <div class="fsbc_bt">郭程程</div>
                        <div class="fsbc_bb">云值：99</div>
                    </div>
                    <div class="fsbc_a ds">
                        <div class="fsbc_ct">麻天天</div>
                        <div class="fsbc_cb">云值：97</div>
                    </div>
                </div>
                <!--phmc-->
                <div class="phmc">
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">4</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zyd_1tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">宋敏</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_b"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">5</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zyd_2tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">王珂</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_b"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">6</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zyd_3tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">刘一柏</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_b"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">7</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zyd_4tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">祁欣桐</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_b"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">8</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zyd_5tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">郭甜</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_b"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz">
                        <div class="gzl">
                            <div class="gzsz">9</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zyd_6tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">李明飞</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_b"></div>
                    </div>
                    <!--phgz-->
                    <div class="phgz phd">
                        <div class="gzl">
                            <div class="gzsz">10</div>
                            <div class="gztx">
                                <img src="${pageContext.request.contextPath}/user/image/zya_1tx_02.jpg"/>
                            </div>
                            <div class="gzxx">
                                <div class="gzxx_t">何润可</div>
                                <div class="gzxx_b">云值：96</div>
                            </div>
                        </div>
                        <div class="gzr_b"></div>
                    </div>
                </div>
                <div class="phgdb"><a href="${pageContext.request.contextPath}/user/paihangbang.jsp">查看更多排名></a></div>

            </li>
        </ul>
    </div>
</div>
<!--bz-->
<div class="bzq">
    <div class="bzy">
        <div class="bzt">
            <div class="wow rollIn bztl">榜主作业欣赏</div>
            <a class="wow bounceInRight bztr" href="${pageContext.request.contextPath}/user/zuoye.jsp">查看更多></a>
        </div>
        <!--sxzy-->
        <!--作业轮播-->
        <div class="Div1">

            <b class="Div1_prev Div1_prev1"><img src="${pageContext.request.contextPath}/user/image/lizi_img011.png"
                                                 title="上一页"/></b>
            <b class="Div1_next Div1_next1"><img src="${pageContext.request.contextPath}/user/image/lizi_img012.png"
                                                 title="下一页"/></b>
            <div class="Div1_main">
                <div class="main_a">
                    <div class="Div1_main_div1">
                        <li class="bzzya">
                            <div class="bzzya_t">
                                <img src="${pageContext.request.contextPath}/user/image/bzzy_a.jpg"/>
                            </div>
                            <div class="bzzya_c">
                                <div class="bzzya_cl">
                                    <div class="bzzya_cl_t">创意海报</div>
                                    <div class="bzzya_cl_b">【太原】UI861班</div>
                                </div>
                                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
                            </div>
                            <div class="bzzya_b">
                                <div class="bzzya_bl">
                                    <div class="bzzya_bll">
                                        <img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                                    </div>
                                    <div class="bzzya_blr">张一凡</div>
                                </div>
                                <div class="bzzya_br">
                                    <div class="bzzya_br_la">4524</div>
                                    <div class="bzzya_br_lb">162</div>
                                    <div class="bzzya_br_lc">8</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>

                        </li>


                    </div>
                    <div>
                        <li class="bzzya">
                            <div class="bzzya_t">
                                <img src="${pageContext.request.contextPath}/user/image/bzzy_b.jpg"/>
                            </div>
                            <div class="bzzya_c">
                                <div class="bzzya_cl">
                                    <div class="bzzya_cl_t">创意海报</div>
                                    <div class="bzzya_cl_b">【太原】UI861班</div>
                                </div>
                                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
                            </div>
                            <div class="bzzya_b">
                                <div class="bzzya_bl">
                                    <div class="bzzya_bll">
                                        <img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                                    </div>
                                    <div class="bzzya_blr">张一凡</div>
                                </div>
                                <div class="bzzya_br">
                                    <div class="bzzya_br_la">4524</div>
                                    <div class="bzzya_br_lb">162</div>
                                    <div class="bzzya_br_lc">8</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>

                        </li>

                    </div>
                    <div>
                        <li class="bzzya">
                            <div class="bzzya_t">
                                <img src="${pageContext.request.contextPath}/user/image/bzzy_c.jpg"/>
                            </div>
                            <div class="bzzya_c">
                                <div class="bzzya_cl">
                                    <div class="bzzya_cl_t">创意海报</div>
                                    <div class="bzzya_cl_b">【太原】UI861班</div>
                                </div>
                                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
                            </div>
                            <div class="bzzya_b">
                                <div class="bzzya_bl">
                                    <div class="bzzya_bll">
                                        <img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                                    </div>
                                    <div class="bzzya_blr">张一凡</div>
                                </div>
                                <div class="bzzya_br">
                                    <div class="bzzya_br_la">4524</div>
                                    <div class="bzzya_br_lb">162</div>
                                    <div class="bzzya_br_lc">8</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>

                        </li>
                    </div>
                    <div>
                        <li class="bzzya bzzyb">
                            <div class="bzzya_t">
                                <img src="${pageContext.request.contextPath}/user/image/bzzy_d.jpg"/>
                            </div>
                            <div class="bzzya_c">
                                <div class="bzzya_cl">
                                    <div class="bzzya_cl_t">创意海报</div>
                                    <div class="bzzya_cl_b">【太原】UI861班</div>
                                </div>
                                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
                            </div>
                            <div class="bzzya_b">
                                <div class="bzzya_bl">
                                    <div class="bzzya_bll">
                                        <img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                                    </div>
                                    <div class="bzzya_blr">张一凡</div>
                                </div>
                                <div class="bzzya_br">
                                    <div class="bzzya_br_la">4524</div>
                                    <div class="bzzya_br_lb">162</div>
                                    <div class="bzzya_br_lc">8</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>

                        </li>
                    </div>
                </div>
                <div class="main_a">
                    <div>
                        </li>
                    </div>
                    <div>
                        <li class="bzzya">
                            <div class="bzzya_t">
                                <img src="${pageContext.request.contextPath}/user/image/bzzy_d.jpg"/>
                            </div>
                            <div class="bzzya_c">
                                <div class="bzzya_cl">
                                    <div class="bzzya_cl_t">创意海报</div>
                                    <div class="bzzya_cl_b">【太原】UI861班</div>
                                </div>
                                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
                            </div>
                            <div class="bzzya_b">
                                <div class="bzzya_bl">
                                    <div class="bzzya_bll">
                                        <img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                                    </div>
                                    <div class="bzzya_blr">张一凡</div>
                                </div>
                                <div class="bzzya_br">
                                    <div class="bzzya_br_la">4524</div>
                                    <div class="bzzya_br_lb">162</div>
                                    <div class="bzzya_br_lc">8</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>

                        </li>

                    </div>
                    <div>
                        <li class="bzzya">
                            <div class="bzzya_t">
                                <img src="${pageContext.request.contextPath}/user/image/bzzy_b.jpg"/>
                            </div>
                            <div class="bzzya_c">
                                <div class="bzzya_cl">
                                    <div class="bzzya_cl_t">创意海报</div>
                                    <div class="bzzya_cl_b">【太原】UI861班</div>
                                </div>
                                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
                            </div>
                            <div class="bzzya_b">
                                <div class="bzzya_bl">
                                    <div class="bzzya_bll">
                                        <img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                                    </div>
                                    <div class="bzzya_blr">张一凡</div>
                                </div>
                                <div class="bzzya_br">
                                    <div class="bzzya_br_la">4524</div>
                                    <div class="bzzya_br_lb">162</div>
                                    <div class="bzzya_br_lc">8</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>

                        </li>
                    </div>
                    <div>
                        <li class="bzzya">
                            <div class="bzzya_t">
                                <img src="${pageContext.request.contextPath}/user/image/bzzy_a.jpg"/>
                            </div>
                            <div class="bzzya_c">
                                <div class="bzzya_cl">
                                    <div class="bzzya_cl_t">创意海报</div>
                                    <div class="bzzya_cl_b">【太原】UI861班</div>
                                </div>
                                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
                            </div>
                            <div class="bzzya_b">
                                <div class="bzzya_bl">
                                    <div class="bzzya_bll">
                                        <img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                                    </div>
                                    <div class="bzzya_blr">张一凡</div>
                                </div>
                                <div class="bzzya_br">
                                    <div class="bzzya_br_la">4524</div>
                                    <div class="bzzya_br_lb">162</div>
                                    <div class="bzzya_br_lc">8</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>

                        </li>

                    </div>
                    <div>
                        <li class="bzzya bzzyb">
                            <div class="bzzya_t">
                                <img src="${pageContext.request.contextPath}/user/image/bzzy_c.jpg"/>
                            </div>
                            <div class="bzzya_c">
                                <div class="bzzya_cl">
                                    <div class="bzzya_cl_t">创意海报</div>
                                    <div class="bzzya_cl_b">【太原】UI861班</div>
                                </div>
                                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
                            </div>
                            <div class="bzzya_b">
                                <div class="bzzya_bl">
                                    <div class="bzzya_bll">
                                        <img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                                    </div>
                                    <div class="bzzya_blr">张一凡</div>
                                </div>
                                <div class="bzzya_br">
                                    <div class="bzzya_br_la">4524</div>
                                    <div class="bzzya_br_lb">162</div>
                                    <div class="bzzya_br_lc">8</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>

                        </li>

                    </div>
                </div>
                <div class="main_a">
                    <div>
                        <li class="bzzya">
                            <div class="bzzya_t">
                                <img src="${pageContext.request.contextPath}/user/image/bzzy_b.jpg"/>
                            </div>
                            <div class="bzzya_c">
                                <div class="bzzya_cl">
                                    <div class="bzzya_cl_t">创意海报</div>
                                    <div class="bzzya_cl_b">【太原】UI861班</div>
                                </div>
                                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
                            </div>
                            <div class="bzzya_b">
                                <div class="bzzya_bl">
                                    <div class="bzzya_bll">
                                        <img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                                    </div>
                                    <div class="bzzya_blr">张一凡</div>
                                </div>
                                <div class="bzzya_br">
                                    <div class="bzzya_br_la">4524</div>
                                    <div class="bzzya_br_lb">162</div>
                                    <div class="bzzya_br_lc">8</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>

                        </li>


                    </div>
                    <div>
                        <li class="bzzya">
                            <div class="bzzya_t">
                                <img src="${pageContext.request.contextPath}/user/image/bzzy_c.jpg"/>
                            </div>
                            <div class="bzzya_c">
                                <div class="bzzya_cl">
                                    <div class="bzzya_cl_t">创意海报</div>
                                    <div class="bzzya_cl_b">【太原】UI861班</div>
                                </div>
                                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
                            </div>
                            <div class="bzzya_b">
                                <div class="bzzya_bl">
                                    <div class="bzzya_bll">
                                        <img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                                    </div>
                                    <div class="bzzya_blr">张一凡</div>
                                </div>
                                <div class="bzzya_br">
                                    <div class="bzzya_br_la">4524</div>
                                    <div class="bzzya_br_lb">162</div>
                                    <div class="bzzya_br_lc">8</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>

                        </li>

                    </div>
                    <div>
                        <li class="bzzya">
                            <div class="bzzya_t">
                                <img src="${pageContext.request.contextPath}/user/image/bzzy_a.jpg"/>
                            </div>
                            <div class="bzzya_c">
                                <div class="bzzya_cl">
                                    <div class="bzzya_cl_t">创意海报</div>
                                    <div class="bzzya_cl_b">【太原】UI861班</div>
                                </div>
                                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
                            </div>
                            <div class="bzzya_b">
                                <div class="bzzya_bl">
                                    <div class="bzzya_bll">
                                        <img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                                    </div>
                                    <div class="bzzya_blr">张一凡</div>
                                </div>
                                <div class="bzzya_br">
                                    <div class="bzzya_br_la">4524</div>
                                    <div class="bzzya_br_lb">162</div>
                                    <div class="bzzya_br_lc">8</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>

                        </li>


                    </div>
                    <div>
                        </li>
                    </div>
                    <div>
                        <li class="bzzya bzzyb">
                            <div class="bzzya_t">
                                <img src="${pageContext.request.contextPath}/user/image/bzzy_d.jpg"/>
                            </div>
                            <div class="bzzya_c">
                                <div class="bzzya_cl">
                                    <div class="bzzya_cl_t">创意海报</div>
                                    <div class="bzzya_cl_b">【太原】UI861班</div>
                                </div>
                                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
                            </div>
                            <div class="bzzya_b">
                                <div class="bzzya_bl">
                                    <div class="bzzya_bll">
                                        <img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                                    </div>
                                    <div class="bzzya_blr">张一凡</div>
                                </div>
                                <div class="bzzya_br">
                                    <div class="bzzya_br_la">4524</div>
                                    <div class="bzzya_br_lb">162</div>
                                    <div class="bzzya_br_lc">8</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>
                            <div class="bzgzq">
                                <div class="bzgzb">
                                    <div class="bzgzl">关注</div>
                                    <div class="bzgzr">私信</div>
                                </div>
                            </div>

                        </li>

                    </div>
                </div>
            </div>
        </div>
        <!--作业轮播-->
        <!--<ul class="wow bounceInUp bzzy">
    	<li class="bzzya">
        	<div class="bzzya_t">
            	<img src="${pageContext.request.contextPath}/user/image/bzzy_a.jpg"/>
            </div>
            <div class="bzzya_c">
            	<div class="bzzya_cl">
                	<div class="bzzya_cl_t">创意海报</div>
                    <div class="bzzya_cl_b">【太原】UI861班</div>
                </div>
                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
            </div>
            <div class="bzzya_b">
            	<div class="bzzya_bl">
                	<div class="bzzya_bll">
                    	<img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                    </div>
                    <div class="bzzya_blr">张一凡</div>
                </div>
                <div class="bzzya_br">
                	<div class="bzzya_br_la">4524</div>
                    <div class="bzzya_br_lb">162</div>
                    <div class="bzzya_br_lc">8</div>	
                </div>
            </div>
            <div class="bzgzq">
            	<div class="bzgzb">
                	<div class="bzgzl">关注</div>
                    <div class="bzgzr">私信</div>
                </div>
            </div>
            <div class="bzgzq">
            	<div class="bzgzb">
                	<div class="bzgzl">关注</div>
                    <div class="bzgzr">私信</div>
                </div>
            </div>
            
        </li>
        <li class="bzzya">
        	<div class="bzzya_t">
            	<img src="${pageContext.request.contextPath}/user/image/bzzy_b.jpg"/>
            </div>
            <div class="bzzya_c">
            	<div class="bzzya_cl">
                	<div class="bzzya_cl_t">创意海报</div>
                    <div class="bzzya_cl_b">【太原】UI861班</div>
                </div>
                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
            </div>
            <div class="bzzya_b">
            	<div class="bzzya_bl">
                	<div class="bzzya_bll">
                    	<img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                    </div>
                    <div class="bzzya_blr">张一凡</div>
                </div>
                <div class="bzzya_br">
                	<div class="bzzya_br_la">4524</div>
                    <div class="bzzya_br_lb">162</div>
                    <div class="bzzya_br_lc">8</div>	
                </div>
            </div>
            <div class="bzgzq">
            	<div class="bzgzb">
                	<div class="bzgzl">关注</div>
                    <div class="bzgzr">私信</div>
                </div>
            </div>
            <div class="bzgzq">
            	<div class="bzgzb">
                	<div class="bzgzl">关注</div>
                    <div class="bzgzr">私信</div>
                </div>
            </div>
            
        </li>
        <li class="bzzya">
        	<div class="bzzya_t">
            	<img src="${pageContext.request.contextPath}/user/image/bzzy_c.jpg"/>
            </div>
            <div class="bzzya_c">
            	<div class="bzzya_cl">
                	<div class="bzzya_cl_t">创意海报</div>
                    <div class="bzzya_cl_b">【太原】UI861班</div>
                </div>
                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
            </div>
            <div class="bzzya_b">
            	<div class="bzzya_bl">
                	<div class="bzzya_bll">
                    	<img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                    </div>
                    <div class="bzzya_blr">张一凡</div>
                </div>
                <div class="bzzya_br">
                	<div class="bzzya_br_la">4524</div>
                    <div class="bzzya_br_lb">162</div>
                    <div class="bzzya_br_lc">8</div>	
                </div>
            </div>
            <div class="bzgzq">
            	<div class="bzgzb">
                	<div class="bzgzl">关注</div>
                    <div class="bzgzr">私信</div>
                </div>
            </div>
            <div class="bzgzq">
            	<div class="bzgzb">
                	<div class="bzgzl">关注</div>
                    <div class="bzgzr">私信</div>
                </div>
            </div>
            
        </li>
        <li class="bzzya bzzyb">
        	<div class="bzzya_t">
            	<img src="${pageContext.request.contextPath}/user/image/bzzy_d.jpg"/>
            </div>
            <div class="bzzya_c">
            	<div class="bzzya_cl">
                	<div class="bzzya_cl_t">创意海报</div>
                    <div class="bzzya_cl_b">【太原】UI861班</div>
                </div>
                <div class="bzzya_cr">云值：<span class="bzzya_cr_a">99</span>分</div>
            </div>
            <div class="bzzya_b">
            	<div class="bzzya_bl">
                	<div class="bzzya_bll">
                    	<img src="${pageContext.request.contextPath}/user/image/zya_3tx_02.jpg"/>
                    </div>
                    <div class="bzzya_blr">张一凡</div>
                </div>
                <div class="bzzya_br">
                	<div class="bzzya_br_la">4524</div>
                    <div class="bzzya_br_lb">162</div>
                    <div class="bzzya_br_lc">8</div>	
                </div>
            </div>
            <div class="bzgzq">
            	<div class="bzgzb">
                	<div class="bzgzl">关注</div>
                    <div class="bzgzr">私信</div>
                </div>
            </div>
            <div class="bzgzq">
            	<div class="bzgzb">
                	<div class="bzgzl">关注</div>
                    <div class="bzgzr">私信</div>
                </div>
            </div>
            
        </li>
    </ul>-->

    </div>
</div>
<!--bot-->
<div class="botq">
    <div class="boty">
        <ul class="botl">
            <li>
                <a class="zy_1">智云</a>
                <a>关于智云</a>
                <a>网站协议</a>
                <a>法律声明</a>
            </li>
            <li>
                <a class="zy_1">帮助中心</a>
                <a>常见问题</a>
                <a>联系我们</a>
                <a>意见反馈</a>
            </li>
            <li>
                <a class="zy_1">学习中心</a>
                <a>学习指南</a>
                <a>学员中心</a>
                <a>版权声明</a>
            </li>
            <li>
                <a class="zy_1">友情链接</a>
                <a>华信智原</a>
                <a>智源在线</a>
            </li>
        </ul>
        <!--botr-->
        <div class="botr">
            <div class="trt">
                <div class="rtl"></div>
                <div class="rtr">
                    <div class="rtr_t">母公司中国东方教育</div>
                    <div class="rtr_b">港股上市股票代码：667.HK</div>
                </div>
            </div>
            <div class="trb">
                <div class="rbl">
                    <div class="rbl_t">咨询热线：400-7777-699</div>
                    <div class="rbl_r">
                        <a><img src="${pageContext.request.contextPath}/user/image/weibo.jpg"/></a>
                        <a class="qq"><img src="${pageContext.request.contextPath}/user/image/qq.jpg"/></a>
                        <a><img src="${pageContext.request.contextPath}/user/image/weixin.jpg"/></a>
                    </div>
                </div>
                <div class="rbr"></div>
            </div>
        </div>
    </div>
</div>
<!--di-->
<div class="diq">
    <div class="diy">
        <div class="di">
            <a>DT人才培训基地（太原中心）</a>|
            <a>晋ICP备16009028号</a>|
            <a>咨询热线：400-7777-699</a>|
            <a>地址：太原市高新区平阳南路龙兴街万立科技大厦17层</a>|
            <a>版权所有：华信智原</a>
        </div>
    </div>
</div>


</body>
</html>
