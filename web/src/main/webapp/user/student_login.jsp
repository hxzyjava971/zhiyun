<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="Author" contect="http://www.webqin.net">
    <title>学生登录</title>
    <link rel="shortcut icon" href="/user/images/favicon.ico"/>
    <!--切换-->
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: "Microsoft Yahei";
            font-size: 14px;
            color: #333;
        }

        a, a:hover {
            text-decoration: none;
            color: #333;
        }

        ul, li {
            list-style: none;
        }

        .tab {
            width: 338px;
            margin: 0 auto;
        }

        .tab-title {
            height: 50px;
            font-size: 0;
            border-bottom: 1px solid #ddd;
        }

        .tab-title .item {
            display: inline-block;
            width: 80px;
            height: 50px;
            line-height: 50px;
            text-align: center;
            margin-right: 20px;
            color: #333;
            font-size: 16px;
            border-bottom: 1px solid transparent;
            color: #999;
        }

        .tab-title .item-cur {
            border-bottom: 1px solid #333;
            color: #333;
        }

        .tab-cont {
            position: relative;
        <!-- background-color: #80b600;
        --> width: 100%;
            height: 390px;
            overflow: hidden;
        }

        .tab-cont__wrap {
            position: absolute;
        }

        .tab-cont .item {
            width: 338px;
            height: auto;
            margin-top: 40px;
            color: #fff;
        }

    </style>
    <!---->
    <!--动画-->
    <link rel="stylesheet" href="/user/css/animate.css" type="text/css"/>
    <!---->
    <link type="text/css" href="/user/css/css.css" rel="stylesheet"/>
</head>

<body>
<!--top-->
<div class="yxq">
    <a href="${pageContext.request.contextPath}/">
        <div class="bz">
            <img src="/user/img/biaozhi.jpg"/>
        </div>
    </a>
    <ul class="nav">
        <a href="/user/teacher_login.jsp">
            <li>老师入口</li>
        </a>
        <li class="na">学生入口</li>
    </ul>
</div>

<!---->

<div class="content bjx">
    <div class="wow fadeInRight bjx_1" data-wow-duration="3s">
        <img src="/user/img/beij.png"/>
    </div>
    <div class="web-width">
        <div class="bjt">
            <h2 class="wow fadeInLeft">来智云</h2>
            <h1 class="wow fadeInLeft" data-wow-delay="0.5s">作业提交更智能</h1>
            <p class="wow fadeInLeft" data-wow-delay="1s"></p>
            <h3 class="wow fadeInLeft" data-wow-delay="1.5s">方便学生查看作业的批改情况，以便于更好地学习方便学生和老师更好的互动式的学习与教学</h3>
        </div>
        <div class="dly">
            <form action="" name="loginForm" id="loginForm" method="get" class="forget-pwd for">
                <div class="tab" js-tab="1">
                    <div class="tab-title">
                        <a href="javascript:;" class="item item-cur" id="change1">密码登录</a>
                        <a href="javascript:;" class="item" id="change2">短信登录</a>
                    </div>
                    <div class="tab-cont">
                        <ul class="tab-cont__wrap">
                            <p id="login1"
                               style="color: red;margin-left: 100px;margin-top: 10px;position: absolute"></p>
                            <li class="item">
                                <dl class="dlj">
                                    <dd><input type="text" placeholder="手机号" name="phoneNum1" id="phoneNum1"/></dd>
                                    <div class="clears"></div>
                                </dl>
                                <dl class="dlj">
                                    <dd><input type="password" placeholder="请输入正确的密码" name="password" id="password"/>
                                    </dd>
                                    <div class="clears"></div>
                                </dl>
                                <div class="subtijiao jj">
                                    <input type="button" value="登录" id="passwordSubmitBtn"/>
                                </div>
                                <div class="dll">
                                    <div class="zd"><input type="checkbox" value="dl" id="d1" checked="checked"/><label
                                            for="dl">下次自动登录</label></div>
                                    <div class="wj"><a
                                            href="${pageContext.request.contextPath}/user/student_frogetpassword1.jsp">忘记密码</a>｜<a
                                            href="${pageContext.request.contextPath}/user/register1.jsp">注册</a></div>
                                </div>
                                <div class="dsf">
                                    <p>第三方账号登录</p>
                                    <ul class="ds">
                                        <a href="https://weibo.com/">
                                            <li class="d_1"></li>
                                        </a>
                                        <a href="https://im.qq.com/index.shtml">
                                            <li class="d_2"></li>
                                        </a>
                                        <a href="https://wx.qq.com/">
                                            <li class="d_3"></li>
                                        </a>
                                    </ul>

                                </div>
                            </li>
                            <p id="login2" style="color: red;margin-left: 100px;margin-top: 10px;position: absolute"></p>
                            <li class="item">
                                <dl>
                                    <dd class="dlj"><input type="text" placeholder="手机号" name="phoneNum" id="phoneNum"/>
                                    </dd>
                                    <div class="clears"></div>
                                </dl>
                                <dl>
                                    <dd class="xy yl"><input type="text" class="xym" placeholder="请输入手机验证码"
                                                             name="phoneCheckCode" id="phoneCheckCode" maxlength="4"/>
                                        <button class="xym_1" id="btnGetPhoneCheckCode" style="float: right"/>
                                        获取验证码</button>
                                    </dd>
                                    <div class="clears"></div>
                                </dl>
                                <div class="subtijiao jj"><input type="button" value="登录"

                                                                 id="phoneCheckCodeSubmitBtn"/></div>
                                <div class="dll">
                                    <div class="zd"><input type="checkbox" value="dl" id="dl" checked="checked"/><label
                                            for="dl">下次自动登录</label></div>
                                    <div class="wj"><a
                                            href="${pageContext.request.contextPath}/user/student_frogetpassword1.jsp">忘记密码</a>｜<a
                                            href="index.html">注册</a></div>
                                </div>
                                <div class="dsf">
                                    <p>第三方账号登录</p>
                                    <ul class="ds">
                                        <a href="https://weibo.com/">
                                            <li class="d_1"></li>
                                        </a>
                                        <a href="https://im.qq.com/index.shtml">
                                            <li class="d_2"></li>
                                        </a>
                                        <a href="https://wx.qq.com/">
                                            <li class="d_3"></li>
                                        </a>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>


        <script src="/user/js/jquery-1.8.3.min.js"></script>
        <script src="/user/js/tab.js"></script>
        <script>
            $(function () {
                // 多个元素不同变化方式（需要在HTML中加入js-tab）
                $('[js-tab=1]').tab();

                $('[js-tab=2]').tab({
                    curDisplay: 2,
                    changeMethod: 'horizontal'
                });
            });
        </script>
        <!---->
    </div><!--web-width/-->
</div><!--content/-->
<!--bottom-->
<div class="bottom">
    <ul class="tm">
        <li>智云</li>
        <li>帮助中心</li>
        <li>学习中心</li>
        <li class="yq">友情链接</li>
    </ul>
    <h6>DT人才培训基地（太原中心）&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;晋ICP备16009028号&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;咨询热线：400-7777-699&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;地址：太原市高新区平阳南路龙兴街万立科技大厦17层&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;版权所有：华信智原</h6>
</div>
<!---->
<!--动画-->
<script src="/user/js/wow.min.js"></script>
<script>
    $(function () {
        $("#phoneNum").blur(checkPhoneNum);
        $("#phoneNum1").blur(checkPhoneNum1);
        $("#password").blur(checkPassword);
        $("#phoneCheckCode").blur(checkPhoneCheckCode);
        $("#loginForm").submit(function () {
            return false;
        })
        // 禁回车
        $(this).keydown( function(e) {
            var key = window.event?e.keyCode:e.which;
            if(key.toString() == "13"){
                return false;
            }
        });
    })
    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))) {
        new WOW().init();
    }

    //异步获取手机验证码
    $("#btnGetPhoneCheckCode").click(function () {
        if (checkPhoneNum()) {
            var phoneNum = $("#phoneNum").val();
            $.post("${pageContext.request.contextPath}/user/student/LoginSendPhoneCode", {"phoneNum": phoneNum}, function (data) {
                if (data.result) {
                    countDown();
                } else if (!data.result) {
                    $("#login1").html("");
                    $("#login2").html(data.message);
                }
            }, "json");
        }
    })

    //验证密码登录 手机号的正则
    function checkPhoneNum1() {
        var phoneNum1 = $("#phoneNum1").val();
        var reg = /^1(3|4|5|6|7|8|9)\d{9}$/;
        var flag = reg.test(phoneNum1);
        if (flag) {
            $("#phoneNum1").css("border", "1px solid rgba(255, 255, 255, 0.4)");
            $("#login1").html("");
        } else {
            $("#phoneNum1").css("border", "1px solid red");
            $("#login1").html("请输入正确的手机号");
            $("#login2").html("");
        }
        return flag;
    }

    //验证密码不为空的正则
    function checkPassword() {
        var password = $("#password").val();
        var reg = /\S/;
        var flag = reg.test(password);
        if (flag) {
            $("#password").css("border", "1px solid rgba(255, 255, 255, 0.4)");
            $("#login1").html("");
        } else {
            $("#password").css("border", "1px solid red");
            $("#login1").html("密码不能为空");
            $("#login2").html("");
        }
        return flag;
    }

    //验证验证码登录时 手机号的正则
    function checkPhoneNum() {
        debugger
        var phoneNum = $("#phoneNum").val();
        var reg = /^1(3|4|5|6|7|8|9)\d{9}$/;
        var flag = reg.test(phoneNum);
        if (flag) {
            $("#phoneNum").css("border", "1px solid rgba(255, 255, 255, 0.4)");
        } else {
            $("#phoneNum").css("border", "1px solid red");
            $("#login1").html("");
            $("#login2").html("请输入正确的手机号");
        }
        return flag;
    }


    //验证手机验证码
    function checkPhoneCheckCode() {
        var phoneCheckCode = $("#phoneCheckCode").val();
        var reg = /\S/;
        var flag = reg.test(phoneCheckCode);
        if (flag) {
            $("#phoneCheckCode").css("border", "1px solid rgba(255, 255, 255, 0.4)");
        } else {
            $("#login1").html("");
            $("#login2").html("验证码不能为空");
        }
        return flag;
    }

    // 设置cookie
    // function setCookie(name,value) {
    //     var Days = 30;
    //     var exp = new Date();
    //     exp.setTime(exp.getTime() + Days*24*60*60*1000);
    //     document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
    // }

    //异步访问 学生的手机 密码登录的controller
    $("#passwordSubmitBtn").click(function () {
        var phoneNum1 = $("#phoneNum1").val();
        var password = $("#password").val();
        var flag = checkPhoneNum1();
        var flag1 = checkPassword();
        var autologin = null;

        if ($("#d1").prop("checked") == true){
            autologin = "remember"
        }
        if (flag == false) {
            $("#phoneNum1").css("border", "1px solid red");
            $("#login1").html("请输入正确的手机号");
            $("#login2").html("");
        }
        if (flag1 == false) {
            $("#password").css("border", "1px solid red");
            $("#login1").html("密码不能为空");
            $("#login2").html("");
        }
        if (flag == true && flag1 == true) {
            $.post("${pageContext.request.contextPath}/user/student/passwordLogin", {
                "phoneNum1": phoneNum1,
                "password": password,
                "autologin": autologin
            }, function (data) {
                if (data.result) {
                    // console.log(data);
                    // setCookie("remember",data.data);
                    window.location.href = "${pageContext.request.contextPath}/";
                } else if (!data.result) {
                    $("#login1").html("手机号或密码错误");
                    $("#login2").html("");
                }
            }, "json");
        } else if (flag == false && flag1 == false) {
            $("#phoneNum1").css("border", "1px solid red");
            $("#password").css("border", "1px solid red");
            $("#login1").html("请输入手机号和密码再登录");
            $("#login2").html("");
        }
    })

    //异步访问 学生的手机 验证码登录的controller
    $("#phoneCheckCodeSubmitBtn").click(function () {
        var phoneNum = $("#phoneNum").val();
        var phoneCheckCode = $("#phoneCheckCode").val();
        var flag = checkPhoneNum();
        var flag1 = checkPhoneCheckCode();
        var autologin = null;

        if ($("#d1").prop("checked") == true){
            autologin = "remember"
        }
        if (flag == false) {
            $("#phoneNum").css("border", "1px solid red");
            $("#login1").html("");
            $("#login2").html("请输入正确的手机号");
        }
        if (flag1 == false) {
            $("#phoneCheckCode").css("border", "1px solid red");
            $("#login1").html("");
            $("#login2").html("验证码不能为空");
        }
        if (flag == true && flag1 == true) {
            $.post("${pageContext.request.contextPath}/user/student/phoneCheckCodeLogin", {
                "phoneNum": phoneNum,
                "phoneCheckCode": phoneCheckCode,
                "autologin": autologin
            }, function (data) {
                if (data.result) {
                    window.location.href = "${pageContext.request.contextPath}/";
                } else if (!data.result) {
                    $("#login1").html("");
                    $("#login2").html(data.message);
                }
            }, "json");
        } else if (flag == false && flag1 == false) {
            $("#phoneNum").css("border", "1px solid red");
            $("#phoneCheckCode").css("border", "1px solid red");
            $("#login1").html("");
            $("#login2").html("请输入手机号和验证码再登录");
        }
    })


    //手机验证码获取后 倒计时60s
    function countDown() {
        let count = 60;
        const countDown = setInterval(() => {
            if (count === 0) {
            $('.xym_1').text('重新发送').removeAttr('disabled');
            $('.xym_1').css({
                // background: '#ff9400',
                color: '#fff',
            });
            clearInterval(countDown);
        } else {
            $('.xym_1').attr('disabled', true);
            $('.xym_1').css({
                background: '#d8d8d8',
                color: '#707070',
            });
            $('.xym_1').text(count + '秒后重新获取');
        }
        count--;
    }, 1000);
    }

    $("#change1").click(function () {
        $("#login2").html("");
    })
    $("#change2").click(function () {
        $("#login1").html("");
    })
</script>
<!---->
</body>
</html>
