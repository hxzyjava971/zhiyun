<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>作业</title>
    <!--动画效果-->
    <link rel="stylesheet" href="/user/css/animate.css" type="text/css">
    <!--置顶插件-->
    <style>
        * {
            margin: 0px;
            padding: 0px;
            list-style: none;
            text-decoration: none;
        }

        #backTop {
            cursor: pointer;
            padding: 5px;
            width: 60px;
            height: 60px;
            background: rgba(0, 0, 0, 0.5);
            position: fixed;
            bottom: 10%;
            right: 13%;
            font-size: 16px;
            -moz-user-select: none;
            -webkit-user-select: none;
            display: none;
            border-radius: 50%;
            z-index: 2;
        }

        .red {
            color: red;
        }

        /*top fix*/
        .banner_rignavcls {
            display: none;
        }

        .banner_rignavcla {
            display: block;
        }

        .topfix {
            width: 100%;
            height: 60px;
            background: rgba(0, 0, 0, 0.8);
            position: fixed;
            top: 0px;
            left: 0px;
            z-index: 999999;
            box-shadow: 0px 3px 4px rgba(0, 0, 0, 0.2);
        }

        .topfixcon {
            width: 1200px;
            height: 60px;
            margin: 0 auto;
        }

        .topfixcon img {
            width: auto;
            height: auto;
            display: block;
            margin-top: 12px;
            display: inline-block;
            float: left;
        }

        .topfixinput {
            width: 570px;
            height: 30px;
            float: right;
            margin-top: 15px;
            border-radius: 4px;
            overflow: hidden;
            position: relative;
            background-color: #FFFFFF;
        }

        .topfixinput input {
            width: 480px;
            height: 30px;
            border: none;
            margin-top: 0px;
            margin-left: 20px;
            padding-left: 0px;
            float: left;
            outline: none;
        }

        .topfixinput a {
            position: absolute;
            left: 500px;
            top: 0px;
            display: inline-block;
            width: 70px;
            height: 31px;
            text-align: center;
            line-height: 30px;
            color: #fff;
            font-size: 16px;
            background: #ff3457;
            float: right;
        }

        .sousuo_a img {
            margin-top: 5px !important;
            width: 20px;
            height: 20px;
            margin-left: 25px;
        }

        /*di*/
        .margin {
            margin: 0px;
        !important
        }

        .load {
            width: 140px;
            height: 120px;
            position: fixed;
            top: 0;
            z-index: 10000;
        / / display: none;
        }

        .loader {
            width: 140px;
            height: 120px;
            /* border: 1px solid red; */
            text-align: center;
            position: absolute;
            top: 680px;
            left: 860px;
            padding-top: 15px;
            background-color: rgba(0, 0, 0, 0.5);
            border-radius: 4px;
        }

        #loader-1 {
            width: 60px;
            height: 60px;
        }

        .load-msg {
            height: 50px;
            line-height: 50px;
            color: #fff;
            font-size: 13px;
            /* margin-top: 20px; */
        }

        svg path, svg rect {
            fill: #ff3457;
        }

        .cell {
            float: left;
            margin-left: 15px;
            margin-top: 15px;
        }
    </style>
    <!--置顶插件-->
    <!--瀑布流插件-->
    <link href="/user/css/lanrenzhijia.css" type="text/css" rel="stylesheet"/>
    <link href="/user/css/style-a.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/admin/plugins/bootstrap/css/bootstrap.min.css">
</head>
<body>
<script src="/user/js/jquery.min.js"></script>
<!--动画效果-->
<script src="/user/js/wow.min.js"></script>
<script>
    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))) {
        new WOW().init();
    }
    ;
</script>
<!--动画效果-->
<!--tou-->
<!--top-->
<jsp:include page="top.jsp"></jsp:include>
<!--sx-->
<div class="sxq">
    <div class="sxy">
        <div class="wow bounceInUp sxnr_c" data-wow-delay="0.3s">
            <div class="nr_ct">
                <div class="nr_ct_l">校区：</div>
                <div class="nr_ct_r" id="xiaoqu">
                    <c:forEach items="${sessionScope.schools}" var="school">
                        <a onclick="ad('${school.id}',this)" class="tiaojian">${school.schoolName}</a>
                    </c:forEach>
                    <input id = "schoolId" hidden type="text" value="1">
                </div>
            </div>
            <div class="nr_cc">
                <div class="nr_ct_l">班级：</div>
                <div class="nr_ct_r" id="findClassBySchool"></div>
            </div>
            <div class="nr_cb">
                <div class="nr_ct_l">学生：</div>
                <div class="nr_ct_r" id="findStudentByClass"></div>
            </div>
            <div class="nr_cb">
                <div class="nr_ct_d">日期：</div>
                <div class="nr_ct_e">开始时间:</div>
                <div class="time"><input type="date" id="startTime" onchange="search()"/></div>
                <div class="nr_ct_e">结束时间:</div>
                <div class="time"><input type="date" id="endTime" onchange="search()"/></div>
            </div>
            <div class="nr_cb">
                <div class="nr_ct_l">类型：</div>
                <div class="nr_ct_r" id="neirong">
                    <c:forEach items="${sessionScope.content}" var="content">
                        <a onclick="search()" class="tiaojian">${content.description}</a>
                    </c:forEach>
                </div>
            </div>
        </div>
        <div class="wow bounceInUp sxnr_b" data-wow-delay="0.3s">
            <div class="gongyou">共有
                <span id="size"></span>个作业
            </div>
        </div>
    </div>
    <!--sxzy-->
    <!--瀑布流插件-->
    <div id="waterfall" class="wow bounceInUp" data-wow-delay="1s"></div>
    <script>
        $(function () {
            search();
        })

        function search(pageNum) {
            var xueXiao,banji,xuesheng,startTime,endTime,keyWords,
                biaoqian = new Array(), neirong = new Array(), leixing = new Array(), paixu;
            xueXiao = $("#schoolId").attr("value");
            banji = $("#findClassBySchool a.red").attr("value");
            xuesheng = $("#findStudentByClass a.red").attr("value");
            keyWords = $("#keyWords").val();

            if($("#startTime").val()){
                startTime = $("#startTime").val();
            }
            if($("#endTime").val()){
                endTime = $("#endTime").val();
            }
            var neirongarr = $("#neirong a.red");
            for(var i = 0;i<neirongarr.length;i++){
                neirong.push(neirongarr[i].innerHTML);
            }
            $.get("/user/homework/findHomework",
                {
                    "pageNum": pageNum,
                    "xueXiao":xueXiao,
                    "banji": banji,
                    "xuesheng": xuesheng,
                    "startTime":startTime,
                    "endTime":endTime,
                    "biaoqian": biaoqian,
                    "neirong": neirong,
                    "leixing": leixing,
                    "paixu": paixu,
                    "keyWords":keyWords
                }, function (pageData) {
                    var result = pageData.content;
                    $("#size").html(pageData.totalElements);
                    $("#waterfall").empty();
                    var str = "";
                    for (var i = 0; i < result.length; i++) {
                        hw = result[i];
                        var schoolName = "", className = "", header = "", studentName = "", homeworksLength = "";
                        try {
                            schoolName = hw.students[0].classes.school.schoolName;
                        } catch (e) {
                        }
                        try {
                            className = hw.students[0].classes.className;
                        } catch (e) {
                        }
                        try {
                            header = hw.students[0].header;
                        } catch (e) {
                        }
                        try {
                            studentName = hw.students[0].name;
                        } catch (e) {
                        }
                        str += "<div class=\"cell\">\n" +
                            "                <a href=\"${pageContext.request.contextPath}/user/jobdetails/message?id=" + hw.id + "\" target=\"new\">\n" +
                            "                    <div class=\"zyq\">\n" +
                            "                        <div class=\"zyt\">\n" +
                            "                            <img src=\"" + hw.coverImg + "\" width=\"285px\" height=\"248px\"/>\n" +
                            "                        </div>\n" +
                            "                        <div class=\"zyc\">\n" +
                            "                            <div class=\"zyct\">" + hw.workName + "</div>\n" +
                            "                            <div class=\"zycc\">\n" +
                            "                                    【" + schoolName + "】\n" +
                            "                                    " + className + "\n" +
                            "                            </div>\n" +
                            "                            <div class=\"zycb\">\n" +
                            "                                <div class=\"zycb_l\">\n" +
                            "                                    <div class=\"zycb_la\">" + hw.clicks + "</div>\n" +
                            "                                    <div class=\"zycb_lc\">" + hw.thumbsUp + "</div>\n" +
                            "                                </div>\n" +
                            "                                <div class=\"zycb_r\">" + hw.tags + "</div>\n" +
                            "                            </div>\n" +
                            "                        </div>\n" +
                            "                        <div class=\"zyb\">\n" +
                            "                            <div class=\"zyba\">\n" +
                            "                                <div class=\"zybl\">\n" +
                            "                                    <img src=\"" + header + "\" width=\"22px\" height=\"22px\"/>\n" +
                            "                                </div>\n" +
                            "                            </div>\n" +
                            "                            <div class=\"zybr\">" + studentName + "</div>\n" +
                            "                            <div class=\"clear\"></div>\n" +
                            "                        </div>\n" +
                            "                    </div>\n" +
                            "                </a>\n" +
                            "            </div>";
                    }
                    $("#waterfall").append(str);
                    // 图片不变形配置
                    imgNotChange();
                    // 分页配置
                    $('#pageLimit').bootstrapPaginator({
                        currentPage: pageData.pageable.pageNumber + 1,//当前页。
                        totalPages: pageData.totalPages,//总页数。
                        bootstrapMajorVersion: 3,//当前版本
                        numberOfPages: 10,//显示的页数
                        tooltipTitles: function () {
                        },// 鼠标移入不显示样式
                        itemTexts: function (type, page, current) {//自定义按钮文本。
                            switch (type) {
                                case "first":
                                    return "首页";
                                case "prev":
                                    return "上一页";
                                case "next":
                                    return "下一页";
                                case "last":
                                    return "末页";
                                case "page":
                                    return page;
                            }
                        },
                        onPageClicked: function (event, originalEvent, type, page) {//给按钮绑定事件，page为按钮上的数字。
                            search(page);
                        }
                    });
            })
        }

        function imgNotChange() {
            // 图片不变形配置
            $('.zyt img').jqthumb({
                classname: 'jqthumb',
                width: 285,
                height: 248,
                showoncomplete: true
            });
            $('.zybl img').jqthumb({
                classname: 'jqthumb',
                width: 22,
                height: 22,
                showoncomplete: true
            });
        }

        // 回到网页顶部
        function goback() {
            $("html,body").animate({
                scrollTop: 0,
                screenLeft: 0,
            }, 300);
        }

        function ad(data1, a) {
            $(".banji,.xuesheng").remove();
            $("#xiaoqu a").css("color", "#666");
            $(a).css("color", "red");
            $("#schoolId").attr("value",data1);
            search();
            $.ajax({
                url: "/user/school/findClass?schoolName=" + data1,
                type: "GET",
                success: function (data) {
                    $("#findClassBySchool,#findStudentByClass").empty();
                    for (i = 0; i < data.length; i++) {
                        $("#findClassBySchool").append('<a class="banji" value="' + data[i].id + '">' + data[i].className + '</a>');
                    }
                    $(".banji").click(function () {
                        $(".xuesheng").remove();
                        $(".banji").removeClass("red");
                        $(this).addClass("red");
                        search();
                        $.ajax({
                            url: "/user/school/findStudent?className=" + $(this).text(),
                            type: "GET",
                            success: function (data) {
                                $("#findStudentByClass").empty();
                                for (i = 0; i < data.length; i++) {
                                    $("#findStudentByClass").append('<a class="xuesheng" value="' + data[i].id + '">' + data[i].name + '</a>');
                                }
                                $(".xuesheng").click(function () {
                                    $(".xuesheng").removeClass("red");
                                    $(this).addClass("red");
                                    search();
                                })
                            }
                        })
                    })
                }
            })
        }

        $(".tiaojian").click(function () {
            $(this).toggleClass("red");
            search();
        })
    </script>
    <%-- 分页 --%>
    <div class="box-footer" style="clear: both;margin-left: 60%">
        <div>
            <ul id="pageLimit"></ul>
        </div>
    </div>
</div>
</div>

<!--置顶插件-->
<div id="backTop">
    <div class="zdra">置顶</div>
</div>
<!--插入脚本-->
<script src="/user/js/3huidaodingbu.js" type="text/javascript" charset="utf-8"></script>
<script src="/user/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/admin/js/bootstrap-paginator.js"></script>
<script src="${pageContext.request.contextPath}/user/js/jqthumb.min.js"></script>
<!--搜索插件-->
<!--bot-->
<%--<底部>--%>
<jsp:include page="bottom.jsp"></jsp:include>
<!--di-->
<div class="diq">
    <div class="diy">
        <div class="di">
            <span>DT人才培训基地（太原中心）</span>|
            <span>晋ICP备16009028号</span>|
            <span>咨询热线：400-7777-699</span>|
            <span>地址：太原市高新区平阳南路龙兴街万立科技大厦17层</span>|
            <span>版权所有：华信智原</span>
        </div>
    </div>
</div>
</body>
</html>