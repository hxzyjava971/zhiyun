<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!--top-->
<link href="${pageContext.request.contextPath}/user/css/style-a.css" rel="stylesheet" type="text/css"/>

<style>
    .woxx > div {
        color: #FFFFFF;
    }

    .dl_2 {
        float: left;
        width: auto;
        margin-top: 0px;
        height: 100%;
        line-height: 60px;
        margin-left: 20px;
        color: white;
        font-size: 14px;
    }

    .cl {
        color: #D10E30 !important;
    }
</style>
<div class="touq" id="tou">
    <div class="touy">
        <div class="toul">
            <div class="toull">
                <img src="${pageContext.request.contextPath}/user/image/log-white.jpg"/>
                <div class="llr">
                    <div class="llr_t">ZHIYUN</div>
                    <div class="llr_b">作业提交更智能</div>
                </div>
            </div>
            <div class="toulr">
                <a href="${pageContext.request.contextPath}/">首页</a>
                <a href="${pageContext.request.contextPath}/user/homework/findAll">作业</a>
                <a href="${pageContext.request.contextPath}/user/paihangbang.jsp">排行榜</a>
                <a class="touzs" href="${pageContext.request.contextPath}/user/xuexizhushou.jsp">学习助手
                    <ul class="zsb">
                        <li>网址导航</li>
                        <li>颜色搭配</li>
                        <li>设计工具</li>
                        <li>尺寸规范</li>
                        <li>字体笔刷</li>
                    </ul>
                </a>
            </div>
        </div>
        <div class="tour">
            <div class="tourl">
                <form action="${pageContext.request.contextPath}/user/homework/findAll">
                    <input type="text" name="keyWords" id="keyWords" value="${keyWords}" placeholder="搜索作品文章"
                           style="margin-left: 20px;"/>
                    <input type="submit" value="" class="rlr"/>
                </form>
            </div>

            <c:if test="${sessionScope.containsKey('student') or sessionScope.containsKey('teacher')}">
                <div class="tourr">
                    <c:if test="${sessionScope.containsKey('student')}">
                        <a href="${pageContext.request.contextPath}/user/student/addPre" target="new">
                            <div class="a_1"></div>
                        </a>
                        <a href="${pageContext.request.contextPath}/user/student_update.jsp">
                            <div class="a_2" id="a-2"></div>
                        </a>
                        <div class="a_3"></div>
                        </a>
                    </c:if>
                    <c:if test="${sessionScope.containsKey('teacher')}">
                        <a href="${pageContext.request.contextPath}/user/teacher/openPage" target="new">
                            <div class="a_1"></div>
                        </a>
                        <div class="a_2" id="a-2"></div>
                        <div class="a_3"></div>
                    </c:if>
                </div>
            </c:if>

            <c:if test="${empty sessionScope.student and empty sessionScope.teacher}">
                <div class="tourr2">
                    <a href="${pageContext.request.contextPath}/user/student_login.jsp" class="dl_2 cl dl11">登录</a>
                    <a href="${pageContext.request.contextPath}/user/register1.jsp" class="dl_2 dl11">注册</a>
                    <script>
                        if ($("#a-2") != null) {
                            if (${sessionScope.containsKey('student') or sessionScope.containsKey('teacher')}) {
                                $(".dl11").empty()
                            }
                        }
                    </script>
                </div>
            </c:if>

            <div class="wo_xs">
                <div class="wo_xsq">
                    <c:if test="${sessionScope.containsKey('student')}">
                        <c:if test="${empty student.header}">
                            <img src="${pageContext.request.contextPath}/user/image/headerImg.jpg" class="header1"
                                 width="30"
                                 height="30"/>
                        </c:if>
                        <c:if test="${not empty student.header}">
                            <img src="${student.header}" width="30" height="30"/>
                        </c:if>
                    </c:if>
                    <c:if test="${sessionScope.containsKey('teacher')}">
                        <c:if test="${empty teacher.picture}">
                            <img src="${pageContext.request.contextPath}/user/image/headerImg.jpg" width="30"
                                 height="30"/>
                        </c:if>
                        <c:if test="${not empty teacher.picture}">
                            <img src="${teacher.picture}" width="30" height="30"/>
                        </c:if>
                    </c:if>
                </div>
                <c:if test="${sessionScope.containsKey('student')}">
                    <ul class="xl_wo">
                        <li class="xl_wotx">
                            <div class="wotx_l">
                                <c:if test="${empty student.header}">
                                    <img src="${pageContext.request.contextPath}/user/image/headerImg.jpg" width="30"
                                         height="30"/>
                                </c:if>
                                <c:if test="${not empty student.header}">
                                    <img src="${student.header}" width="30" height="30"/>
                                </c:if>
                            </div>
                            <div class="wotx_r">
                                <div class="wotx_rt">${student.name}</div>
                            </div>
                        </li>
                        <a href="${pageContext.request.contextPath}/user/homework/findNotCommentHomeworkByStudent"
                           target="new">
                            <li class="woxx">
                                <div class="wdwz">个人中心</div>
                            </li>
                        </a>
                        <a href="${pageContext.request.contextPath}/user/student_frogetpassword1.jsp" target="new">
                            <li class="woxx">
                                <div class="wdzy">修改密码</div>
                            </li>
                        </a>
                        <li class="woxx">
                            <div class="wdtc"><a href="${pageContext.request.contextPath}/user/student/logout">退出登录</a>
                            </div>
                        </li>
                    </ul>
                </c:if>
                <c:if test="${sessionScope.containsKey('teacher')}">
                    <ul class="xl_wo">
                        <li class="xl_wotx">
                            <div class="wotx_l">
                                <c:if test="${empty teacher.picture}">
                                    <img src="${pageContext.request.contextPath}/user/image/headerImg.jpg" width="30"
                                         height="30"/>
                                </c:if>
                                <c:if test="${not empty teacher.picture}">
                                    <img src="${teacher.picture}" width="30" height="30"/>
                                </c:if>
                            </div>
                            <div class="wotx_r">
                                <div class="wotx_rt">${teacher.name}</div>
                            </div>
                        </li>
                        <a href="${pageContext.request.contextPath}/user/homework/findNotCommentHomeworkByTeacher"
                           target="new">
                            <li class="woxx">
                                <div class="wdwz">批改作业</div>
                            </li>
                        </a>
                        <a href="${pageContext.request.contextPath}/user/teacher_frogetpassword1.jsp" target="new">
                            <li class="woxx">
                                <div class="wdzy">修改密码</div>
                            </li>
                        </a>
                        <li class="woxx">
                            <div class="wdtc"><a href="${pageContext.request.contextPath}/user/teacher/logout">退出登录</a>
                            </div>
                        </li>
                    </ul>
                </c:if>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/user/js/jqthumb.min.js"></script>
<script>
    // 图片不变形配置
    $('.wo_xsq img,.wotx_l img').jqthumb({
        classname: 'jqthumb',
        width: 30,
        height: 30,
        showoncomplete: true
    });
</script>