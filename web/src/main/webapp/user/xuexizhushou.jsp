<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>无标题文档</title>
    <!--置顶插件-->
    <style>
        #backTop {
            cursor: pointer;
            padding: 5px;
            width: 60px;
            height: 60px;
            background: rgba(0, 0, 0, 0.5);
            position: fixed;
            bottom: 10%;
            right: 13%;
            font-size: 16px;
            -moz-user-select: none;
            -webkit-user-select: none;
            display: none;
            border-radius: 50%;
            z-index: 2;
        }
    </style>
    <!--置顶插件-->
    <!--动画效果-->
    <link rel="stylesheet" href="css/animate.css" type="text/css">
    <!--动画效果-->
    <link href="css/style-a.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<!--动画效果-->
<script src="js/wow.min.js"></script>
<script>
    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))) {
        new WOW().init();
    }
    ;
</script>
<jsp:include page="top.jsp"></jsp:include>
<!--cd-->
<div class="cdq">
    <div class="cdy">
        <div class="cdn">
            <div class="wow fadeInDown cddh" data-wow-delay="0s">
                <a>网址导航</a>/
            </div>
            <div class="wow fadeInDown cdys" data-wow-delay="0.1s">
                <a>颜色搭配</a>/
            </div>
            <div class="wow fadeInDown cdgj" data-wow-delay="0.2s">
                <a>设计工具</a>/
            </div>
            <div class="wow fadeInDown cdcc" data-wow-delay="0.3s">
                <a>尺寸规范</a>/
            </div>
            <div class="wow fadeInDown cdzt" data-wow-delay="0.4s">
                <a>字体笔刷</a>
            </div>
        </div>
    </div>
</div>
<!--cb-->
<div class="cbq">
    <div class="cby">
        <div class="wow bounceInRight pm">
            <div class="cbt">
                <div class="btr" id="pm">平面设计</div>
            </div>
            <ul class="cbb">
                <li>
                    <a href="http://www.moko.cc/moko/post/1.html" target="_blank">
                        <div class="bbl pm_1"></div>
                        <div class="bbr">
                            <object><a href="http://www.moko.cc/moko/post/1.html" target="_blank" class="bbr_mz">美空</a>
                            </object>
                            <div class="bbr_nr">设计P图累的时候，不妨到这里看看美女吧</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="https://www.sj520.cn/" target="_blank">
                        <div class="bbl pm_2"></div>
                        <div class="bbr">
                            <object><a href="https://www.sj520.cn/" target="_blank" class="bbr_mz">520设计网</a></object>
                            <div class="bbr_nr">百万优质精选素材免费下载</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="http://www.thefox.cn/" target="_blank">
                        <div class="bbl pm_3"></div>
                        <div class="bbr">
                            <object><a class="bbr_mz">WordPress主题</a></object>
                            <div class="bbr_nr">创客云_专注WordPress建站资源分享平台</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="bbl pm_4"></div>
                        <div class="bbr">
                            <object><a class="bbr_mz">网易图片</a></object>
                            <div class="bbr_nr">网易新闻中心图片频道，24小时热图实时推送</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="bbl pm_5"></div>
                        <div class="bbr">
                            <object><a class="bbr_mz">新浪图片</a></object>
                            <div class="bbr_nr">有温度的视觉，摄影师成长平台，影像记录中国</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="bbl pm_6"></div>
                        <div class="bbr">
                            <object><a class="bbr_mz">蜂鸟网</a></object>
                            <div class="bbr_nr">摄影爱好者分享摄影技巧和作品的中国影像门户</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="bbl pm_7"></div>
                        <div class="bbr">
                            <object><a class="bbr_mz">中国国家地理网</a></object>
                            <div class="bbr_nr">最专业的深度旅游体验平台，最具特色的互动社区</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="bbl pm_8"></div>
                        <div class="bbr">
                            <object><a class="bbr_mz">Style-Arena</a></object>
                            <div class="bbr_nr">日本街拍帅哥美女一览无余！东京街头时尚网</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="bbl pm_9"></div>
                        <div class="bbr">
                            <a class="bbr_mz">时尚网</a>
                            <div class="bbr_nr">美图看不完！高端时尚白领生活专属领地</div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <!--ds-->
        <div class="wow bounceInRight pm" data-wow-delay="0.2s">
            <div class="cbt">
                <div class="btr_ds" id="ds">电商美工</div>
            </div>
            <ul class="cbb">
                <li>
                    <div class="bbl ds_1"></div>
                    <div class="bbr">
                        <a class="bbr_mz">Kuler</a>
                        <div class="bbr_nr">网页设计师配色的最佳之选</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ds_2"></div>
                    <div class="bbr">
                        <a class="bbr_mz">设计配色大全</a>
                        <div class="bbr_nr">中国传统色彩、日本传统色彩、色彩搭配等</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ds_3"></div>
                    <div class="bbr">
                        <a class="bbr_mz">COLOURlovers</a>
                        <div class="bbr_nr">交流颜色、色彩趋势和配色方案的超人气社区</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ds_4"></div>
                    <div class="bbr">
                        <a class="bbr_mz">色彩猎人</a>
                        <div class="bbr_nr">每天收集并策划发布美丽的配色方案</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ds_5"></div>
                    <div class="bbr">
                        <a class="bbr_mz">Coolors</a>
                        <div class="bbr_nr">实用！成千上万的配色方案</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ds_6"></div>
                    <div class="bbr">
                        <a class="bbr_mz">漂亮的渐变颜色 </a>
                        <div class="bbr_nr">今年流行的渐变！点击屏幕两侧按钮可选更多色彩</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ds_7"></div>
                    <div class="bbr">
                        <a class="bbr_mz">CssWinner网页色彩</a>
                        <div class="bbr_nr">CSS画廊，可根据右侧颜色块展现最流行的网页</div>
                    </div>
                </li>
            </ul>
        </div>
        <!--ui-->
        <div class="wow bounceInRight pm">
            <div class="cbt">
                <div class="btr_ui" id="ui">UI设计</div>
            </div>
            <ul class="cbb">
                <li>
                    <div class="bbl ui_1"></div>
                    <div class="bbr">
                        <a class="bbr_mz">UI8</a>
                        <div class="bbr_nr">聚集世界各地优秀设计师的界面源文件</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ui_2"></div>
                    <div class="bbr">
                        <a class="bbr_mz">Designmodo</a>
                        <div class="bbr_nr">所有高质量UI工具包都在这里可以找到，部分免费下载</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ui_3"></div>
                    <div class="bbr">
                        <a class="bbr_mz">Flat UI</a>
                        <div class="bbr_nr">扁平化UI设计灵感，采集扁平化相关的App、网页等</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ui_4"></div>
                    <div class="bbr">
                        <a class="bbr_mz"> UI 设计</a>
                        <div class="bbr_nr">超赞！为您精挑细选的界面设计频道</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ui_5"></div>
                    <div class="bbr">
                        <a class="bbr_mz">移动端设计</a>
                        <div class="bbr_nr">推荐！收集被程序员追着砍的App动效和视频</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ui_6"></div>
                    <div class="bbr">
                        <a class="bbr_mz">365psd</a>
                        <div class="bbr_nr">兢兢业业每天更新着用户界面相关的PSD</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ui_7"></div>
                    <div class="bbr">
                        <a class="bbr_mz">CollectUI</a>
                        <div class="bbr_nr">UI设计必备！100多个分类，不用发愁没灵感了</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ui_8"></div>
                    <div class="bbr">
                        <a class="bbr_mz"> UI Movement</a>
                        <div class="bbr_nr">展示世界最有才华设计师的界面动效设计作品</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ui_9"></div>
                    <div class="bbr">
                        <a class="bbr_mz">Pixeden</a>
                        <div class="bbr_nr">赞！免费优质界面设计源文件及有网站模板</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ui_10"></div>
                    <div class="bbr">
                        <a class="bbr_mz"> Site Inspire</a>
                        <div class="bbr_nr">致力于分享推荐优秀网页及交互设计案例</div>
                    </div>
                </li>
            </ul>
        </div>
        <!--ch-->
        <div class="wow bounceInRight pm">
            <div class="cbt">
                <div class="btr_ch" id="ch">插画</div>
            </div>
            <ul class="cbb">
                <li>
                    <div class="bbl ch_1"></div>
                    <div class="bbr">
                        <a class="bbr_mz">Grafolio</a>
                        <div class="bbr_nr">韩国插画设计师交流和展示插画作品集的平台</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ch_2"></div>
                    <div class="bbr">
                        <a class="bbr_mz">FolioArt</a>
                        <div class="bbr_nr">推荐！英国插画师作品及灵感展示网站</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ch_3"></div>
                    <div class="bbr">
                        <a class="bbr_mz">SAKIROO</a>
                        <div class="bbr_nr">韩国插画设计网，人设为主</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ch_4"></div>
                    <div class="bbr">
                        <a class="bbr_mz">drawr</a>
                        <div class="bbr_nr">漫画菜鸟必备！可以观看别人的绘画过程</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ch_5"></div>
                    <div class="bbr">
                        <a class="bbr_mz">插画驿站</a>
                        <div class="bbr_nr">Behance旗下的插画网</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ch_6"></div>
                    <div class="bbr">
                        <a class="bbr_mz">Cool Vibe</a>
                        <div class="bbr_nr">超酷！展示高清插画壁纸以及动漫、梦幻图片的地方</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ch_7"></div>
                    <div class="bbr">
                        <a class="bbr_mz">A站</a>
                        <div class="bbr_nr">AcFun作为弹幕视频网站，是中国二次元文化的开创者</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ch_8"></div>
                    <div class="bbr">
                        <a class="bbr_mz">动漫之家</a>
                        <div class="bbr_nr">国内最全最专业的在线漫画、原创漫画</div>
                    </div>
                </li>
                <li>
                    <div class="bbl ch_9"></div>
                    <div class="bbr">
                        <a class="bbr_mz">B站</a>
                        <div class="bbr_nr">国内知名视频弹幕网站，有最及时的动漫新番</div>
                    </div>
                </li>
            </ul>
        </div>
        <!--java-->
        <div class="wow bounceInRight pm">
            <div class="cbt">
                <div class="btr_java" id="java">JAVA</div>
            </div>
            <ul class="cbb">
                <li>
                    <div class="bbl java_1"></div>
                    <div class="bbr">
                        <a class="bbr_mz">w3school在线教程</a>
                        <div class="bbr_nr">必备！全球最大的中文Web技术教程</div>
                    </div>
                </li>
                <li>
                    <div class="bbl java_2"></div>
                    <div class="bbr">
                        <a class="bbr_mz">凹凸实验室</a>
                        <div class="bbr_nr">推荐！沉淀与分享前端开发、页面制作技巧</div>
                    </div>
                </li>
                <li>
                    <div class="bbl java_3"></div>
                    <div class="bbr">
                        <a class="bbr_mz">代码笔</a>
                        <div class="bbr_nr">超赞！面向前端设计人员的圣地</div>
                    </div>
                </li>
                <li>
                    <div class="bbl java_4"></div>
                    <div class="bbr">
                        <a class="bbr_mz">Bootstrap中文网</a>
                        <div class="bbr_nr">简洁、直观、强悍的响应式前端开发框架</div>
                    </div>
                </li>
                <li>
                    <div class="bbl java_5"></div>
                    <div class="bbr">
                        <a class="bbr_mz">w3ctech</a>
                        <div class="bbr_nr">推荐！中国最大的前端技术社区</div>
                    </div>
                </li>
                <li>
                    <div class="bbl java_6"></div>
                    <div class="bbr">
                        <a class="bbr_mz">4分钟网页</a>
                        <div class="bbr_nr">经典教学案例！教你4分钟变戏法式制作一个网页</div>
                    </div>
                </li>
            </ul>
        </div>
        <!--sn-->
        <div class="wow bounceInRight pm">
            <div class="cbt">
                <div class="btr" id="sn">室内设计</div>
            </div>
            <ul class="cbb">
                <li>
                    <a href="http://www.moko.cc/moko/post/1.html" target="_blank">
                        <div class="bbl sn_1"></div>
                        <div class="bbr">
                            <object><a href="http://www.moko.cc/moko/post/1.html" target="_blank"
                                       class="bbr_mz">Houzz</a></object>
                            <div class="bbr_nr">全球最著名的装修和室内设计平台</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="https://www.sj520.cn/" target="_blank">
                        <div class="bbl sn_2"></div>
                        <div class="bbr">
                            <object><a href="https://www.sj520.cn/" target="_blank" class="bbr_mz">AD</a></object>
                            <div class="bbr_nr">AD在室内设计领域具有不可估量的影响力</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="bbl sn_3"></div>
                        <div class="bbr">
                            <object><a class="bbr_mz">居然设计家</a></object>
                            <div class="bbr_nr">国内一流的室内设计平台</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="bbl sn_4"></div>
                        <div class="bbr">
                            <object><a class="bbr_mz">设计牛奶</a></object>
                            <div class="bbr_nr">一本致力于现代设计的在线灵感杂志</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="bbl sn_5"></div>
                        <div class="bbr">
                            <object><a class="bbr_mz">美国家园频道</a></object>
                            <div class="bbr_nr">致力于家庭装修、园艺、手工艺和家庭改造方案</div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="bbl sn_6"></div>
                        <div class="bbr">
                            <object><a class="bbr_mz">ArchDaily</a></object>
                            <div class="bbr_nr">
                                世界最受欢迎的建筑网站
                            </div>
                        </div>
                    </a>
                </li>

            </ul>
        </div>

    </div>
    <!--lm-->
    <div class="wow bounceInDown lm">
        <div class=" lmpm">
            <a href="#pm">
                <div>平面设计</div>
            </a>
        </div>
        <div class="lmds">
            <a href="#ds">
                <div>电商美工</div>
            </a>
        </div>
        <div class="lmui">
            <a href="#ui">
                <div class="mui">&nbsp;&nbsp;UI设计</div>
            </a>
        </div>
        <div class="lmch">
            <a href="#ch">
                <div class="mch">插画</div>
            </a>
        </div>
        <div class="lmjava">
            <a href="#java">
                <div class="mjava">JAVA</div>
            </a>
        </div>
        <div class="lmsn">
            <a href="#sn">
                <div class="msn">室内设计</div>
            </a>
        </div>
        <div class="lmqt">
            <a href="#">
                <div class="mqt">其他素材</div>
            </a>
        </div>
    </div>
    <!--lmr-->

    <!--置顶插件-->
    <div id="backTop">
        <div class="zdra">置顶</div>
    </div>
    <!--插入脚本-->
    <script src="js/3huidaodingbu.js" type="text/javascript" charset="utf-8"></script>
    <!--置顶插件-->

    <!--bot-->
    <div class="botq">
        <div class="boty">
            <ul class="botl">
                <li>
                    <a class="zy_1">智云</a>
                    <a>关于智云</a>
                    <a>网站协议</a>
                    <a>法律声明</a>
                </li>
                <li>
                    <a class="zy_1">帮助中心</a>
                    <a>常见问题</a>
                    <a>联系我们</a>
                    <a>意见反馈</a>
                </li>
                <li>
                    <a class="zy_1">学习中心</a>
                    <a>学习指南</a>
                    <a>学员中心</a>
                    <a>版权声明</a>
                </li>
                <li>
                    <a class="zy_1">友情链接</a>
                    <a>华信智原</a>
                    <a>智源在线</a>
                </li>
            </ul>
            <!--botr-->
            <div class="botr">
                <div class="trt">
                    <div class="rtl"></div>
                    <div class="rtr">
                        <div class="rtr_t">母公司中国东方教育</div>
                        <div class="rtr_b">港股上市股票代码：667.HK</div>
                    </div>
                </div>
                <div class="trb">
                    <div class="rbl">
                        <div class="rbl_t">咨询热线：400-7777-699</div>
                        <div class="rbl_r">
                            <a><img src="image/weibo.jpg"/></a>
                            <a class="qq"><img src="image/qq.jpg"/></a>
                            <a><img src="image/weixin.jpg"/></a>
                        </div>
                    </div>
                    <div class="rbr"></div>
                </div>
            </div>
        </div>
    </div>
    <!--di-->
    <div class="diq">
        <div class="diy">
            <div class="di">
                <a>DT人才培训基地（太原中心）</a>|
                <a>晋ICP备16009028号</a>|
                <a>咨询热线：400-7777-699</a>|
                <a>地址：太原市高新区平阳南路龙兴街万立科技大厦17层</a>|
                <a>版权所有：华信智原</a>
            </div>
        </div>
    </div>
</body>
</html>
