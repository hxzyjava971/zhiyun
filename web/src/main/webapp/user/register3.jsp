<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--<html xmlns="http://www.w3.org/1999/xhtml">-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Author" contect="http://www.webqin.net">
<title>设置密码</title>
<!--动画-->
<link rel="stylesheet" href="/user/css/animate.css" type="text/css"/>
<!---->
<link rel="shortcut icon" href="/user/images/favicon.ico" />
<link type="text/css" href="/user/css/css.css" rel="stylesheet" />
    <script type="text/javascript" src="/user/js/jquery-1.8.3-min.js"></script>
    <script type="text/javascript" src="/user/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="/user/js/bootstrap.min.js"></script>
    <!--下拉-->
    <link rel="stylesheet" type="text/css" href="/user/fonts/iconfont.css">
    <script type="text/javascript" src="/user/js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/user/layui/css/layui.css">
</head>

<body>
<!--top-->
<div class="yxq">
	<a href="../页面/index.html"><div class="bz">
    	<img src="/user/img/biaozhi.jpg"/>
    </div></a>
    <%--<ul class="nav">--%>
    	<%--<li>老师入口</li>--%>
        <%--<li class="na">学生入口</li>--%>
    <%--</ul>--%>
</div>


<!---->

  <div class="content">
  <div class="wow fadeInRight bjtx" data-wow-duration="3s">
  	<img src="/user/img/beij.png"/>
  </div>
   <div class="web-width">
     <div class="for-liucheng">
      <div class="liulist for-cur"></div>
      <div class="liulist for-cur"></div>
      <div class="liulist for-cur"></div>
      <div class="liulist"></div>
      <div class="liutextbox">
       <div class="liutext for-cur"><strong>验证手机</strong><br /><em>1</em></div>
       <div class="liutext for-cur"><strong>填写信息</strong><br /><em>2</em></div>
       <div class="liutext for-cur"><strong>设置新密码</strong><br /><em>3</em></div>
       <div class="liutext"><strong>注册完成</strong><br /><em>4</em></div>
      </div>
     </div><!--for-liucheng/-->


     <form action="${pageContext.request.contextPath}/user/student/ZcThree" method="post" class="forget-pwd" id="formZC3">
         <%--<input type="hidden" value="${student.nickName}" name="nickName" />--%>
         <%--<input type="hidden" value="${student.phoneNum}" name="phoneNum" />--%>
         <%--<input type="hidden" value="${student.email}" name="email" />--%>
         <%--<input type="hidden" value="${student.name}" name="name" />--%>
         <%--<input type="hidden" value="${classId}" name="classId" />--%>
             <p id="prompt" style="color: red ; margin-left: 150px"></p>
       <dl>
        <dt>密码：</dt>
        <dd><input type="password" placeholder="密码6-20位,至少包括数字和英文" name="password" id="password1"/></dd>
        <div class="clears"></div>
       </dl>
       <dl>
        <dt>确认密码：</dt>
        <dd><input type="password" name="password2" id="password2"/></dd>
        <div class="clears"></div>
       </dl>
       <div class="subtijiao"><input type="submit" value="提交" /></div>
      </form><!--forget-pwd/-->


      <%--<form action="register2.jsp" method="get" class="forget-pwd fh_1">--%>
       <%--<div class="subtijiao"><input type="submit" value="返回上一步" /></div>--%>
      <%--</form>--%>
   </div><!--web-width/-->
  </div><!--content/-->
<!--bottom-->
  <div class="bottom">
  	<ul class="tm">
    	<li>智云</li>
        <li>帮助中心</li>
        <li>学习中心</li>
        <li class="yq">友情链接</li>
    </ul>
    <h6>DT人才培训基地（太原中心）&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;晋ICP备16009028号&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;咨询热线：400-7777-699&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;地址：太原市高新区平阳南路龙兴街万立科技大厦17层&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;版权所有：华信智原</h6>
  </div>
<!---->
   <!--动画-->
    <script src="/user/js/wow.min.js"></script>
<script>
    $(function () {
        $("#password1").blur(checkPass1);
        $("#password2").blur(checkPass2);
        $("#formZC3").submit(function () {
            return checkPass1() && checkPass2() && checkPass3() ;
        })
        function checkPass1() {
            var password1 = $("#password1").val();
            var reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/;
            var flag = reg.test(password1);
            if(password1 == ""){
                $("#prompt").html("密码栏不能为空");
            }else {
                if (flag) {
                    $("#password1").css("border", "1px solid rgba(255, 255, 255, 0.4)");
                    $("#prompt").html("");
                } else {
                    $("#prompt").html("密码格式不正确");
                }
            }
            return flag;
        }

        function checkPass2() {
            var password2 = $("#password2").val();
            var reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/;
            var flag = reg.test(password2);
            if(password2 == ""){
                $("#prompt").html("确认密码栏不能为空");
            }else {
                if (flag) {
                    $("#password2").css("border", "1px solid rgba(255, 255, 255, 0.4)");
                    $("#prompt").html("");
                } else {
                    $("#prompt").html("确认密码栏格式不正确");
                }
            }
            return flag;
        }
        function checkPass3() {
            var password1 = $("#password1").val();
            var password2 = $("#password2").val();
            if(password1 == password2){
                $("#prompt").html("");
                return true;
            }else {
                $("#prompt").html("两次输入密码不一致");
                return false;
            }
        }
    })

    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))){
        new WOW().init();
    }

</script>
<!---->
</body>
</html>
