<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>修改个人信息</title>
    <!--上传图片代码-->
    <link rel="stylesheet" type="text/css" href="/user/css/globle.css"/>
    <script type="text/javascript" src="/user/js/jquery.min (2).js"></script>
    <!--上传封面代码-->
    <link href="/user/css/bootstrap.min.css" rel="stylesheet">
    <link href="/user/css/bootstrap-fileinput.css" rel="stylesheet">
    <!--动画-->
    <link rel="stylesheet" href="/user/css/animate.css" type="text/css"/>
    <link href="/user/css/style1.css" rel="stylesheet" type="text/css"/>
    <style>
        .qued a {
            width: 200px;
            height: 48px;
            border-radius: 6px;
            display: block;
            text-align: center;
            line-height: 48px;
            background: rgba(243, 51, 84, 1.00);
            float: left;
            color: #fff;
            font-size: 18px;
            margin: 30px 50px 30px 0;
        }
    </style>
</head>
<body>
<!--top-->
<jsp:include page="top.jsp"></jsp:include>
<!--上传-->
<div class="yxq dpz">
    <div class="wow pulse pg" data-wow-iteration="5" data-wow-duration="0.2s">个人信息</div>
    <div class="dp">
        <div class="zy">
            <h4 class="wow zoomInLeft">上传头像</h4>
            <h5 class="wow slideInLeft" data-wow-delay="1.5s">注：支持jpg/gif/png格式 rgb模式，不超过1M！<font >上传正方形头像,效果更佳。</font></h5>
            <!--上传封面代码-->
            <div class="wow slideInLeft container" data-wow-delay="0.5s">
                <div class="page-header">
                    <form id="ajaxForm" enctype="multipart/form-data">
                        <div class="form-group" id="uploadForm">
                            <div class="fileinput fileinput-new" data-provides="fileinput" id="exampleInputUpload">
                                <div class="fileinput-new thumbnail"
                                     style="width: 200px;height:200px; border-radius: 100%;">
                                    <img id='img' style="width: 100%;border-radius: 100%"
                                         src="${student.header}" alt=""/>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="width: 200px; height: 200px; border-radius: 100%;" id="imgs">111
                                </div>
                                <div>
                        <span class="btn btn-primary btn-file">
                            <span class="fileinput-new" id="sub">选择文件</span>
                            <span class="fileinput-exists">换一张</span>
                            <input type="file" name="pic1" id="picID" accept="image/gif,image/jpeg,image/x-png">
                        </span>
                                    <a href="javascript:;" class="btn btn-warning fileinput-exists"
                                       data-dismiss="fileinput">移除</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <script src="/user/js/jquery.min.js"></script>
            <script src="/user/js/bootstrap-fileinput2.js"></script>
            <!---->
            <script src="/user/js/webuploader.min.js"></script>
            <script src="/user/js/diyUpload.js"></script>
        </div>
        <div class="pf">
            <form action="${pageContext.request.contextPath}/user/student/updatePersonalMessage" method="post"
                  id="updateForm">
                <div class="wow bounceInRight zpxx" data-wow-delay="1s">
                    <input type="hidden" value="${student.header}" name="header" id="headerImgHidden" hidden/>
                    <input type="text" value="${student.id}" name="id" hidden/>
                    <input type="text" value="${student.password}" name="password" hidden/>
                    <input type="text" value="${student.statusStr}" name="status" hidden/>
                    <%--<input type="text" value="${student.classes.id}" name="classId" hidden/>--%>
                    <div class="mc">
                        <span>姓名（不可修改）</span>
                        <input type="text" value="${student.name}" name="name" readonly/>
                    </div>
                    <div class="mc">
                        <span>手机号（不可修改）</span>
                        <input type="text" value="${student.phoneNum}" name="phoneNum" id="phoneNum" readonly/>
                    </div>
                    <div class="mc">
                        <span>昵称</span>
                        <input type="text" value="${student.nickName}" name="nickName"/>
                    </div>
                    <div class="mc">
                        <span>一句话个性签名</span>
                        <input type="text" value="${student.introduction}" name="introduction"/>
                    </div>
                    <div class="mc2">
                        <span id="classSpan">班级</span>
                        <select class="form-control" name="classId" id="classId" style="height: 50px"></select>
                    </div>
                </div>

                <div class="wow bounceInRight zpxx zplb" data-wow-delay="1.5s">
                    <div class="bq1">
                        <span>性别：</span>
                        <c:if test="${student.sex=='男'}">
                            <input type="radio" id="yc" value="男" name="sex" class="zpin" checked="checked"/><label
                                for="yc">男</label>
                            <input type="radio" id="lm" value="女" name="sex" class="zpin"/><label for="lm">女</label>
                        </c:if>
                        <c:if test="${student.sex=='女'}">
                            <input type="radio" id="yc" value="男" name="sex" class="zpin"/><label for="yc">男</label>
                            <input type="radio" id="lm" value="女" name="sex" class="zpin" checked="checked"/><label
                                for="lm">女</label>
                        </c:if>
                        <c:if test="${student.sex==null}">
                            <input type="radio" id="yc" value="男" name="sex" class="zpin" checked="checked"/><label
                                for="yc">男</label>
                            <input type="radio" id="lm" value="女" name="sex" class="zpin"/><label for="lm">女</label>
                        </c:if>
                    </div>
                </div>
                <div class="wow bounceInDown qued" data-wow-delay="1.5s">
                    <a href="#" id="fromSubBtn">修改</a>
                    <a href="javascript:history.go(-1)" class="fh_1">返回</a>
                </div>
            </form>
        </div>
    </div>
</div>

<!--bottom-->
<div class="ds bo">
    <div class="yxq bo_1">
        <ul class="boz">
            <li class="boz_1">
                <h4>智云
                    <div class="boz1"></div>
                </h4>
                <ul class="boz_2">
                    <li>关于智云</li>
                    <li>网站协议</li>
                    <li>法律声明</li>
                </ul>
            </li>
            <li class="boz_1">
                <h4>帮助中心
                    <div class="boz1"></div>
                </h4>
                <ul class="boz_2">
                    <li>常见问题</li>
                    <li>联系我们</li>
                    <li>意见反馈</li>
                </ul>
            </li>
            <li class="boz_1">
                <h4>学习中心
                    <div class="boz1"></div>
                </h4>
                <ul class="boz_2">
                    <li>学习指南</li>
                    <li>学员中心</li>
                    <li>版权声明</li>
                </ul>
            </li>
            <li class="boz_1">
                <h4>友情链接
                    <div class="boz1"></div>
                </h4>
                <ul class="boz_2">
                    <li>华信智原</li>
                    <li>智原在线</li>
                </ul>
            </li>
        </ul>
        <div class="boy">
            <div class="bz_2">
                <p></p>
                <h1></h1>
            </div>
            <div class="lx">
                <div class="lxz">
                    <h6>报名热线：400-7777-699</h6>
                    <ul class="tb_1">
                        <li>
                            <img src="/user/image_page/wb.png"/>
                        </li>
                        <li class="tb_3">
                            <img src="/user/image_page/qq.png"/>
                        </li>
                        <li class="tb_2">
                            <img src="/user/image_page/wx.png"/>
                        </li>
                    </ul>
                </div>
                <div class="lxy"></div>
            </div>

        </div>
    </div>
    <div class="x_1"></div>
    <h6 class="bo_2">
        DT人才培训基地（太原中心）&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;晋ICP备16009028号&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;咨询热线：400-7777-699&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;地址：太原市高新区平阳南路龙兴街万立科技大厦17层&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;版权所有：华信智原
    </h6>
</div>
<!--动画-->
<script src="/user/js/wow.min.js"></script>
<script>
    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))) {
        new WOW().init();
    }
    ;
    $(function () {
        $("#fromSubBtn").click(function () {
            $("#updateForm").submit();
        });
        var studentSchool = ${student.classes.school.id};
        var studentClass = ${student.classes.id};
        $.get("/user/school/fcbs",{"schoolId":studentSchool},function (data) {
            debugger
            var str = "";
            for(var i = 0;i<data.length;i++){
                if(data[i].id == studentClass){
                    str += "<option value='"+data[i].id+"' selected>"+data[i].className+"</option>";
                }else{
                    str += "<option value='"+data[i].id+"'>"+data[i].className+"</option>";
                }
            }
            $("#classId").html(str);
        })

    });
</script>
<!---->
</body>
</html>
