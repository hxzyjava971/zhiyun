<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <meta charset="utf-8">
    <title>作业批改</title>
    <!--点评代码-->
    <link type="text/css" href="${pageContext.request.contextPath}/user/css/style (6).css" rel="stylesheet"/>
    <!---->
    <!--动画-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/animate.css" type="text/css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/jquery.min.js"></script>
    <!---->
    <link href="${pageContext.request.contextPath}/user/css/style1.css" rel="stylesheet" type="text/css"/>
    <style>
        .bigimg {
            width: 600px;
            position: fixed;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            display: none;
            z-index: 9999;
            border: 10px solid #fff;
        }

        .mask {
            position: fixed;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            background-color: #000;
            opacity: 0.5;
            filter: Alpha(opacity=50);
            z-index: 98;
            transition: all 1s;
            display: none
        }

        .bigbox {
            width: 234px;
            background: #fff;
            border: 1px solid #ededed;
            margin: 0 auto;
            border-radius: 10px;
            overflow: hidden;
            padding: 10px;
        }

        .bigbox > .imgbox {
            width: 400px;
            height: auto;
            float: left;
            border-radius: 5px;
            overflow: hidden;
            margin: 0 10px 10px 0px;
        }

        .bigbox > .imgbox > img {
            width: 100%;
        }

        .imgbox:hover {
            cursor: zoom-in
        }

        .mask:hover {
            cursor: zoom-out
        }

        .mask > img {
            position: fixed;
            right: 22px;
            top: 80px;
            width: 60px;
        }

        .mask > img:hover {
            cursor: pointer
        }
    </style>
</head>
<body>
<!--top-->
<jsp:include page="top.jsp"></jsp:include>
<!--点评-->
<form id="myform" action="${pageContext.request.contextPath}/user/homework/commentHomework" method="post">
    <input type="hidden" name="teacherId" value="${teacher.id}">
    <input type="hidden" name="id" value="${homework.id}">
    <div class="yxq dpz">
        <div class="pulse pg" data-wow-iteration="5">作业批改</div>
        <div class="dp">
            <div class="zy">
                <h4 class="zoomInLeft">
                    <c:if test="${fn:length(homework.students) == 1}">
                        <c:forEach items="${homework.students}" var="student1">
                            ${student1.name}
                        </c:forEach>
                    </c:if>
                    <c:if test="${fn:length(homework.students) != 1} ">
                        <c:forEach items="${homework.students}" var="student1">
                            ${student1.name}-
                        </c:forEach>
                    </c:if>${homework.typeStr}</h4>

                <h2 class="zoomInLeft">${homework.workName}</h2>
                <ul id='box' class="slideInLeft tp">
                    <li>
                        <div class="bigbox">
                            <div class="imgbox">
                                <img src="${homework.coverImg}" class="smallimg" alt="" style="width: 58%">
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <img src="" alt="" class="bigimg">
            <div class="mask">
                <img src="${pageContext.request.contextPath}/user/image_page/close.png" alt="" >
            </div>
            <div class="pf">
                <div class="bounceInUp zyzsl">
                    ${fn:replace(homework.instruction, "<img", "<img class='smallimg'")}
                </div>
            </div>
                <!--点评代码-->
                <div class="order-evaluation clearfix">
                    <div class="bounceInRight box box_1">
                        <h4>作业评分</h4>
                        <p>云值：<input type="text" class="yz" name="score" id="score">分</p>
                        <div class="block">
                            <ul>
                                <li data-default-index="0">
                                    <span>星级评价：</span>
                                    <span>
                                        <c:forEach var="x" begin="0" end="4" step="1">
                                            <img src="${pageContext.request.contextPath}/user/img/x1.png"
                                                 style="width: 28px;height: 28px">
                                        </c:forEach>
				                    </span>
                                    <em class="level"></em>
                                </li>
                            </ul>
                        </div>
                        <input type="hidden" name="star" id="starThing">
                        <div class="order-evaluation-checkbox">
                            <p class="bqp">标签评价：</p>
                            <ul class="clearfix ck1">
                                <c:forEach items="${sessionScope.assess}" var="assess">
                                    <li class="order-evaluation-check">${assess.description}<i
                                            class="iconfont icon-checked"></i></li>
                                </c:forEach>
                                <li class="order-evaluation-check diyLabel" data-impression="8"><input type="text"
                                                                                                       placeholder="+添加标签"
                                                                                                       class="bq_2 "/><i
                                        class="iconfont icon-checked"></i></li>
                                <li class="order-evaluation-check diyLabel" data-impression="9"><input type="text"
                                                                                                       placeholder="+添加标签"
                                                                                                       class="bq_2 "/><i
                                        class="iconfont icon-checked"></i></li>
                            </ul>
                            <input type="hidden" name="tagEvaluate" id="labelZeal">
                        </div>
                    </div>
                    <div class="bounceInRight box">
                        <h4>评语点评</h4>
                        <div class="order-evaluation-textarea">
                            <textarea name="descEvaluate" id="TextArea1" onKeyUp="words_deal();"
                                      placeholder="请输入评语"></textarea>
                            <p>还可以输入<em id="textCount">140</em>个字</p>
                        </div>
                    </div>
                    <a class="bounceInDown" href="#" id="order_evaluation"
                       onclick="document.getElementById('myform').submit();return false;">确定 </a>
                    <a  href="${pageContext.request.contextPath}/user/homework/findNotCommentHomeworkByTeacher" type="button" class="btn bg-default">返回</a>
                </div>
                <div id="order_evaluate_modal" class="dmlei_tishi_info"></div>

                <script type="text/javascript">
                    /*
                     * 根据index获取 str
                     * **/
                    function byIndexLeve(index) {
                        var str = "";
                        var strr = "";
                        switch (index) {
                            case 0:
                                str = "较差";
                                strr = 1;
                                break;
                            case 1:
                                str = "中等";
                                strr = 2;
                                break;
                            case 2:
                                str = "一般";
                                strr = 3;
                                break;
                            case 3:
                                str = "良好";
                                strr = 4;
                                break;
                            case 4:
                                str = "优秀";
                                strr = 5;
                                break;
                        }
                        return str;
                    }

                    function byIndexLeve1(index) {
                        var str = "";
                        var strr = "";
                        switch (index) {
                            case 0:
                                strr = 0;
                                break;
                            case 1:
                                strr = 1;
                                break;
                            case 2:
                                strr = 2;
                                break;
                            case 3:
                                strr = 3;
                                break;
                            case 4:
                                strr = 4;
                                break;
                            case 5:
                                strr = 5;
                                break;
                        }
                        return strr;
                    }

                    //  星星数量
                    var stars = [
                        ['${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x1.png', '${pageContext.request.contextPath}/user/img/x1.png', '${pageContext.request.contextPath}/user/img/x1.png', '${pageContext.request.contextPath}/user/img/x1.png'],
                        ['${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x1.png', '${pageContext.request.contextPath}/user/img/x1.png', '${pageContext.request.contextPath}/user/img/x1.png'],
                        ['${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x1.png', '${pageContext.request.contextPath}/user/img/x1.png'],
                        ['${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x1.png'],
                        ['${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x2.png', '${pageContext.request.contextPath}/user/img/x2.png'],
                    ];
                    $(".block li").find("img").hover(function (e) {
                        var obj = $(this);
                        var index = obj.index();
                        if (index < (parseInt($(".block li").attr("data-default-index")) - 1)) {
                            return;
                        }
                        var li = obj.closest("li");
                        var star_area_index = li.index();
                        for (var i = 0; i < 5; i++) {
                            li.find("img").eq(i).attr("src", stars[index][i]);//切换每个星星
                        }
                        $(".level").html(byIndexLeve(index));
                    }, function () {
                    })

                    $(".block li").hover(function (e) {
                    }, function () {
                        var index = $(this).attr("data-default-index");//点击后的索引
                        index = parseInt(index);
                        $(".level").html(byIndexLeve(index - 1));
                        $(".order-evaluation ul li:eq(0)").find("img").attr("src", "${pageContext.request.contextPath}/user/img/x1.png");
                        for (var i = 0; i < index; i++) {
                            $(".order-evaluation ul li:eq(0)").find("img").eq(i).attr("src", "${pageContext.request.contextPath}/user/img/x2.png");
                        }
                    })
                    $(".block li").find("img").click(function () {
                        var obj = $(this);
                        var li = obj.closest("li");
                        var star_area_index = li.index();
                        var index1 = obj.index();
                        li.attr("data-default-index", (parseInt(index1) + 1));
                        var index = $(".block li").attr("data-default-index");//点击后的索引
                        index = parseInt(index);
                        console.log(index);
                        $(".level").html(byIndexLeve(index - 1));
                        $("#starThing").val(byIndexLeve1(index));

                        $(".order-evaluation ul li:eq(0)").find("img").attr("src", "${pageContext.request.contextPath}/user/img/x1.png");
                        for (var i = 0; i < index; i++) {
                            $(".order-evaluation ul li:eq(0)").find("img").eq(i).attr("src", "${pageContext.request.contextPath}/user/img/x2.png");
                        }

                    });
                    //印象
                    $(".order-evaluation-check").click(function () {
                        if ($(this).hasClass('checked')) {
                            //当前为选中状态，需要取消
                            $(this).removeClass('checked');
                        } else {
                            //当前未选中，需要增加选中
                            $(this).addClass('checked');
                        }
                        $("#labelZeal").val(label_return())
                    });

                    $(".diyLabel").change(function () {
                        $("#labelZeal").val(label_return())
                    })

                    function label_return() {
                        var tree1 = document.getElementsByClassName("order-evaluation-check");
                        var li = $(".order-evaluation-check");
                        var list = new Array()
                        for (var i = 0; i < tree1.length; i++) {
                            if ($(li[i]).hasClass('diyLabel') && $(li[i]).hasClass('checked')) {
                                list[i] = $(li[i]).find("input").val();
                            } else if ($(li[i]).hasClass('checked')) {
                                list[i] = $(li[i]).text().slice(0, -1);
                            }
                        }
                        return list;
                    };

                    //评价字数限制
                    function words_deal() {
                        var curLength = $("#TextArea1").val().length;
                        if (curLength > 140) {
                            var num = $("#TextArea1").val().substr(0, 140);
                            $("#TextArea1").val(num);
                            alert("超过字数限制，多出的字将被截断！");
                        }
                        else {
                            $("#textCount").text(140 - $("#TextArea1").val().length);
                        }
                    }

                    $("#order_evaluation").click(function () {
                        $("#order_evaluate_modal").html("感谢您的评价！么么哒(* ￣3)(ε￣ *)").show().delay(3000).hide(500);
                    })
                </script>
            </div>
        </div>
    </div>
</form>

<!--bottom-->
<jsp:include page="bottom.jsp"></jsp:include>
<!--动画-->
<%--<script src="${pageContext.request.contextPath}/user/js/wow.min.js"></script>--%>
<script>
    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))) {
        new WOW().init();
    }
    ;
</script>

<script src="${pageContext.request.contextPath}/user/js/zoom.js"></script>
<script>
    $(function () {
        /*
         smallimg   // 小图
         bigimg  //点击放大的图片
         mask   //黑色遮罩
         */
        var obj = new zoom('mask', 'bigimg','smallimg');
        obj.init();
    })

    function isNumber(str) {
        var n = Number(str);
        if (!isNaN(n)) {
            if (str.indexOf(".") == -1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    $("#score").blur(function () {
        var score = $(this).val()
        if (isNumber(score)) {
            if (score < 0 || score > 100) {
                alert("请输入0-100的整数！")
            }
        } else {
            alert("请输入0-100的整数！")
        }
    })
</script>

</body>
</html>
