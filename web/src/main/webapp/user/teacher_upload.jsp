<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>上传精品作业</title>
    <!--上传图片代码-->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/globle.css"/>
    <script src="${pageContext.request.contextPath}/user/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/user/js/bootstrap-fileinput3.js"></script>
    <!--上传封面代码-->
    <link href="${pageContext.request.contextPath}/user/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/user/css/bootstrap-fileinput.css" rel="stylesheet">
    <!--动画-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/animate.css" type="text/css"/>
    <link href="${pageContext.request.contextPath}/user/css/style1.css" rel="stylesheet" type="text/css"/>
    <style>
        #editor {
            width: 836px;
            margin: 0 auto;
        }
        .w-e-text-container {
            height: 500px !important; /*!important是重点，因为原div是行内样式设置的高度300px*/
        }
        .bt {
            margin-left: 35px;
        }
        .qued a {
            width: 200px;
            height: 48px;
            border-radius: 6px;
            display: block;
            text-align: center;
            line-height: 48px;
            background: rgba(243, 51, 84, 1.00);
            float: left;
            color: #fff;
            font-size: 18px;
            margin: 30px 50px 30px 0;
        }
    </style>
</head>
<body>
<!--top-->
<jsp:include page="top.jsp"></jsp:include>
<!--上传-->
<div class="yxq dpz">
    <div class="pulse pg" data-wow-iteration="5" data-wow-duration="0.2s">上传精品作业</div>
    <div class="dp">
        <div class="zy">
            <h4 class="zoomInLeft">上传封面</h4>
            <h5 class="slideInLeft" data-wow-delay="1.5s">注：支持jpg/gif/png格式 rgb模式，不超过1m!</h5>
            <!--上传封面代码-->
            <div class="slideInLeft container" data-wow-delay="0.5s">
                <div class="page-header">
                    <form id="ajaxForm" enctype="multipart/form-data">
                        <div class="form-group" id="uploadForm">
                            <div class="fileinput fileinput-new" data-provides="fileinput" id="exampleInputUpload">
                                <div class="fileinput-new thumbnail"
                                     style="width: 200px;height: auto;max-height:150px;">
                                    <img id='img' style="width: 100%;height: auto;max-height: 140px;"
                                         src="${pageContext.request.contextPath}/user/images/noimage.png" alt=""/>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 200px; max-height: 150px;" id="imgs">
                                </div>
                                <div>
                                    <span class="btn btn-primary btn-file">
                                        <span class="fileinput-new" id="sub">选择文件</span>
                                        <span class="fileinput-exists">换一张</span>
                                        <input type="file" name="pic1" id="picID"
                                               accept="image/gif,image/jpeg,image/x-png">
                                    </span>
                                    <a href="javascript:;" class="btn btn-warning fileinput-exists"
                                       data-dismiss="fileinput">移除</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="pf">
            <form method="post" id="form1" action="${pageContext.request.contextPath}/user/teacher/addQualityWork">
                <div id="div4" style="display: none"></div>
                <input name="coverImg" id="coverImg" type="hidden">
                <input name="studentId" value="${studentId}" type="hidden">
                <div class="bounceInRight zpxx" data-wow-delay="1s">
                    <p class="bt">作品名称：</p>
                    <div class="mc"><input type="text" placeholder="请输入作品名称" name="workName"/>
                        <span>限50字以下</span>
                    </div>
                    <p class="bt">作品介绍：</p>
                    <div class="mc"><input type="text" placeholder="请输入作品介绍" name="jobdescription"/>
                        <span>限50字以下</span>
                    </div>
                    <p class="bt">作品内容<span style="color: #999">（注：不要上传超过5M的图片！）：</span></p>
                    <div id="editor"></div>
                    <textarea id="text1" name="instruction" hidden></textarea>
                    <!-- 注意， 只需要引用 JS，无需引用任何 CSS ！！！-->
                    <script type="text/javascript"
                            src="${pageContext.request.contextPath}/user/js/wangEditor.min.js"></script>
                    <script type="text/javascript">
                        var E = window.wangEditor
                        var editor = new E('#editor')
                        // 自定义菜单配置
                        editor.customConfig.menus = [
                            'image',  // 插入图片
                            'bold',  // 粗体
                            'fontSize',  // 字号
                            'fontName',  // 字体
                            'italic',  // 斜体
                            'underline',  // 下划线
                            'foreColor',  // 文字颜色
                            'backColor',  // 背景颜色
                            'justify',  // 对齐方式
                            'undo',  // 撤销
                        ]
                        // editor.customConfig.uploadImgShowBase64 = true   // 使用 base64 保存图片
                        // 上传图片到服务器
                        editor.customConfig.uploadFileName = 'file'; //设置文件上传的参数名称
                        editor.customConfig.uploadImgServer = '${pageContext.request.contextPath}/user/homework/upload2'; //设置上传文件的服务器路径
                        editor.customConfig.uploadImgMaxSize = 5 * 1024 * 1024; // 将图片大小限制为 5M
                        //自定义上传图片事件
                        editor.customConfig.uploadImgHooks = {
                            before: function (xhr, editor, files) {
                            },
                            success: function (xhr, editor, result) {
                                console.log("上传成功");
                            },
                            fail: function (xhr, editor, result) {
                                console.log("上传失败,原因是" + result);
                            },
                            error: function (xhr, editor) {
                                console.log("上传出错");
                            },
                            timeout: function (xhr, editor) {
                                console.log("上传超时");
                            }
                        }

                        var $text1 = $('#text1')
                        editor.customConfig.onchange = function (html) {
                            // 监控变化，同步更新到 textarea
                            $text1.val(html)
                        }
                        editor.create()
                    </script>
                </div>
                <div class=" bounceInRight zpxx zplb" data-wow-delay="1.5s">
                    <div class="lx1">
                        <span>类型：</span>
                        <c:forEach items="${types}" var="type" varStatus="vs">
                            <c:if test="${vs.index == 0}">
                                <input type="radio" value="${type.description}" name="type" id="type+${type.id}" checked/>
                                <label for="type+${type.id}">${type.description}</label>
                            </c:if>
                            <c:if test="${vs.index != 0}">
                                <input type="radio" value="${type.description}" name="type" id="type+${type.id}"/>
                                <label for="type+${type.id}">${type.description}</label>
                            </c:if>
                        </c:forEach>
                    </div>
                    <div class=" bounceInDown qued" data-wow-delay="0.9s">
                        <a href='javascript:void(0)' onclick="queding()">确定</a>
                        <a href="javascript:history.go(-1)" class="fh_1">返回</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--bottom-->
<jsp:include page="bottom.jsp"></jsp:include>
<!--动画-->
<script src="${pageContext.request.contextPath}/user/js/wow.min.js"></script>
<script>
    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))) {
        new WOW().init();
    }
    function queding() {
        var i = $("#imgs").html();
        if (i.trim().length == 0) {
            alert("您未上传封面图，不能提交作业！")
        } else {
            $("#form1").submit();
        }
    }
    function add() {
        $("#div3").css("display", "block");
    }
    function del() {
        $("#div3").css("display", "none")
        $(".box").attr("checked", false);
    }
</script>
</body>
</html>
