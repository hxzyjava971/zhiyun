<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>智云</title>
    <!--侧边栏-->
    <style>
        * {
            margin: 0;
            padding: 0;
            list-style-type: none;
        }

        a, img {
            border: 0;
        }

        /* side */
        .side {
            position: fixed;
            width: 54px;
            height: 220px;
            right: 0;
            top: 630px;
            z-index: 100;
        }

        .side ul li {
            width: 54px;
            height: 54px;
            float: left;
            position: relative;
            border-bottom: 1px solid #444;
        }

        .side ul li .sidebox {
            position: absolute;
            width: 54px;
            height: 54px;
            top: 0;
            right: 0;
            transition: all 0.3s;
            background: #000;
            opacity: 0.8;
            filter: Alpha(opacity=80);
            color: #fff;
            font: 14px/54px "微软雅黑";
            overflow: hidden;
        }

        .side ul li .sidetop {
            width: 54px;
            height: 54px;
            line-height: 54px;
            display: inline-block;
            background: #000;
            opacity: 0.8;
            filter: Alpha(opacity=80);
            transition: all 0.3s;
        }

        .side ul li .sidetop:hover {
            background: #ae1c1c;
            opacity: 1;
            filter: Alpha(opacity=100);
        }

        .side ul li img {
            float: left;
        }
    </style>
    <!--切换-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/style (3).css">
    <!--轮播-->
    <link href="${pageContext.request.contextPath}/user/css/lrtk2.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/jquery-1.7.2.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/lrtk.js"></script>
    <!--就业轮播-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/script.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/shouyelunbo.css"/>
    <!--文章轮播-->
    <link type="text/css" href="${pageContext.request.contextPath}/user/css/lanrenzhijia2.css" rel="stylesheet"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/lanrenzhijia.js"></script>
    <!--动画-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/animate.css" type="text/css"/>
    <!--回到顶部-->
    <link href="${pageContext.request.contextPath}/user/css/style1.css" rel="stylesheet" type="text/css"/>
</head>
<body>

<!--top-->
<jsp:include page="top.jsp"></jsp:include>

<!--banner-->
<div class="banner">
    <!-- 轮播代码 开始 -->
    <div class="slide-main" id="touchMain">
        <div class="slide-box" id="slideContent">
            <c:forEach items="${headerImgs}" var="headerImg" varStatus="vs">
                <div class="slide">
                    <a stat="sslink-${vs.index}" target="_blank">
                        <div><img src="${headerImg.url}" width="100%"/></div>
                    </a>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<!--功能-->
<div class="ls">
    <div class="yxq">
        <ul class="wow bounceInDown ba_2" data-wow-delay="1s">
            <li>
                <div>
                    <img src="${pageContext.request.contextPath}/user/image_page/b_1.jpg"/>
                </div>
                <p>覆盖全国的就业网络</p>
            </li>
            <li>
                <div>
                    <img src="${pageContext.request.contextPath}/user/image_page/b_2.jpg"/>
                </div>
                <p>优质的就业服务体系</p>
            </li>
            <li>
                <div>
                    <img src="${pageContext.request.contextPath}/user/image_page/b_3.jpg"/>
                </div>
                <p>入学签订就业协议</p>
            </li>
            <li>
                <div>
                    <img src="${pageContext.request.contextPath}/user/image_page/b_4.jpg"/>
                </div>
                <p>独特的信息化就业库</p>
            </li>
            <li>
                <div>
                    <img src="${pageContext.request.contextPath}/user/image_page/b_5.jpg"/>
                </div>
                <p>订单式名企合作模式</p>
            </li>
        </ul>

    </div>
</div>

<!--学员作业-->
<div class="bb">
    <div class="xy">
        <div class=" xy_1">
            <div class="xybt">
                <p class="wow flipInX" data-wow-duration="2s">学员作业</p>
                <h6 class="wow bounceInUp" data-wow-delay="0.5s">Student assignments</h6>
            </div>
        </div>
        <!--切换-->
        <div class="wrap">
            <ul class="tabs group">
                <c:forEach items="${dictionaries}" var="dictionary">
                    <li><a class="neirong">${dictionary.description}</a></li>
                </c:forEach>
            </ul>
            <div id="content">
                <div id="one" class="qh">
                    <ul class="xy_3" id="tihuan"></ul>
                </div>
            </div>
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/user/js/index.js"></script>

    <a href="${pageContext.request.contextPath}/user/homework/findAll">
        <div class="more">
            <div class="more_1">MORE</div>
            <img src="${pageContext.request.contextPath}/user/image_page/jiant (2).png" class="jt"/>
        </div>
    </a>
    <div class="xy_4"></div>
</div>

<!--金牌导师-->
<div class="ds">
    <div class="wow flipInX xy_1">
        <div class="xybt ds_1">
            <p>金牌导师</p>
            <h6>Gold instructor</h6>
        </div>
    </div>
    <div class="yxq jg">
        <div class="wow bounceInLeft dsz" data-wow-delay="0.5s" data-wow-duration="0.8s">
            <img src="${pageContext.request.contextPath}/user/image/daoshi2.jpg"/>
        </div>
        <ul class="wow bounceInRight dsy" data-wow-delay="0.5s" data-wow-duration="0.8s">
            <c:forEach items="${teacherList}" var="teacher">
                <li class="t_2 yangnan">
                    <div><img src="${teacher.headerImg}" width="120" height="180"/></div>
                    <div class="jj">
                        <p>
                            <img src="${teacher.headerImg}"/>
                        </p>
                        <div class="jj_1">
                            <h2>${teacher.name}</h2>
                            <h3>${teacher.position}</h3>
                            <div class="x"></div>
                            <div class="wz">
                                <h5>${teacher.introduce}</h5>
                                <img src="${teacher.picture}"/>
                            </div>
                        </div>
                    </div>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>

<!--就业明星-->
<div class="bb">
    <div class="yxq mx">
        <div class="xy_1">
            <div class="wow bounceInUp xybt">
                <p>就业明星</p>
                <h6>Employment star</h6>
            </div>
        </div>
        <!--就业轮播-->
        <div class="wow zoomInLeft Div1" data-wow-delay="1s" data-wow-duration="1s">
            <b class="Div1_prev Div1_prev1"><img src="${pageContext.request.contextPath}/user/images/lizi_img011.png" title="上一页"/></b>
            <b class="Div1_next Div1_next1"><img src="${pageContext.request.contextPath}/user/images/lizi_img012.png" title="下一页"/></b>
            <div class="Div1_main">
                <c:forEach begin="0" end="5" step="1" var="index">
                    <ul class="div2">
                        <li class="mx1">
                            <h1 class="mm"></h1>
                            <div class="mx1_1">
                                <p>
                                    <img src="${studentList[index*4].header}"/>
                                </p>
                                <h3>${studentList[index*4].name}</h3>
                            </div>
                            <h5 class="mx1_2">${studentList[index*4].position}</h5>
                            <h2 class="mx1_4">${studentList[index*4].workDesc}
                                <div class="mx1_3">${studentList[index*4].company}</div>
                            </h2>
                            <h6>${studentList[index*4].company}
                                <div class="mx1_5">
                                        ${studentList[index*4].info}
                                </div>
                            </h6>
                        </li>
                        <li class="mx1 mx2">
                            <h1 class="mm mx2_1"></h1>
                            <div class="mx1_1">
                                <p>
                                    <img src="${studentList[index*4+1].header}"/>
                                </p>
                                <h3>${studentList[index*4+1].name}</h3>
                            </div>
                            <h5 class="mx1_2">${studentList[index*4+1].position}</h5>
                            <h2 class="mx1_4">${studentList[index*4+1].workDesc}
                                <div class="mx1_3 mx2_2">${studentList[index*4+1].company}</div>
                            </h2>
                            <h6>${studentList[index*4+1].company}
                                <div class="mx1_5 mx2_2">
                                        ${studentList[index*4+1].info}
                                </div>
                            </h6>
                        </li>
                        <li class="mx1 mx3">
                            <h1 class="mm mx3_1"></h1>
                            <div class="mx1_1">
                                <p>
                                    <img src="${studentList[index*4+2].header}"/>
                                </p>
                                <h3>${studentList[index*4+2].name}</h3>
                            </div>
                            <h5 class="mx1_2">${studentList[index*4+2].position}</h5>
                            <h2 class="mx1_4">${studentList[index*4+2].workDesc}
                                <div class="mx1_3 mx3_2">${studentList[index*4+2].company}</div>
                            </h2>
                            <h6>${studentList[index*4+2].company}
                                <div class="mx1_5 mx3_2">
                                        ${studentList[index*4+2].info}
                                </div>
                            </h6>
                        </li>
                        <li class="mx1 mx4">
                            <h1 class="mm mx4_1"></h1>
                            <div class="mx1_1">
                                <p>
                                    <img src="${studentList[index*4+3].header}"/>
                                </p>
                                <h3>${studentList[index*4+3].name}</h3>
                            </div>
                            <h5 class="mx1_2">${studentList[index*4+3].position}</h5>
                            <h2 class="mx1_4">${studentList[index*4+3].workDesc}
                                <div class="mx1_3 mx4_2">${studentList[index*4+3].company}</div>
                            </h2>
                            <h6>${studentList[index*4+3].company}
                                <div class="mx1_5 mx4_2">
                                        ${studentList[index*4+3].info}
                                </div>
                            </h6>
                        </li>
                    </ul>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
<!--最新文章-->
<!--文章轮播-->
<div class="ds">
    <div class="xy_1">
        <div class="wow pulse xybt ds_1" data-wow-iteration="5" data-wow-duration="0.25s">
            <p>最新文章</p>
            <h6>Latest article</h6>
        </div>
    </div>
    <div id="zSlider">
        <div class="wow bounceInLeft " data-wow-duration="1s" data-wow-delay="0.5s" id="picshow">
            <div id="picshow_img">
                <ul>
                    <li><img src="${pageContext.request.contextPath}/user/images/lanrenzhijia1.jpg"></li>
                    <li><img src="${pageContext.request.contextPath}/user/images/lanrenzhijia2.jpg"></li>
                    <li><img src="${pageContext.request.contextPath}/user/images/lanrenzhijia3.jpg"></li>
                    <li><img src="${pageContext.request.contextPath}/user/images/lanrenzhijia4.jpg"></li>
                    <li><img src="${pageContext.request.contextPath}/user/images/lanrenzhijia5.jpg"></li>
                    <li><img src="${pageContext.request.contextPath}/user/images/lanrenzhijia6.jpg"></li>
                </ul>
            </div>
            <div id="picshow_tx">
                <ul>
                    <li>
                        <h3><a href="#" target="_blank">iOS13深色UI要来了，国外老哥总结做好它的8个实用技巧</a></h3>
                        <p>这不仅仅是一篇经验，而是前辈用日积月累得工作心得，用笔记用脑想，最重要得是要走心。还有我是占位符不是简介。</p>
                    </li>
                    <li>
                        <h3><a href="#" target="_blank">市场图从设计到上传过程中需要注意的问题</a></h3>
                        <p>这不仅仅是一篇经验，而是前辈用日积月累得工作心得，用笔记用脑想，最重要得是要走心。还有我是占位符不是简介。</p>
                    </li>
                    <li>
                        <h3><a href="#" target="_blank">创意品牌Logo设计：“形神”兼具的设计，方显高级和质感</a></h3>
                        <p>这不仅仅是一篇经验，而是前辈用日积月累得工作心得，用笔记用脑想，最重要得是要走心。还有我是占位符不是简介。</p>
                    </li>
                    <li>
                        <h3><a href="#" target="_blank">基础图形引发吉祥物设计思路</a></h3>
                        <p>这不仅仅是一篇经验，而是前辈用日积月累得工作心得，用笔记用脑想，最重要得是要走心。还有我是占位符不是简介。</p>
                    </li>
                    <li>
                        <h3><a href="#" target="_blank">客家土楼PS大鱼海棠</a></h3>
                        <p>这不仅仅是一篇经验，而是前辈用日积月累得工作心得，用笔记用脑想，最重要得是要走心。还有我是占位符不是简介。</p>
                    </li>
                    <li>
                        <h3><a href="#" target="_blank">功能型图标设计风格总结</a></h3>
                        <p>图标设计是 UI设计中非常重要的一环，因为除了文字和图片的排版之外，在扁平时代我们能够传递给用户情绪和设计感的通道就是页面中的各种图形和图标了。</p>
                    </li>
                </ul>
            </div>
        </div>
        <div id="select_btn">
            <ul>
                <li class="wow bounceInRight" data-wow-delay="0.2s"><a href="#" target="_blank"><img
                        src="${pageContext.request.contextPath}/user/images/01.jpg"><span class="select_text">iOS13深色UI的8个实用技巧</span><span
                        class="select_date">2019/08/31</span></a></li>
                <li class="wow bounceInRight" data-wow-delay="0.4s"><a href="#" target="_blank"><img
                        src="${pageContext.request.contextPath}/user/images/02.jpg"><span class="select_text">市场图从设计到上传中注意的问题</span><span
                        class="select_date">2019/08/15</span></a></li>
                <li class="wow bounceInRight" data-wow-delay="0.6s"><a href="#" target="_blank"><img
                        src="${pageContext.request.contextPath}/user/images/03.jpg"><span
                        class="select_text">创意品牌Logo设计</span><span class="select_date">2019/08/03</span></a>
                </li>
                <li class="wow bounceInRight" data-wow-delay="0.8s"><a href="#" target="_blank"><img
                        src="${pageContext.request.contextPath}/user/images/04.jpg"><span class="select_text">基础图形引发吉祥物设计思路</span><span
                        class="select_date">2019/07/25</span></a></li>
                <li class="wow bounceInRight" data-wow-delay="1.0s"><a href="#" target="_blank"><img
                        src="${pageContext.request.contextPath}/user/images/05.jpg"><span
                        class="select_text">客家土楼PS大鱼海棠</span><span class="select_date">2019/07/20</span></a>
                </li>
                <li class="wow bounceInRight" data-wow-delay="1.2s"><a href="#" target="_blank"><img
                        src="${pageContext.request.contextPath}/user/images/06.jpg"><span class="select_text">功能型图标设计风格总结</span><span
                        class="select_date">2019/07/06</span></a></li>
            </ul>
        </div>
    </div>
</div>
<!---->

<!--回到顶部-->
<div id="backTop">
    <!--上传按钮-->
    <a href="${pageContext.request.contextPath}/user/student/addPre">
        <div class="wow bounceInUp scan">
            <div class="antb"></div>
            <h1>上传作品
                <h4>Uploading works</h4>
            </h1>
        </div>
    </a>
    <img src="${pageContext.request.contextPath}/user/image/goback4.gif" width="54" height="67">
</div>
<!--插入脚本-->
<script src="${pageContext.request.contextPath}/user/js/jquery-1.7.2.js"></script>
<script src="${pageContext.request.contextPath}/user/js/3huidaodingbu.js"></script>

<script>
    $(function () {
        $(".side ul li").hover(function () {
            $(this).find(".sidebox").stop().animate({"width": "124px"}, 200).css({
                "opacity": "1",
                "filter": "Alpha(opacity=100)",
                "background": "#ae1c1c"
            })
        }, function () {
            $(this).find(".sidebox").stop().animate({"width": "54px"}, 200).css({
                "opacity": "0.8",
                "filter": "Alpha(opacity=80)",
                "background": "#000"
            })
        });
    });

    //回到顶部函数
    function goTop() {
        $('html,body').animate({'scrollTop': 0}, 900);
    }
</script>
<!-- 代码部分end -->
<%--<底部>--%>
<jsp:include page="bottom.jsp"></jsp:include>
<!--动画-->
<script src="${pageContext.request.contextPath}/user/js/wow.min.js"></script>
<script src="${pageContext.request.contextPath}/user/js/jqthumb.min.js"></script>
<script>
    $(function () {
        search();
    })
    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))) {
        new WOW().init();
    }
    ;

    function search(nr) {
        var neirong = new Array();
        neirong.push(nr);
        $.get("/user/homework/findHomework",
            {
                "pageSize": 8,
                "neirong": neirong,
            }, function (pageData) {
                $("#tihuan").empty();
                var result = pageData.content;
                var str = "";
                for (var i = 0; i < result.length; i++) {
                    hw = result[i];
                    var schoolName = "", className = "", header = "", studentName = "", homeworksLength = "";
                    try {
                        schoolName = hw.students[0].classes.school.schoolName;
                    } catch (e) {
                    }
                    try {
                        className = hw.students[0].classes.className;
                    } catch (e) {
                    }
                    try {
                        header = hw.students[0].header;
                    } catch (e) {
                    }
                    try {
                        studentName = hw.students[0].name;
                    } catch (e) {
                    }
                    str += " <a href='${pageContext.request.contextPath}/user/jobdetails/message?id=" + hw.id + "' target=\"new\">\n" +
                        "                            <li class=\"wow slideInLeft xyzp\">\n" +
                        "                                <div class=\"xyt\">\n" +
                        "                                    <img src='" + hw.coverImg + "'/>\n" +
                        "                                </div>\n" +
                        "                                <div class=\"xylb\">\n" +
                        "                                    <div class=\"xyxq\">\n" +
                        "                                        <h2>" + hw.workName + "</h2>\n" +
                        "                                        <p class=\"xyxq_1\">【" + schoolName + "】" + className + "</p>\n" +
                        "                                        <div class=\"xq\">\n" +
                        "                                            <ul class=\"dz\">\n" +
                        "                                                <li class=\"dz_1\">\n" +
                        "                                                    <img src=\"${pageContext.request.contextPath}/user/image_page/yanjing.png\"/>\n" +
                        "                                                    <p>" + hw.clicks + "</p>\n" +
                        "                                                </li>\n" +
                        "                                                <li class=\"dz_1\">\n" +
                        "                                                    <img src=\"${pageContext.request.contextPath}/user/image_page/dianzan.png\"/>\n" +
                        "                                                    <p>" + hw.thumbsUp + "</p>\n" +
                        "                                                </li>\n" +
                        "                                            </ul>\n" +
                        "                                            <div>" + hw.tags + "</div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                    <div class=\"tx\">\n" +
                        "                                        <div class=\"tx1\">\n" +
                        "                                            <div class=\"tx_1\">\n" +
                        "                                                <img src='" + header + "'/>\n" +
                        "                                            </div>\n" +
                        "                                            <p class=\"li_1\">" + studentName + "</p>\n" +
                        "                                            <div class=\"clear\"></div>\n" +
                        "                                        </div>\n" +
                        "                                    </div>\n" +
                        "                                </div>\n" +
                        "                            </li>\n" +
                        "                        </a>"
                }
                $("#tihuan").append(str);
                $("#one").show();
                // 图片不变形配置
                imgNotChange();
            })
    }

    $(".neirong").click(function () {
        var neirong = $(this).text();
        search(neirong);
    })

    function imgNotChange() {
        // 图片不变形配置
        $('.xyt img').jqthumb({
            classname: 'jqthumb',
            width: 285,
            height: 190,
            showoncomplete: true
        });
        $('.tx_1 img').jqthumb({
            classname: 'jqthumb',
            width: 22,
            height: 22,
            showoncomplete: true
        });
    }
</script>
<!---->
</body>
</html>
