<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>待批改作业</title>
    <!--作业-->
    <!--动画效果-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/animate.css" type="text/css">
    <!--动画效果-->
    <!--fycj-->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/zxf_page.css">
    <!--cj-->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/style (4).css">
    <!--cj-->
    <link href="${pageContext.request.contextPath}/user/css/style-a.css" rel="stylesheet" type="text/css"/>
    <!--分页代码-->
    <link type="text/css" href="${pageContext.request.contextPath}/user/css/style (2).css" rel="stylesheet"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/jquery.min (4).js"></script>
    <!---->
    <!--分页代码1-->
    <!--切换-->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/assets/style.css">
    <!---->
    <!--自己-->
    <link href="${pageContext.request.contextPath}/user/css/style1.css" rel="stylesheet" type="text/css"/>
    <style>
        a {
            text-decoration: none;
        }
        a:hover {
            text-decoration: none;
        }
        #pageLimit{
            margin-top: 8px;
        }
        #pageLimit li{
            cursor: pointer;
        }
        .bo{
            margin-top: 20px;
        }
    </style>

</head>


<body>
<!--动画-->
<script src="${pageContext.request.contextPath}/user/js/wow.min.js"></script>
<script>
    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))) {
        new WOW().init();
    }
    ;
</script>
<!--动画-->
<%--<script src="/demos/googlegg.js"></script>--%>
<!--top-->
<jsp:include page="top.jsp"></jsp:include>
<!--banner-->
<div class="beijing">
    <div class="yxq">
        <div class="bj_1">
            <div class="wow swing bj1" data-wow-iteration="2"
                 style="background:url(${teacher.picture}) 0% 44% / 152% no-repeat; "></div>
            <div class="bj1_1">
                <h2 class="wow bounceInUp ">${teacher.name}</h2>
                <%--<h4 class="wow bounceInDown " data-wow-delay="0.5s">智云ID：20180821</h4>--%>
            </div>
        </div>
        <div class="wow flipInY bj_2">${teacher.introduction}</div>
    </div>
</div>

<!--关注-->
<div class="tab">
    <div class="box">
        <ul class="bounceInLeft menus" data-wow-delay="1s">
            <li class="bg">我的作业</li>
            <li>我的关注</li>
            <li>我的粉丝</li>
            <li>我的消息</li>
            <li>我的收藏</li>
        </ul>
        <div class="right">
            <div class="scroll">
                <div class="tab_right">
                    <div class="ypq">
                        <div class="ypt">
                            <div class=" bounceInRight yptrr" data-wow-delay="1s">待批改</div>
                            <div class=" bounceInRight yptll" data-wow-delay="1s"
                                 onclick="location.href='${pageContext.request.contextPath}/user/homework/findAlreadyCommentHomeworkByTeacher?teacherId=${teacher.id}'">
                                已批改
                            </div>
                            <select onchange="chooesClass(1,4)" id="chooesClasses" class="form-control"
                                    style=" width: 256px;margin-left: 581px;margin-top: 12px">
                                <c:if test="${classId == null}">
                                    <option value=-1>查询所有</option>
                                </c:if>
                                <c:forEach items="${classes}" var="classes">
                                    <c:if test="${classes.id == classId}">
                                        <option selected value= ${classes.id}>${classes.className}</option>
                                    </c:if>
                                    <c:if test="${classes.id != classId}">
                                        <option value= ${classes.id}>${classes.className}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>
                        <ul class="yp">
                            <c:forEach items="${homeworks.content}" var="homework">
                                <li class=" bounceInRight ypzy" data-wow-delay="1.2s">
                                    <div class="ypl">
                                        <img src="${homework.coverImg}" class="img_a"/>
                                    </div>
                                    <div class="ypr">
                                        <div class="ypry">
                                            <div class="ypryl"><a
                                                    href="${pageContext.request.contextPath}/user/homework/findById?teacherId=${teacher.id}&homeworkId=${homework.id}" target="_blank">${homework.typeStr}:${homework.workName}</a>
                                            </div>
                                            <div class="ypryr ypryr_a"><a
                                                    href="${pageContext.request.contextPath}/user/homework/findById?teacherId=${teacher.id}&homeworkId=${homework.id}" target="_blank">立即批改></a>
                                            </div>
                                        </div>
                                        <div class="ypre ypre_a">
                                            <span>提交人：${homework.students[0].name}</span>
                                            <span>提交时间：${homework.timeStr}</span>
                                        </div>
                                        <div class="ypri">
                                            <div class="yprir" id="dududu">
                                                <div class="ypril_a_a">${homework.tags}</div>
                                                <input type="hidden" value="${homework.content}" id="content">
                                                <div class="ypril_a_a">${homework.typeStr}</div>
                                            </div>
                                        </div>
                                        <div class="yprw">
                                            <div class="yprwl">作业介绍：</div>
                                            <div class="yprwr_a">
                                                    ${homework.instruction}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                    <%--<div class="zxf_pagediv"></div>--%>
                    <div class="box-footer fenye">
                        <div class="pull-left">
                            <div class="form-group form-inline" id="fenye">
                                总共${homeworks.totalPages}页，共${homeworks.totalElements}条数据。
                            </div>
                        </div>
                        <div>
                            <ul id="pageLimit" style="margin: 0"></ul>
                        </div>
                    </div>
                    <script src="${pageContext.request.contextPath}/user/js/jquery.min_b.js"
                            type="text/javascript"></script>
                    <script src="${pageContext.request.contextPath}/user/js/zxf_page.js"
                            type="text/javascript"></script>
                    <!--fycj-->
                </div>
                <div class="tab_right">
                    <div class="tm_1">关注</div>
                    <hr/>
                    <ul class="ta_1">
                        <li class="li_2">
                            <div class="ljt1">
                                <div class="lj"></div>
                            </div>
                            <div class="tx_2"><img
                                    src="${pageContext.request.contextPath}/user/image_page/guanzhu/yezi.jpg"/></div>
                            <h4 class="mz">叶子老师</h4>
                            <h5 class="li_3">西安校区&nbsp;｜&nbsp;ui高级导师
                                <ul class="ul1">
                                    <li><span>创作</span>&nbsp;79</li>
                                    <li><span>粉丝</span>&nbsp;1548</li>
                                </ul>
                            </h5>

                        </li>
                        <li class="li_2">
                            <div class="ljt1">
                                <div class="lj"></div>
                            </div>
                            <div class="tx_2"><img
                                    src="${pageContext.request.contextPath}/user/image_page/guanzhu/yyn.jpg"/></div>
                            <h4 class="mz">伊元娜</h4>
                            <h5 class="li_3">太原校区&nbsp;｜&nbsp;ui设计65班
                                <ul class="ul1">
                                    <li><span>创作</span>&nbsp;13</li>
                                    <li><span>粉丝</span>&nbsp;562</li>
                                </ul>
                            </h5>

                        </li>
                        <li class="li_2 li_1">
                            <div class="ljt1">
                                <div class="lj"></div>
                            </div>
                            <div class="tx_2"><img
                                    src="${pageContext.request.contextPath}/user/image_page/guanzhu/tzc.jpg"/></div>
                            <h4 class="mz">谭志超</h4>
                            <h5 class="li_3">北京校区&nbsp;｜&nbsp;电商运营914班
                                <ul class="ul1">
                                    <li><span>创作</span>&nbsp;123</li>
                                    <li><span>粉丝</span>&nbsp;459</li>
                                </ul>
                            </h5>

                        </li>
                        <li class="li_2">
                            <div class="ljt1">
                                <div class="lj"></div>
                            </div>
                            <div class="tx_2"><img
                                    src="${pageContext.request.contextPath}/user/image_page/guanzhu/cuu.jpg"/></div>
                            <h4 class="mz">蔡UU老师</h4>
                            <h5 class="li_3">南京校区&nbsp;｜&nbsp;就业指导老师
                                <ul class="ul1">
                                    <li><span>创作</span>&nbsp;142</li>
                                    <li><span>粉丝</span>&nbsp;1420</li>
                                </ul>
                            </h5>

                        </li>
                        <li class="li_2">
                            <div class="ljt1">
                                <div class="lj"></div>
                            </div>
                            <div class="tx_2"><img
                                    src="${pageContext.request.contextPath}/user/image_page/guanzhu/sxn.jpg"/></div>
                            <h4 class="mz">苏向楠</h4>
                            <h5 class="li_3">广州校区&nbsp;｜&nbsp;UI设计823班
                                <ul class="ul1">
                                    <li><span>创作</span>&nbsp;102</li>
                                    <li><span>粉丝</span>&nbsp;1140</li>
                                </ul>
                            </h5>

                        </li>
                        <li class="li_2 li_1">
                            <div class="ljt1">
                                <div class="lj"></div>
                            </div>
                            <div class="tx_2"><img
                                    src="${pageContext.request.contextPath}/user/image_page/guanzhu/zy.jpg"/></div>
                            <h4 class="mz">张妍</h4>
                            <h5 class="li_3">合肥校区&nbsp;｜&nbsp;电商运营116班
                                <ul class="ul1">
                                    <li><span>创作</span>&nbsp;14</li>
                                    <li><span>粉丝</span>&nbsp;256</li>
                                </ul>
                            </h5>

                        </li>
                        <li class="li_2">
                            <div class="ljt1">
                                <div class="lj"></div>
                            </div>
                            <div class="tx_2"><img
                                    src="${pageContext.request.contextPath}/user/image_page/guanzhu/xjh.jpg"/></div>
                            <h4 class="mz">萧家恒</h4>
                            <h5 class="li_3">太原校区&nbsp;｜&nbsp;ui高级导师
                                <ul class="ul1">
                                    <li><span>创作</span>&nbsp;35</li>
                                    <li><span>粉丝</span>&nbsp;954</li>
                                </ul>
                            </h5>

                        </li>
                        <li class="li_2">
                            <div class="ljt1">
                                <div class="lj"></div>
                            </div>
                            <div class="tx_2"><img
                                    src="${pageContext.request.contextPath}/user/image_page/guanzhu/zzh.jpg"/></div>
                            <h4 class="mz">钟之涵</h4>
                            <h5 class="li_3">长沙校区&nbsp;｜&nbsp;ui高级导师
                                <ul class="ul1">
                                    <li><span>创作</span>&nbsp;14</li>
                                    <li><span>粉丝</span>&nbsp;358</li>
                                </ul>
                            </h5>

                        </li>
                    </ul>


                </div>
                <div class="tab_right">
                    <div class="tm_1">粉丝</div>
                    <hr/>
                    <ul class="ta_1">
                        <li class="fs">
                            <div class="tb txt">
                                <img src="${pageContext.request.contextPath}/user/image_page/shunianwo1.jpg"/>
                            </div>
                            <h3 class="zbs h3_1">书念我</h3>
                            <ul class="ul2_1">
                                <li class="uli">
                                    <h5>作品</h5>
                                    <h2>12</h2>
                                </li>
                                <li class="uli_1">
                                    <h5>粉丝</h5>
                                    <h2>1140</h2>
                                </li>
                                <h4></h4>
                            </ul>
                            <h5 class="jj1">广州｜ui设计研修班
                                <ul class="gz sx">
                                    <li class="li3">关注</li>
                                    <li class="li2">私信</li>
                                </ul>
                            </h5>
                        </li>
                        <li class="fs">
                            <div class="tb txt">
                                <img src="${pageContext.request.contextPath}/user/image_page/zoey1.jpg"/>
                            </div>
                            <h3 class="zbs h3_1">Zoey</h3>
                            <ul class="ul2_1">
                                <li class="uli">
                                    <h5>作品</h5>
                                    <h2>102</h2>
                                </li>
                                <li class="uli_1">
                                    <h5>粉丝</h5>
                                    <h2>1210</h2>
                                </li>
                                <h4></h4>
                            </ul>
                            <h5 class="jj1">北京｜ui设计511班
                                <ul class="gz sx">
                                    <li class="li3">关注</li>
                                    <li class="li2">私信</li>
                                </ul>
                            </h5>
                        </li>
                        <li class="fs li_1">
                            <div class="tb txt">
                                <img src="${pageContext.request.contextPath}/user/image_page/tx_11.jpg"/>
                            </div>
                            <h3 class="zbs h3_1">廖忠辉</h3>
                            <ul class="ul2_1">
                                <li class="uli">
                                    <h5>作品</h5>
                                    <h2>52</h2>
                                </li>
                                <li class="uli_1">
                                    <h5>粉丝</h5>
                                    <h2>1269</h2>
                                </li>
                                <h4></h4>
                            </ul>
                            <h5 class="jj1">太原｜室内设计952班
                                <ul class="gz sx">
                                    <li class="li3">关注</li>
                                    <li class="li2">私信</li>
                                </ul>
                            </h5>
                        </li>
                        <li class="fs">
                            <div class="tb txt">
                                <img src="${pageContext.request.contextPath}/user/image_page/chenxiaohuan1.jpg"/>
                            </div>
                            <h3 class="zbs h3_1">陈小欢</h3>
                            <ul class="ul2_1">
                                <li class="uli">
                                    <h5>作品</h5>
                                    <h2>53</h2>
                                </li>
                                <li class="uli_1">
                                    <h5>粉丝</h5>
                                    <h2>1109</h2>
                                </li>
                                <h4></h4>
                            </ul>
                            <h5 class="jj1">兰州｜UI设计362班
                                <ul class="gz sx">
                                    <li class="li3">关注</li>
                                    <li class="li2">私信</li>
                                </ul>
                            </h5>
                        </li>
                        <li class="fs">
                            <div class="tb txt">
                                <img src="${pageContext.request.contextPath}/user/image_page/jiayanwu1.jpg"/>
                            </div>
                            <h3 class="zbs h3_1">贾彦武</h3>
                            <ul class="ul2_1">
                                <li class="uli">
                                    <h5>作品</h5>
                                    <h2>12</h2>
                                </li>
                                <li class="uli_1">
                                    <h5>粉丝</h5>
                                    <h2>235</h2>
                                </li>
                                <h4></h4>
                            </ul>
                            <h5 class="jj1">西安｜ui设计952班
                                <ul class="gz sx">
                                    <li class="li3">关注</li>
                                    <li class="li2">私信</li>
                                </ul>
                            </h5>
                        </li>
                    </ul>
                </div>
                <div class="tab_right">
                    <div class="tm_1">消息</div>
                    <hr/>
                    <ul class="ta_1">
                        <li class="xinx">
                            <div class="ljt">
                                <div class="lj"></div>
                            </div>
                            <div class="tx3">
                                <div class="tx2"><img
                                        src="${pageContext.request.contextPath}/user/image_page/wzt_2.jpg"/></div>
                                <div class="xb">1</div>
                            </div>
                            <div class="wznr">
                                <div class="bt2"><strong>王小源老师</strong>&nbsp;&nbsp;
                                    <span>与你的私信</span>&nbsp;&nbsp;
                                    <a>08/29&nbsp;11:05</a></div>
                                <p>8月15日上传的作品【品牌海报设计】已完成批改，批改分为90分，星级评分4颗星，点评内容如下：明暗对比强烈、主体物突出</p>

                            </div>
                            <!-- <div class="jt1">
                                 <img src="${pageContext.request.contextPath}/user/image_page/right.png"/>
                             </div>-->

                        </li>
                        <li class="xinx">
                            <div class="ljt">
                                <div class="lj"></div>
                            </div>
                            <div class="tx3">
                                <div class="tx2"><img
                                        src="${pageContext.request.contextPath}/user/image_page/wzt_2.jpg"/></div>

                            </div>
                            <div class="wznr">
                                <div class="bt2"><strong>王小源老师</strong>&nbsp;&nbsp;
                                    <span>与你的私信</span>&nbsp;&nbsp;
                                    <a>08/10&nbsp;17:12</a></div>
                                <p>8月03日上传的作品【宠物APP项目策划方案】已完成批改，批改分为85分，星级评分3.5颗星，点评内容如下：主体物突出、色彩干净、点缀色合理</p>
                            </div>
                        </li>
                        <li class="xinx">
                            <div class="ljt">
                                <div class="lj"></div>
                            </div>
                            <div class="tx3">
                                <div class="tx2"><img src="${pageContext.request.contextPath}/user/image_page/tx2.jpg"/>
                                </div>

                            </div>
                            <div class="wznr">
                                <div class="bt2"><strong>智云管家</strong>&nbsp;&nbsp;
                                    <span>通知</span>&nbsp;&nbsp;
                                    <a>07/18&nbsp;08:05</a></div>
                                <p>
                                    本阶段成绩优秀，各方面都完成的很好，得到老师的赞扬，恭喜你完成平面软件阶段的学习，于本月25日将进入下一阶段【APP项目制作】的学习。但也有不足之处，望下阶段好好努力。</p>

                            </div>
                        </li>

                    </ul>
                </div>
                <div class="tab_right">
                    <div class="tm_1">收藏</div>
                    <hr/>
                    <ul class="ta_1">
                        <li class="xyzp scj">
                            <div class="ljt3">
                                <div class="lj"></div>
                            </div>
                            <div class="xyt">
                                <img src="${pageContext.request.contextPath}/user/image_page/guanzhu/sc_1.jpg"/>
                            </div>
                            <div class="xylb">
                                <div class="xyxq scz">
                                    <h2>提案式GUI</h2>
                                    <div class="xq">
                                        <ul class="dz">
                                            <li class="dz_1">
                                                <img src="${pageContext.request.contextPath}/user/image_page/yanjing.png"/>
                                                <p>1047</p>
                                            </li>
                                            <li class="dz_1">
                                                <img src="${pageContext.request.contextPath}/user/image_page/xinxi.png"/>
                                                <p>2</p>
                                            </li>
                                            <li class="dz_1">
                                                <img src="${pageContext.request.contextPath}/user/image_page/dianzan.png"/>
                                                <p>141</p>
                                            </li>
                                        </ul>
                                        <div class="sc1">原创</div>
                                    </div>
                                </div>
                                <div class="tx">
                                    <div class="tx1">
                                        <div class="tx_1">
                                            <img src="${pageContext.request.contextPath}/user/image_page/guanzhu/sc1_1.jpg"/>
                                        </div>
                                        <p class="li_1 sc_1">蒋晓玲</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </li>
                        <li class="xyzp scj">
                            <div class="ljt3">
                                <div class="lj"></div>
                            </div>
                            <div class="xyt">
                                <img src="${pageContext.request.contextPath}/user/image_page/guanzhu/sc_3.jpg"/>
                            </div>
                            <div class="xylb">
                                <div class="xyxq scz">
                                    <h2>引导页设计</h2>
                                    <div class="xq">
                                        <ul class="dz">
                                            <li class="dz_1">
                                                <img src="${pageContext.request.contextPath}/user/image_page/yanjing.png"/>
                                                <p>3104</p>
                                            </li>
                                            <li class="dz_1">
                                                <img src="${pageContext.request.contextPath}/user/image_page/xinxi.png"/>
                                                <p>10</p>
                                            </li>
                                            <li class="dz_1">
                                                <img src="${pageContext.request.contextPath}/user/image_page/dianzan.png"/>
                                                <p>20</p>
                                            </li>
                                        </ul>
                                        <div class="sc1">原创</div>
                                    </div>
                                </div>
                                <div class="tx">
                                    <div class="tx1">
                                        <div class="tx_1">
                                            <img src="${pageContext.request.contextPath}/user/image_page/guanzhu/sc1_3.jpg"/>
                                        </div>
                                        <p class="li_1 sc_1">曹豪</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </li>
                        <li class="xyzp scj_1 li_1">
                            <div class="ljt3">
                                <div class="lj"></div>
                            </div>
                            <div class="xyt">
                                <img src="${pageContext.request.contextPath}/user/image_page/guanzhu/sc_2.jpg"/>
                            </div>
                            <div class="xylb">
                                <div class="xyxq scz">
                                    <h2>主题图标</h2>
                                    <div class="xq">
                                        <ul class="dz">
                                            <li class="dz_1">
                                                <img src="${pageContext.request.contextPath}/user/image_page/yanjing.png"/>
                                                <p>1019</p>
                                            </li>
                                            <li class="dz_1">
                                                <img src="${pageContext.request.contextPath}/user/image_page/xinxi.png"/>
                                                <p>0</p>
                                            </li>
                                            <li class="dz_1">
                                                <img src="${pageContext.request.contextPath}/user/image_page/dianzan.png"/>
                                                <p>12</p>
                                            </li>
                                        </ul>
                                        <div class="sc1">原创</div>
                                    </div>
                                </div>
                                <div class="tx">
                                    <div class="tx1">
                                        <div class="tx_1">
                                            <img src="${pageContext.request.contextPath}/user/image_page/guanzhu/sc1_2.jpg"/>
                                        </div>
                                        <p class="li_1 sc_1">周明君</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </li>
                        <li class="xyzp scj">
                            <div class="ljt3">
                                <div class="lj"></div>
                            </div>
                            <div class="xyt">
                                <img src="${pageContext.request.contextPath}/user/image_page/guanzhu/sc_4.jpg"/>
                            </div>
                            <div class="xylb">
                                <div class="xyxq scz">
                                    <h2>画册设计</h2>
                                    <div class="xq">
                                        <ul class="dz">
                                            <li class="dz_1">
                                                <img src="${pageContext.request.contextPath}/user/image_page/yanjing.png"/>
                                                <p>2350</p>
                                            </li>
                                            <li class="dz_1">
                                                <img src="${pageContext.request.contextPath}/user/image_page/xinxi.png"/>
                                                <p>4</p>
                                            </li>
                                            <li class="dz_1">
                                                <img src="${pageContext.request.contextPath}/user/image_page/dianzan.png"/>
                                                <p>45</p>
                                            </li>
                                        </ul>
                                        <div class="sc1">原创</div>
                                    </div>
                                </div>
                                <div class="tx">
                                    <div class="tx1">
                                        <div class="tx_1">
                                            <img src="${pageContext.request.contextPath}/user/image_page/guanzhu/sc1_4.jpg"/>
                                        </div>
                                        <p class="li_1 sc_1">林宏达</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
<script src="${pageContext.request.contextPath}/user/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/user/assets/index.js"></script>
<!--bottom-->
<jsp:include page="bottom.jsp"></jsp:include>
<link rel="stylesheet"
      href="${pageContext.request.contextPath}/admin/plugins/bootstrap/css/bootstrap.min.css">
<script src="${pageContext.request.contextPath}/admin/js/bootstrap-paginator.js"></script>
<script src="${pageContext.request.contextPath}/user/js/jqthumb.min.js"></script>
<script type="text/javascript">
    function imgNotChange() {
        // 图片不变形配置
        $('.img_a').jqthumb({
            classname: 'jqthumb',
            width: 210,
            height: 227,
            showoncomplete: true
        });
    }
    function changePageSize() {
        //获取下拉框的值
        var pageSize = $("#changePageSize").val();
        //向服务器发送请求，改变每页显示条数
        location.href = "${pageContext.request.contextPath}/user/homework/findNotCommentHomeworkByTeacher?teacherId=${teacher.id}&pageNum=1&pageSize=" + pageSize;
    }
    // 配置分页插件
    $('#pageLimit').bootstrapPaginator({
        currentPage:${homeworks.number+1},//当前页。
        totalPages: ${homeworks.totalPages},//总页数。
        bootstrapMajorVersion: 3,//当前版本
        numberOfPages: 10,//显示的页数
        tooltipTitles: function () {
        },// 鼠标移入不显示样式
        itemTexts: function (type, page, current) {//自定义按钮文本。
            switch (type) {
                case "first":
                    return "首页";
                case "prev":
                    return "上一页";
                case "next":
                    return "下一页";
                case "last":
                    return "末页";
                case "page":
                    return page;
            }
        },
        onPageClicked: function (event, originalEvent, type, page) {//给按钮绑定事件，page为按钮上的数字。
            scroll(0, 0);
            $.get("${pageContext.request.contextPath}/user/homework/findNotCommentHomeworkByTeacherYiBu", {
                    "teacherId":${teacher.id},
                    "pageNum": page,
                    "pageSize":${homeworks.size}
                },
                function (data) {
                    $(".yp").html("")
                    var data2 = eval(data);
                    console.log(data2);
                    data2 = data2.content;
                    for (var i in data2) {
                        $(".yp").append(" <li class=\"bounceInRight ypzy\" data-wow-delay=\"0.2s\">\n" +
                            "                <div class=\"ypl\">\n" +
                            "                <img src=" + data2[i].coverImg + " class=\"img_a\"/>\n" +
                            "                </div>\n" +
                            "                <div class=\"ypr \">\n" +
                            "                <div class=\"ypry\">\n" +
                            "                <div class=\"ypryl\"><a target='_blank' href=" + "'${pageContext.request.contextPath}/user/homework/findById?teacherId=${teacher.id}&homeworkId=" + data2[i].id + "'>" + data2[i].typeStr + ":" + data2[i].workName + "</a></div>\n" +
                            "            <div class=\"ypryr ypryr_a\"><a target='_blank' href=" + "'${pageContext.request.contextPath}/user/homework/findById?teacherId=${teacher.id}&homeworkId=" + data2[i].id + "'>立即批改></a>\n" +
                            "                </div>\n" +
                            "                </div>\n" +
                            "                <div class=\"ypre ypre_a\">\n" +
                            "                <span>提交人：" + data2[i].students[0].name + "</span>\n" +
                            "            <span>提交时间：" + data2[i].timeStr + "</span>\n" +
                            "            </div>\n" +
                            "\n" +
                            "            <div class=\"ypri\">\n" +
                            "\n" +
                            "                <div class=\"yprir\" id=\"dududu\">\n" +
                            "                <div class=\"ypril_a_a\">" + data2[i].tags + "</div>\n" +
                            "                <input type=\"hidden\" value='" + data2[i].content + "'id=\"content\">\n" +
                            "                <div class=\"ypril_a_a\">" + data2[i].typeStr + "</div>\n" +
                            "\n" +
                            "                </div>\n" +
                            "                </div>\n" +
                            "                <div class=\"yprw\">\n" +
                            "                <div class=\"yprwl\">作业介绍：</div>\n" +
                            "            <div class=\"yprwr_a\">\n" +
                            "                " + data2[i].instruction + "\n" +
                            "                </div>\n" +
                            "                </div>\n" +
                            "                </div>\n" +
                            "                </li>")


                    };
                    x();
                }).error(function (data) {
                console.log(data)
                $(".yp").html("")
                var data2 = eval(data);
                // data2 = data2.responseText;
                for (var i in data2) {
                    $(".yp").append(" <li class=\"wow bounceInRight ypzy\" data-wow-delay=\"1.2s\">\n" +
                        "                <div class=\"ypl\">\n" +
                        "                <img src=" + data2[i].coverImg + " class=\"img_a\"/>\n" +
                        "                </div>\n" +
                        "                <div class=\"ypr \">\n" +
                        "                <div class=\"ypry\">\n" +
                        "                <div class=\"ypryl\"><a>" + data2[i].typeStr + ":" + data2[i].workName + "</a></div>\n" +
                        "            <div class=\"ypryr ypryr_a\"><a href=" + "'${pageContext.request.contextPath}/user/homework/findById?teacherId=${teacher.id}&homeworkId=" + data2[i].id + "'>立即批改></a>\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                <div class=\"ypre ypre_a\">\n" +
                        "                <span>提交人：${teacher.name}</span>\n" +
                        "            <span>提交时间：" + data2[i].timeStr + "</span>\n" +
                        "            </div>\n" +
                        "\n" +
                        "            <div class=\"ypri\">\n" +
                        "\n" +
                        "                <div class=\"yprir\" id=\"dududu\">\n" +
                        "                <div class=\"ypril_a_a\">" + data2[i].tags + "</div>\n" +
                        "                <input type=\"hidden\" value='" + data2[i].content + "'id=\"content\">\n" +
                        "                <div class=\"ypril_a_a\">" + data2[i].typeStr + "</div>\n" +
                        "\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                <div class=\"yprw\">\n" +
                        "                <div class=\"yprwl\">作业介绍：</div>\n" +
                        "            <div class=\"yprwr_a\">\n" +
                        "                " + data2[i].instruction + "\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                </li>")
                }
                x();
            })
        }
    });
    $("li").css("cursor", "pointer");
    function x() {
        var yourString = $("#content").val();
        var result = yourString.split(",");
        // var a  = $("#dududu").html();
        for (var i = 0; i < result.length; i++) {
            $("#dududu").append("<div class=\"ypril_a_a\" >" + result[i] + "</div>")
        }
        imgNotChange();
    }
    $(function () {
        var yourString = $("#content").val();
        var result = yourString.split(",");
        // var a  = $("#dududu").html();
        for (var i = 0; i < result.length; i++) {
            $("#dududu").append("<div class=\"ypril_a_a\" >" + result[i] + "</div>")
        }
        imgNotChange();
    })
    function chooesClass(page, size) {
        $.get("${pageContext.request.contextPath}/user/homework/findNotCommentHomeworkByTeacherYiBu",
            {
                "pageNum": page,
                "pageSize": size,
                "classId": $("#chooesClasses").val()
            }, function (data2) {
                $(".yp").html("")
                console.log(data2);
                for (var i = 0; i < data2.content.length; i++) {
                    $(".yp").append(" <li class=\"bounceInRight ypzy\" data-wow-delay=\"0.2s\">\n" +
                        "                <div class=\"ypl\">\n" +
                        "                <img src=" + data2.content[i].coverImg + " class=\"img_a\"/>\n" +
                        "                </div>\n" +
                        "                <div class=\"ypr \">\n" +
                        "                <div class=\"ypry\">\n" +
                        "                <div class=\"ypryl\"><a>" + data2.content[i].typeStr + ":" + data2.content[i].workName + "</a></div>\n" +
                        "            <div class=\"ypryr ypryr_a\"><a href=" + "'${pageContext.request.contextPath}/user/homework/findById?teacherId=${teacher.id}&homeworkId=" + data2.content[i].id + "'>立即批改></a>\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                <div class=\"ypre ypre_a\">\n" +
                        "                <span>提交人：" + data2.content[i].students[0].name + "</span>\n" +
                        "            <span>提交时间：" + data2.content[i].timeStr + "</span>\n" +
                        "            </div>\n" +
                        "\n" +
                        "            <div class=\"ypri\">\n" +
                        "\n" +
                        "                <div class=\"yprir\" id=\"dududu\">\n" +
                        "                <div class=\"ypril_a_a\">" + data2.content[i].tags + "</div>\n" +
                        "                <input type=\"hidden\" value='" + data2.content[i].content + "'id=\"content\">\n" +
                        "                <div class=\"ypril_a_a\">" + data2.content[i].typeStr + "</div>\n" +
                        "\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                <div class=\"yprw\">\n" +
                        "                <div class=\"yprwl\">作业介绍：</div>\n" +
                        "            <div class=\"yprwr_a\">\n" +
                        "                " + data2.content[i].instruction + "\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                </div>\n" +
                        "                </li>")

                }
                ;
                $("#fenye").html("总共" + data2.totalPages + "页，共" + data2.totalElements + "条数据")
                // 配置分页插件
                $("#pageLimit").empty();
                $('#pageLimit').bootstrapPaginator({
                    currentPage: data2.number + 1,//当前页。
                    totalPages: data2.totalPages,//总页数。
                    bootstrapMajorVersion: 3,//当前版本
                    numberOfPages: 10,//显示的页数
                    tooltipTitles: function () {
                    },// 鼠标移入不显示样式
                    itemTexts: function (type, page, current) {//自定义按钮文本。
                        switch (type) {
                            case "first":
                                return "首页";
                            case "prev":
                                return "上一页";
                            case "next":
                                return "下一页";
                            case "last":
                                return "末页";
                            case "page":
                                return page;
                        }
                    },
                    onPageClicked: function (event, originalEvent, type, page) {//给按钮绑定事件，page为按钮上的数字。
                        scroll(0, 0);
                        $.get("${pageContext.request.contextPath}/user/homework/findNotCommentHomeworkByTeacherYiBu", {
                                "pageNum": page,
                                "classId": $("#chooesClasses").val()
                            },
                            function (data) {
                                $(".yp").html("")
                                var data2 = eval(data);
                                console.log(data2);
                                data2 = data2.content;
                                for (var i in data2) {
                                    $(".yp").append(" <li class=\"bounceInRight ypzy\" data-wow-delay=\"0.2s\">\n" +
                                        "                <div class=\"ypl\">\n" +
                                        "                <img src=" + data2[i].coverImg + " class=\"img_a\"/>\n" +
                                        "                </div>\n" +
                                        "                <div class=\"ypr \">\n" +
                                        "                <div class=\"ypry\">\n" +
                                        "                <div class=\"ypryl\"><a>" + data2[i].typeStr + ":" + data2[i].workName + "</a></div>\n" +
                                        "            <div class=\"ypryr ypryr_a\"><a href=" + "'${pageContext.request.contextPath}/user/homework/findById?teacherId=${teacher.id}&homeworkId=" + data2[i].id + "'>立即批改></a>\n" +
                                        "                </div>\n" +
                                        "                </div>\n" +
                                        "                <div class=\"ypre ypre_a\">\n" +
                                        "                <span>提交人：" + data2[i].students[0].name + "</span>\n" +
                                        "            <span>提交时间：" + data2[i].timeStr + "</span>\n" +
                                        "            </div>\n" +
                                        "\n" +
                                        "            <div class=\"ypri\">\n" +
                                        "\n" +
                                        "                <div class=\"yprir\" id=\"dududu\">\n" +
                                        "                <div class=\"ypril_a_a\">" + data2[i].tags + "</div>\n" +
                                        "                <input type=\"hidden\" value='" + data2[i].content + "'id=\"content\">\n" +
                                        "                <div class=\"ypril_a_a\">" + data2[i].typeStr + "</div>\n" +
                                        "\n" +
                                        "                </div>\n" +
                                        "                </div>\n" +
                                        "                <div class=\"yprw\">\n" +
                                        "                <div class=\"yprwl\">作业介绍：</div>\n" +
                                        "            <div class=\"yprwr_a\">\n" +
                                        "                " + data2[i].instruction + "\n" +
                                        "                </div>\n" +
                                        "                </div>\n" +
                                        "                </div>\n" +
                                        "                </li>")
                                }
                                ;
                                x();
                            }).error(function (data) {
                            $(".yp").html("")
                            var data2 = eval(data);
                            for (var i in data2) {
                                $(".yp").append(" <li class=\"wow bounceInRight ypzy\" data-wow-delay=\"1.2s\">\n" +
                                    "                <div class=\"ypl\">\n" +
                                    "                <img src=" + data2[i].coverImg + " class=\"img_a\"/>\n" +
                                    "                </div>\n" +
                                    "                <div class=\"ypr \">\n" +
                                    "                <div class=\"ypry\">\n" +
                                    "                <div class=\"ypryl\"><a>" + data2[i].typeStr + ":" + data2[i].workName + "</a></div>\n" +
                                    "            <div class=\"ypryr ypryr_a\"><a href=" + "'${pageContext.request.contextPath}/user/homework/findById?teacherId=${teacher.id}&homeworkId=" + data2[i].id + "'>立即批改></a>\n" +
                                    "                </div>\n" +
                                    "                </div>\n" +
                                    "                <div class=\"ypre ypre_a\">\n" +
                                    "                <span>提交人：${teacher.name}</span>\n" +
                                    "            <span>提交时间：" + data2[i].timeStr + "</span>\n" +
                                    "            </div>\n" +
                                    "\n" +
                                    "            <div class=\"ypri\">\n" +
                                    "\n" +
                                    "                <div class=\"yprir\" id=\"dududu\">\n" +
                                    "                <div class=\"ypril_a_a\">" + data2[i].tags + "</div>\n" +
                                    "                <input type=\"hidden\" value='" + data2[i].content + "'id=\"content\">\n" +
                                    "                <div class=\"ypril_a_a\">" + data2[i].typeStr + "</div>\n" +
                                    "\n" +
                                    "                </div>\n" +
                                    "                </div>\n" +
                                    "                <div class=\"yprw\">\n" +
                                    "                <div class=\"yprwl\">作业介绍：</div>\n" +
                                    "            <div class=\"yprwr_a\">\n" +
                                    "                " + data2[i].instruction + "\n" +
                                    "                </div>\n" +
                                    "                </div>\n" +
                                    "                </div>\n" +
                                    "                </li>")
                            }
                            x();
                        })
                    }
                });

                x();
            }
        ).error(function (data) {
            $(".yp").html("")
            var data2 = eval(data);
            for (var i = 0; i < data2.content.length; i++) {
                $(".yp").append(" <li class=\"wow bounceInRight ypzy\" data-wow-delay=\"1.2s\">\n" +
                    "                <div class=\"ypl\">\n" +
                    "                <img src=" + data2.content[i].coverImg + " class=\"img_a\"/>\n" +
                    "                </div>\n" +
                    "                <div class=\"ypr \">\n" +
                    "                <div class=\"ypry\">\n" +
                    "                <div class=\"ypryl\"><a>" + data2.content[i].typeStr + ":" + data2.content[i].workName + "</a></div>\n" +
                    "            <div class=\"ypryr ypryr_a\"><a href=" + "'${pageContext.request.contextPath}/user/homework/findById?teacherId=${teacher.id}&homeworkId=" + data2.content[i].id + "'>立即批改></a>\n" +
                    "                </div>\n" +
                    "                </div>\n" +
                    "                <div class=\"ypre ypre_a\">\n" +
                    "                <span>提交人：${teacher.name}</span>\n" +
                    "            <span>提交时间：" + data2.content[i].timeStr + "</span>\n" +
                    "            </div>\n" +
                    "\n" +
                    "            <div class=\"ypri\">\n" +
                    "\n" +
                    "                <div class=\"yprir\" id=\"dududu\">\n" +
                    "                <div class=\"ypril_a_a\">" + data2[i].content[i].tags + "</div>\n" +
                    "                <input type=\"hidden\" value='" + data2.content[i].content + "'id=\"content\">\n" +
                    "                <div class=\"ypril_a_a\">" + data2.content[i].typeStr + "</div>\n" +
                    "\n" +
                    "                </div>\n" +
                    "                </div>\n" +
                    "                <div class=\"yprw\">\n" +
                    "                <div class=\"yprwl\">作业介绍：</div>\n" +
                    "            <div class=\"yprwr_a\">\n" +
                    "                " + data2.content[i].instruction + "\n" +
                    "                </div>\n" +
                    "                </div>\n" +
                    "                </div>\n" +
                    "                </li>")
            }
            x();
        })
    }

</script>

</body>
</html>

