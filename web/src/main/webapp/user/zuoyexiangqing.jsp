<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>作业详情</title>
    <!--动画效果-->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/animate.css" type="text/css">
    <!--动画效果-->
    <!--滚动播放插件-->
    <link type="text/css" href="${pageContext.request.contextPath}/user/css/style_lb.css" rel="stylesheet"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/tools.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/js.js"></script>
    <!--滚动播放插件--
    <!--plcj-->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/bootstrap.css">
    <style>
        /*.zyzsl_c {*/
        /*width: 100%;*/
        /*!* height: 100%; *!*/
        /*}*/
        .container {
            width: 1200px;
            overflow: hidden;

        }

        .commentbox {
            width: 1100px;
            margin: 20px auto;
        }

        .mytextarea {
            width: 100%;
            overflow: auto;
            word-break: break-all;
            height: 100px;
            color: #000;
            font-size: 1em;
            resize: none;
        }

        .comment-list {
            width: 1100px;
            margin: 0px auto 20px;
            clear: both;
        }

        .comment-list .comment-info {
            position: relative;
            padding: 20px;
            border-bottom: 1px solid #ccc;
        }

        .comment-list .comment-info header {
            width: 10%;
            position: absolute;
        }

        .comment-list .comment-info header img {
            width: 60%;
            border-radius: 50%;
            padding: 5px;
        }

        .comment-list .comment-info .comment-right {
            padding: 5px 0px 5px 11%;
        }

        .comment-list .comment-info .comment-right h3 {
            margin: 5px 0px;
        }

        .comment-list .comment-info .comment-right .comment-content-header {
            height: 25px;
        }

        .comment-list .comment-info .comment-right .comment-content-header span, .comment-list .comment-info .comment-right .comment-content-footer span {
            padding-right: 2em;
            color: #aaa;
        }

        .comment-list .comment-info .comment-right .comment-content-header span, .comment-list .comment-info .comment-right .comment-content-footer span.reply-btn, .send, .reply-list-btn {
            cursor: pointer;
        }

        .comment-list .comment-info .comment-right .reply-list {
            border-left: 3px solid #ccc;
            padding-left: 7px;
        }

        .comment-list .comment-info .comment-right .reply-list .reply {
            border-bottom: 1px dashed #ccc;
        }

        .comment-list .comment-info .comment-right .reply-list .reply div span {
            padding-left: 10px;
        }

        .comment-list .comment-info .comment-right .reply-list .reply p span {
            padding-right: 2em;
            color: #aaa;
        }

        element.style {
            visibility: visible;
            animation-delay: 0.8s;
            animation-name: bounceInDown;
            margin-left: 860px;
            margin-top: -85px;
        }

        .xqt {
            width: 100%;
            height: 80px;
            line-height: 80px;
            font-size: 14px;
            border-bottom: 1px solid #eee;
        }
    </style>
    <!--plcj-->
    <!--样式表开始-->
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/user/css/demo.css">
    <!--样式表结束-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/jquery-1.8.2.min.js"></script>
    <!--gzcj-->
    <style>
        ul,
        li {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .tgl {
            display: none;
        }

        .tgl-btn {
            width: 74px !important;
            height: 30px !important;
            line-height: 30px;
            text-align: center;
        }

        .tgl + .tgl-btn {
            outline: 0;
            display: block;
            width: 100%;
            height: 100%;
            position: relative;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .tgl-flip + .tgl-btn {
            padding: 10px;
            -webkit-transition: all .3s ease;
            transition: all .3s ease;
            font-family: sans-serif;
            -webkit-perspective: 100px;
            perspective: 100px;
        }

        .tgl-flip + .tgl-btn:after, .tgl-flip + .tgl-btn:before {
            display: inline-block;
            -webkit-transition: all .3s ease;
            transition: all .3s ease;
            width: 100%;
            text-align: center;
            line-height: 2em;
            font-weight: bold;
            color: #fff;
            position: absolute;
            top: 0;
            left: 0;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            border-radius: 4px;
        }

        .tgl-flip + .tgl-btn:after {
            content: attr(data-tg-on);
            background: #666;
            -webkit-transform: rotateY(-180deg);
            transform: rotateY(-180deg);
            width: 74px;
            height: 30px;
            line-height: 30px;
        }

        .tgl-flip + .tgl-btn:before {
            background: #ff3457;
            content: attr(data-tg-off);
            width: 74px;
            height: 30px;
            line-height: 30px;
        }

        .tgl-flip:checked + .tgl-btn:before {
            -webkit-transform: rotateY(180deg);
            transform: rotateY(180deg);
            width: 74px;
            height: 30px;
            line-height: 30px;
        }

        .tgl-flip:checked + .tgl-btn:after {
            -webkit-transform: rotateY(0);
            transform: rotateY(0);
            left: 0;
            background: #666666;
            width: 74px;
            height: 30px;
            line-height: 30px;

        }

        .tgl + .tgl-btn1 {
            outline: 0;
            display: block;
            width: 100%;
            height: 100%;
            position: relative;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .tgl-btn1 {
            width: 74px !important;
            height: 30px !important;
            line-height: 30px;
            text-align: center;
        }

        .tgl-flip + .tgl-btn1 {
            padding: 10px;
            -webkit-transition: all .3s ease;
            transition: all .3s ease;
            font-family: sans-serif;
            -webkit-perspective: 100px;
            perspective: 100px;
        }

        .tgl-flip + .tgl-btn1:after, .tgl-flip + .tgl-btn1:before {
            display: inline-block;
            -webkit-transition: all .3s ease;
            transition: all .3s ease;
            width: 100%;
            text-align: center;
            line-height: 2em;
            font-weight: bold;
            color: #fff;
            position: absolute;
            top: 0;
            left: 0;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            border-radius: 4px;
        }

        .tgl-flip + .tgl-btn1:after {
            content: attr(data-tg-off);
            background: #ff3457;
            -webkit-transform: rotateY(-180deg);
            transform: rotateY(-180deg);
            width: 74px;
            height: 30px;
            line-height: 30px;
        }

        .tgl-flip + .tgl-btn1:before {
            background: #666666;
            content: attr(data-tg-on);
            width: 74px;
            height: 30px;
            line-height: 30px;
        }

        .tgl-flip:checked + .tgl-btn1:before {
            -webkit-transform: rotateY(180deg);
            transform: rotateY(180deg);
            width: 74px;
            height: 30px;
            line-height: 30px;
        }

        .tgl-flip:checked + .tgl-btn1:after {
            -webkit-transform: rotateY(0);
            transform: rotateY(0);
            left: 0;
            background: #ff3457;
            width: 74px;
            height: 30px;
            line-height: 30px;

        }
    </style>
    <!--gzcj-->
    <link href="${pageContext.request.contextPath}/user/css/style-a.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<!--动画效果-->
<script src="${pageContext.request.contextPath}/user/js/.min.js"></script>
<script>
    if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))) {
        new ().init();
    }
    ;
</script>
<!--动画效果-->
<!--tou-->
<jsp:include page="top.jsp"></jsp:include>
<!--zyxq-->
<div class="xqq">
    <div class="xqy">
        <div class=" bounceInDown xqt">
            <a href="${pageContext.request.contextPath}/user/index.jsp" class="xqth_a">首页</a> /
            <a href="${pageContext.request.contextPath}/user/zuoye.jsp" class="xqth_a">作业</a> /
            <a href="${pageContext.request.contextPath}/user/zuoyexiangqing.jsp"
               class="xqth">${studentmessage.homework.workName}</a>
        </div>
        <div class="xqb">
            <div class=" bounceInDown xqbl" data--delay="0.4s">
                <div class="xqbll">
                    <div class=" bounce xqbll_a" data--delay="0.4s">${studentmessage.homework.workName}
                        <div class="xqbll_b">${studentmessage.homework.commentTimeStr}/${studentmessage.homework.cherkedStr}</div>
                        <div class="xqbll_c">${studentmessage.homework.tags}
                            / ${studentmessage.homework.content}  </div>
                    </div>
                    <div class="xqblr">
                        <div class="zycb_la">${studentmessage.homework.clicks}</div>
                        <%--<div class="zycb_lb">${studentmessage.homework.commentses.size()}</div>--%>
                        <div class="zycb_lc">${studentmessage.homework.thumbsUp}</div>
                    </div>
                </div>
                <div class=" bounceInDown xqbr" data--delay="0.8s" style="float: right">
                    <div class=" bounce xqbrl" data--delay="0.8s">
                        <img src="${studentmessage.student.header}"/>
                    </div>
                    <div class="xqbrr">
                        <div class="xqbrr_a">${studentmessage.student.name}</div>
                        <div class="xqbrr_b">${studentmessage.student.classes.school.schoolName}/${studentmessage.student.classes.className}</div>
                        <div class="xqbrr_c">
                            <c:if test="${sessionScope.student != null}">
                                <c:if test="${studentmessage.student.id != sessionScope.student.id}">
                                    <div class="xqbrr_cl">
                                        <!--gzcj-->
                                        <ul class="tg-list">
                                            <li class="tg-list-item">
                                                <c:if test="${concern == true}">
                                                    <input class="tgl tgl-flip" id="cb5" type="checkbox" value="1">
                                                    <label class="tgl-btn1" data-tg-off="关注" data-tg-on="已关注" for="cb5"
                                                           style="font-size:12px" onclick=guanzhu()></label>
                                                </c:if>
                                                <c:if test="${concern != true}">
                                                    <input class="tgl tgl-flip" id="cb5" type="checkbox" value="0">
                                                    <label class="tgl-btn" data-tg-on="已关注" data-tg-off="关注" for="cb5"
                                                           style="font-size:12px" onclick=guanzhu()></label>
                                                </c:if>
                                            </li>
                                        </ul>
                                        <!--gzcj-->
                                    </div>
                                </c:if>
                            </c:if>
                            <c:if test="${sessionScope.student == null}">
                                <input class="tgl tgl-flip" id="cb5" type="checkbox" value="0">
                                <label class="tgl-btn" data-tg-on="已关注" data-tg-off="关注" for="cb5"
                                       style="font-size:12px" onclick=guanzhu()></label>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--zyxq-->
    <!--zyzs-->
    <div class="zyzsq">
        <div class="zyzsy">
            <div class=" bounceInUp zyzsl" data--delay="1s">
                <div class="zyzsl_t">${studentmessage.homework.workName}
                    <div class="zyzsl_t_wz">${studentmessage.homework.instruction}</div>
                </div>
                <div class="zyzsl_b">
                    <!--chajian-->
                    <!--动态点赞开始-->
                    <div class="praise">
                        <span id="praise"><img src="${pageContext.request.contextPath}/user/image/zan.png"
                                               id="praise-img"/></span>
                        <span id="praise-txt">${studentmessage.homework.thumbsUp}</span>
                        <span id="add-num"><em>+1</em></span>
                    </div>
                    <!--动态点赞结束-->
                    <script>
                        /* @author:Romey
                         * 动态点赞
                         * 此效果包含css3，部分浏览器不兼容（如：IE10以下的版本）
                        */
                        $(function () {
                            $("#praise").click(function () {
                                var praise_img = $("#praise-img");
                                var text_box = $("#add-num");
                                var praise_txt = $("#praise-txt");
                                var num = parseInt(praise_txt.text());
                                if (praise_img.attr("src") == ("${pageContext.request.contextPath}/user/image/yizan.png")) {
                                    $(this).html("<img src='${pageContext.request.contextPath}/user/image/zan.png' id='praise-img' class='animation' />");
                                    praise_txt.removeClass("hover");
                                    text_box.show().html("<em class='add-animation'>-1</em>");
                                    $(".add-animation").removeClass("hover");
                                    num -= 1;
                                    praise_txt.text(num)
                                    $.post("/user/jobdetails/thumbsUp", {
                                        "homeworkId":${studentmessage.homework.id},
                                        "num": -1
                                    }, function () {
                                        $.post("/user/jobdetails/findThumbsUp", {"homeworkId":${studentmessage.homework.id}}, function (data) {
                                            $(".zycb_lc").text(data);
                                        });
                                    });

                                } else {
                                    $(this).html("<img src='${pageContext.request.contextPath}/user/image/yizan.png' id='praise-img' class='animation' />");
                                    praise_txt.addClass("hover");
                                    text_box.show().html("<em class='add-animation'>+1</em>");
                                    $(".add-animation").addClass("hover");
                                    num += 1;
                                    praise_txt.text(num)
                                    $.post("/user/jobdetails/thumbsUp", {
                                        "homeworkId":${studentmessage.homework.id},
                                        "num": +1
                                    }, function () {
                                        $.post("/user/jobdetails/findThumbsUp", {"homeworkId":${studentmessage.homework.id}}, function (data) {
                                            $(".zycb_lc").text(data);
                                        });
                                    });
                                }
                            });
                        })
                    </script>
                    <!--chajian-->
                </div>
            </div>
            <div class="zyzsr">
                <div class=" bounceInRight zyzs_xx" data--delay="1.2s">
                    <div class="zyzs_xx_t">作业信息</div>
                    <div class="zyzs_xx_c">
                        <div class="zyzs_xx_ct">标签：<span>${studentmessage.homework.tags}</span></div>
                        <div class="zyzs_xx_ct">内容：<span>${studentmessage.homework.content}</span></div>
                        <div class="zyzs_xx_ct zyzs_xx_ct1">类型：<span>${studentmessage.homework.typeStr}</span></div>
                    </div>
                    <div class="zyzs_xx_b">作业上传时间：${studentmessage.homework.timeStr}</div>
                </div>
                <div class=" bounceInRight zyzs_pg" data--delay="1.4s">
                    <div class="zyzs_pg_t">批改信息</div>
                    <div class="zyzs_pg_c">
                        <div class="zyzs_pg_cr">作业批改人</div>
                        <div class="zyzs_pg_cm">
                            <div class="zyzs_pg_cml">
                                <img src="${studentmessage.homework.teacher.picture}"/>
                            </div>
                            <div class="zyzs_pg_cmr">
                                <div class="zyzs_pg_cmr_t">${studentmessage.homework.teacher.name}</div>
                                <div class="zyzs_pg_cmr_b">${studentmessage.student.classes.school.schoolName}/${studentmessage.student.classes.className}</div>
                            </div>
                        </div>
                        <div class="zyzs_pg_cf">云值：<span>${studentmessage.homework.score}</span>分</div>
                        <div class="zyzs_pg_cx">星级点评：<c:forEach begin="1" end="${studentmessage.homework.star}"><img
                                src="${pageContext.request.contextPath}/user/image/ypzy_xx.jpg" no-repeat 80px
                                0px> </c:forEach></div>
                        <div class="zyzs_pg_cb">标签点评：</div>
                        <div class="zyzs_pg_cbq">
                            <c:forEach items="${studentmessage.biaoqianpingjia}" var="bqpj">
                                <div class="zyzs_pg_cbq_1">${bqpj}</div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="zyzs_pg_b">
                        <div class="zyzs_pg_bd">评语点评：</div>
                        <div class="zyzs_pg_bc">${studentmessage.homework.descEvaluate}</div>
                        <%--<div class="zyzs_pg_bt">--%>
                        <%--<c:if test="${studentmessage.size==0}">--%>
                        <%--<div class="zyzs_pg_btl" onclick="x1()" style="display: none" >上一份作业</div>--%>
                        <%--</c:if>--%>
                        <%--<c:if test="${studentmessage.size!=0||studentmessage.size==null}">--%>
                        <%--<div class="zyzs_pg_btl" onclick="x1()"  >上一份作业</div>--%>
                        <%--</c:if>--%>
                        <%--<c:if test="${studentmessage.size==1}">--%>
                        <%--<div class="zyzs_pg_btr" onclick="x2()" style="display: none" >下一份作业</div>--%>
                        <%--</c:if>--%>
                        <%--<c:if test="${studentmessage.size!=1||studentmessage.size==null}">--%>
                        <%--<div class="zyzs_pg_btr" onclick="x2()" >下一份作业</div>--%>
                        <%--</c:if>--%>
                        <%--</div>--%>
                    </div>
                </div>
                <!--zyzs_tj-->
                <div class=" bounceInRight zyzs_tj">
                    <div class="zyzs_tjt">
                        <div class="zyzs_tjt_l">其他作业</div>
                        <%--<div class="zyzs_tjt_r" id=""><a onclick="yibu()"><img src="${pageContext.request.contextPath}/user/image/sx.png" width="18px" height="14px"/>换一批</a></div>--%>
                    </div>
                    <ul class="zyzs_tjb" id="tihuan">
                        <c:forEach begin="0" end="2" items="${otherTasks}" var="otherTask">
                            <a href="${pageContext.request.contextPath}/user/jobdetails/message?id=${otherTask.id}">
                                <li>
                                    <div class="zyzs_tjb_l">
                                        <img src="${otherTask.coverImg}" class="tuijian"/>
                                    </div>
                                    <div class="zyzs_tjb_r">
                                        <div class="zyzs_tjb_rt">${otherTask.workName}</div>
                                        <c:forEach items="${otherTask.students}" var="student">
                                            <c:if test="${student.name.equals(studentmessage.student.name) }">
                                                <div class="zyzs_tjb_rc">${student.classes.className}</div>
                                                <div class="zyzs_tjb_rb">
                                                    <div class="zyzs_tjb_rb_l">
                                                        <img src="${student.header}"/>
                                                    </div>
                                                    <div class="zyzs_tjb_rb_r">${student.name}</div>
                                                </div>
                                            </c:if>
                                        </c:forEach>
                                    </div>
                                </li>
                            </a>
                        </c:forEach>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>

    <%--作业评论--%>
    <c:if test="${student.id==null && teacher.id==null}">
    <div class=" bounceInRight zyplq">
        <div class="zyply">
            <div id="zypl">
                <a href="${pageContext.request.contextPath}/user/student_login.jsp">登录</a>
                后发表评论
            </div>
        </div>
    </div>
    </c:if>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/index.css">
    <section class="tent_info">
        <%--//登陆状态--%>
        <c:if test="${student.id!=null || teacher.id != null}">
            <div class="col-md-12 col-sm-12 col-xl-12 textarea one">
                <textarea class="text_send" id="text_send"></textarea>
                <div class="col-md-12 col-sm-12 col-xl-12 btn btn_send">提交</div>
            </div>
        </c:if>
        <div id="newpl"></div>
        <%--有评论时--%>
        <div id="commentsDiv">
        </div>
    </section>
    <input hidden id="hid" value="${studentmessage.homework.id}"/>
    <span hidden id="plName"><c:if test="${student!=null}">${student.nickName}</c:if><c:if
            test="${teacher!=null}">${teacher.name}</c:if></span>
    <span hidden id="header"><c:if test="${student!=null}">${student.header}</c:if><c:if
            test="${teacher!=null}">${teacher.picture}</c:if></span>
    <span hidden id="tourId"><c:if test="${student!=null}">${student.id}</c:if><c:if
            test="${teacher!=null}">${teacher.id}</c:if></span>
    <span hidden id="pageNum"></span>
    <script type="text/javascript" src="${pageContext.request.contextPath}/user/js/jquery-min.js"></script>
    <script src="${pageContext.request.contextPath}/user/js/bootstrap-paginator.js"></script>

    <%--作业评论--%>
    <script type="text/javascript">
        $(".btn_send").on('click', function () {
            var now = time();
            //获取评论信息
            var text_send = $("#text_send").val();
            var name = $("#plName").text();
            var header = $("#header").text();
            var tourId = $("#tourId").text();
            if (text_send == "") {
                return false;
            }
            var hid = $("#hid").val();
            $.get("/user/jobdetails/commentsAdd", {
                "hid": hid,
                "content": text_send,
                "id": tourId,
                "name": name,
                "header": header
            }, function (data) {
                seach()
            });
            $(".text_send").val("");
        });
        $(".content_text").show();
        $(".tent_info").on('click', '.btn_back', function () {
            var back_time = time();
            var hid = $("#hid").val();
            //获取评论信息
            var con_back = $(this).parent().parent().parent().find("#con_back").val();
            var cid = $(this).parent().parent().parent().find("#cid").val();
            var name = $("#plName").text();
            var header = $("#header").text();
            var tourId = $("#tourId").text();
            if (con_back == "") {
                return false;
            }
            $.get("/user/jobdetails/answerAdd", {
                "cid": cid,
                "content": con_back,
                "id": tourId,
                "name": name,
                "header": header
            }, function x(data) {
                seach($("#pageNum").html())
            });
            $(this).parent().parent().parent().find("#con_back").val("");
        });

        function hfDivToggle(aid) {
            var str = "#hfDiv" + aid;
            $(str).slideToggle();
        }

        function del(id, choose, page) {
            $.get("/user/jobdetails/delCommentsOrAnswer", {"id": id, "choose": choose}, function (data) {
                if (choose == 1) {
                    seach()
                }
                if (choose == 2) {
                    seach(page)
                }
            })
        }

        //评论提交
        function time() {
            function time(s) {
                return s < 10 ? '0' + s : s;
            }

            var myDate = new Date();
            //获取当前年
            var year = myDate.getFullYear();
            //获取当前月
            var month = myDate.getMonth() + 1;
            //获取当前日
            var date = myDate.getDate();
            var h = myDate.getHours();       //获取当前小时数(0-23)
            var m = myDate.getMinutes();     //获取当前分钟数(0-59)
            var s = myDate.getSeconds();
            return year + '-' + time(month) + "-" + time(date) + " " + time(h) + ':' + time(m) + ":" + time(s);
        }
    </script>


    <div class="bzq">
        <div class="bzy">
            <div class="bzt">
                <div class=" rollIn bztl">猜你喜欢</div>
            </div>
            <!--滚动播放插件-->
            <div class=" bounceInRight wrap" id="wrap">
                <ul class="content"></ul>
                <a href="javascript:;" class="prev">&#60;</a>
                <a href="javascript:;" class="next">&#62;</a>
            </div>
            <!--滚动播放插件-->
        </div>
    </div>
    <!--bottom-->
    <jsp:include page="bottom.jsp"></jsp:include>
    <!--di-->
    <div class="diq">
        <div class="diy">
            <div class="di">
                <a>DT人才培训基地（太原中心）</a>|
                <a>晋ICP备16009028号</a>|
                <a>咨询热线：400-7777-699</a>|
                <a>地址：太原市高新区平阳南路龙兴街万立科技大厦17层</a>|
                <a>版权所有：华信智原</a>
            </div>
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/user/js/jqthumb.min.js"></script>
    <script>
        $(function () {
            seach()
        });

        // 图片不变形配置
        $('.tuijian').jqthumb({
            classname: 'jqthumb',
            width: 150,
            height: 96,
            showoncomplete: true
        });
        $('.xqbrl img').jqthumb({
            classname: 'jqthumb',
            width: 90,
            height: 90,
            showoncomplete: true
        });
        $('.zyzs_tjb_rb_l img').jqthumb({
            classname: 'jqthumb',
            width: 30,
            height: 30,
            showoncomplete: true
        });

        function guanzhu() {
            var val = $("#cb5").val();
            //点击关注后，如果未登录，跳转到登录界面
            if (${sessionScope.student == null}) {
                location.href = "${pageContext.request.contextPath}/user/student_login.jsp";
            } else { //如果已登录，向后台发送异步请求添加(0)或删除(1)一条关注数据
                var follow = ${empty sessionScope.student.id ? 0 : sessionScope.student.id};
                var toFollow = ${toFollowId};
                if (val == "0") {
                    $.get("${pageContext.request.contextPath}/user/student/concern", {
                        "follow": follow,
                        "toFollow": toFollow
                    }, function (data) {
                        $("#cb5").val("1");
                    }, "json")
                } else if (val == "1") {
                    $.get("${pageContext.request.contextPath}/user/student/cancel", {
                        "follow": follow,
                        "toFollow": toFollow
                    }, function (data) {
                        $("#cb5").val("0");
                    }, "json")
                }
            }
        }

        function seach(pageNum) {
            tourId = $("#tourId").text()
            var hid = ${studentmessage.homework.id}
                $.get("/user/jobdetails/findCommentsByHid", {
                    "pagenum": pageNum,
                    "hid": hid
                }, function x(pageData) {
                    var comments = pageData.content;
                    $("#commentsDiv").val("");
                    var str = "";
                    if (comments.length == 0) {
                        str += "<img src=\"/user/img/pinglun1.png\" alt=\"评论图片\"style=\"margin: 0px 400px\"/>"
                    } else {
                        for (var i = 0; i < comments.length; i++) {
                            thisComments = comments[i];
                            str += "<div class=\"col-md-12 col-sm-12 col-xl-12 one cont\">\n" +
                                "<div class=\"col-md-2 col-sm-2 col-xl-12 one img\">\n" +
                                "<img style=\"border-radius: 100%;width: 100px;height: 100px;\" src='" + thisComments.header + "'>\n" +
                                "</div>\n" +
                                "<div class=\"col-md-10 col-sm-10 col-xl-12 one content\">\n" +
                                "<div class=\"comment-right\">\n" +
                                "<h4>" + thisComments.name + "</h4>\n" +
                                "<div class=\"comment-content-header three\">\n" +
                                "<span><i class=\"glyphicon glyphicon-time two\"></i>" + thisComments.timeStr + "</span>&nbsp;&nbsp;&nbsp;&nbsp;\n<span style='margin-left: 500px'>";
                            if (tourId != '') {
                                str += "<a onclick='hfDivToggle(" + i + ")' style='cursor:hand'>回复</a>"
                            }
                            if (tourId == thisComments.tourId) {
                                str += "&nbsp;&nbsp;<a onclick='del(" + thisComments.id + ",1," + (pageData.pageable.pageNumber + 1) + ")' style='cursor:hand'>删除</a>\n";
                            }
                            str += "</span>" +
                                "</div>\n" +
                                "<p class=\"tent\">" + thisComments.content + "</p>\n" +
                                "<div class=\"comment-content-footer\">\n" +
                                "<div class=\"row\">\n" +
                                "<div class=\"col-md-12 col-sm-12 col-xl-12 three\">\n" +
                                "</div>\n" +
                                "</div>\n" +
                                "</div>\n";
                            if (thisComments.answers.length != 0) {
                                for (var j = 0; j < thisComments.answers.length; j++) {
                                    answer = thisComments.answers[j];
                                    str += "<div style='margin:15px 5px' class=\"reply-list zyhf three\">\n" +
                                        "<div class=\"reply\">\n" +
                                        "<div class=\"three\">\n" +
                                        "<a class=\"replyname\">" + answer.name + "</a>:<span>" + answer.timeStr + "</span>\n";
                                    if (tourId == answer.tourId) {
                                        str += "<span style='padding-left:480px;width:30px'><a onclick='del(" + answer.id + ",2," + (pageData.pageable.pageNumber + 1) + ")' style='cursor:hand'>删除</a></span>";
                                    }
                                    str += "<div><p>" + answer.answer + "</p></div>" +
                                        "</div>\n" +
                                        "</div>\n" +
                                        "</div>\n";
                                }
                            }
                            str +=
                                "<div  id='hfDiv" + i + "' style='display: none;' class=\"col-md-12 col-sm-12 col-xl-12 content_text one three hfDiv\">\n" +
                                "<input value=\"" + thisComments.id + "\" hidden id=\"cid\">\n" +
                                "<textarea style='width: 700px' class=\"col-md-10 col-sm-10 col-xl-10 con_back\" id=\"con_back\"></textarea>\n" +
                                "<a style='width: 60px' class='btn btn-danger' onclick='hfDivToggle(" + i + ")'>取消</a>" +
                                "<a style='width: 60px' class='btn btn-default btn_back'>回复</a>" +
                                "</div>\n" +
                                "</div>\n" +
                                "</div>"
                        }
                        str += "<div class=\"box-footer\" style=\"clear: both;margin-left: 60%;\">\n" +
                            "<div>\n" +
                            "<ul class=\"pageLimit\"></ul>\n" +
                            "</div>\n" +
                            "</div>"
                    }
                    $("#commentsDiv").html(str);
                    // 分页插件
                    $('.pageLimit').bootstrapPaginator({
                        bootstrapMajorVersion: 3, // bootstrap版本
                        currentPage: pageData.pageable.pageNumber + 1, // 当前页
                        totalPages: pageData.totalPages, // 总页数
                        numberOfPages: 5, // 最多显示多少页
                        itemTexts: function (type, page, current) {
                            switch (type) {
                                case "first":
                                    return "首页";
                                case "prev":
                                    return "上一页";
                                case "next":
                                    return "下一页";
                                case "last":
                                    return "末页";
                                case "page":
                                    return page;
                            }
                        },
                        onPageClicked: function (event, originalEvent, type, page) { // 点击页码执行的方法
                            $("#pageNum").html(page)
                            seach(page)
                        }
                    })
                })
        }
    </script>
</body>
</html>
