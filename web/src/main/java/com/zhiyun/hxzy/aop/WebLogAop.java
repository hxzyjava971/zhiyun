package com.zhiyun.hxzy.aop;

import com.zhiyun.hxzy.domain.*;
import com.zhiyun.hxzy.service.ProgrammerService;
import com.zhiyun.hxzy.service.WebErrorLogService;
import com.zhiyun.hxzy.service.WebLogService;
import com.zhiyun.hxzy.util.MailUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;

@Component
@Aspect
public class WebLogAop {
    @Autowired
    private WebErrorLogService errorLogService;
    @Autowired
    private HttpServletRequest request;
    private Date visitTime;
    @Autowired
    private MailUtil mailUtil;
    @Autowired
    private ProgrammerService programmerService;
    @Autowired
    private JdbcTemplate template;

//    @Before("execution(* com.zhiyun.hxzy.controller.user.*.*(..))")
//    public void qianzhi() {
//        visitTime = new Date();
//    }

//    @AfterReturning("execution(* com.zhiyun.hxzy.controller.user.*.*(..))")
//    public void houzhi(JoinPoint joinPoint) {
//        String requestURI = request.getRequestURI();//访问路径
//        String localAddr = request.getLocalAddr();//访问ip
//        long l = new Date().getTime() - visitTime.getTime();//访问时长
//        String name = joinPoint.getSignature().getName();//访问方法
//
//        HttpSession session = request.getSession();
//        WebLog log = new WebLog();
//        if(session.getAttribute("teacher") != null){
//            log.setUsername(((Teacher)session.getAttribute("teacher")).getName());
//        }else if(session.getAttribute("student") != null){
//            log.setUsername(((Student)session.getAttribute("student")).getName());
//        }
//
//        log.setVisitTime(visitTime);
//        log.setExecutionTime(l);
//        log.setIp(localAddr);
//        log.setUrl(requestURI);
//        log.setMethod(name);
//        template.update("insert into hx_weblog value(null,?,?,?,?,?,?)",log.getExecutionTime(),
//                log.getIp(),log.getMethod(),log.getUrl(),log.getUsername(),log.getVisitTime());
//    }

//    @AfterThrowing(throwing = "ex",pointcut = "execution(* com.zhiyun.hxzy.controller.user.*.*(..))")
//    public void yichang(JoinPoint joinPoint, Throwable ex) {
//        String requestURI = request.getRequestURI();//访问路径
//        String localAddr = request.getLocalAddr();//访问ip
//        String name = joinPoint.getSignature().getName();//访问方法
//        String message = ex.getMessage();
//
//        WebErrorLog log = new WebErrorLog();
//        log.setVisitTime(visitTime);
//        log.setIp(localAddr);
//        log.setUrl(requestURI);
//        log.setMethod(name);
//        log.setMessage(message);
//
//        HttpSession session = request.getSession();
//        if(session.getAttribute("teacher") != null){
//            log.setUsername(((Teacher)session.getAttribute("teacher")).getName());
//        }else if(session.getAttribute("student") != null){
//            log.setUsername(((Student)session.getAttribute("student")).getName());
//        }
//
//        // 获取异常栈信息
//        ByteArrayOutputStream buf = new ByteArrayOutputStream();
//        ex.printStackTrace(new java.io.PrintWriter(buf, true));
//        String expMessage = buf.toString();
//        log.setStackTrace(expMessage);
//        errorLogService.addErrorLog(log);
//        // 给程序员发送邮件：异常信息邮件
//        String text = "出异常了！：" +
//                "<br>异常信息:" + ex.getMessage() +
//                "<br>异常栈信息:" + expMessage;
//
//        // 发送邮件
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                List<Programmer> programmers = programmerService.findAllActiveProgrammer();
//                for (Programmer programmer:programmers){
//                    mailUtil.sendMail(programmer.getPemail(),text,"智云系统出异常了!");
//                }
//            }
//        }).start();
//    }
}
