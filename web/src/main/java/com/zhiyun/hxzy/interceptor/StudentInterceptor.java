package com.zhiyun.hxzy.interceptor;

import com.zhiyun.hxzy.domain.Student;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class StudentInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        Student student = (Student) request.getSession().getAttribute("student");
        if (student == null || "".equals(student)) {
            System.out.println("学生session验证失败:" + request.getRequestURI());
            response.sendRedirect("/user/student_login.jsp");
            return false;
        } else {
            System.out.println("学生session验证通过:" + request.getRequestURI());
            return true;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

    }
}
