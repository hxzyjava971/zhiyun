package com.zhiyun.hxzy.interceptor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfiguration {
    //所有的WebMvcConfigurerAdapter组件都会一起起作用
    @Bean //将组件注册在容器中
    public WebMvcConfigurer webMvcConfigurerAdapter(){
        return new WebMvcConfigurer(){
            //注册拦截器
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                //静态资源； *.css,*.js
                //SpringBoot已经做好了静态资源映射
//                registry.addInterceptor(new LoginHandlerInterceptor()).addPathPatterns("/**")
//                .excludePathPatterns("/index.html","/","/user/login","/static/**","/webjars/**");
                // /**  表示拦截所有路径下的所有请求
                registry.addInterceptor(new StudentInterceptor())
                        .addPathPatterns("/user/student/updatePersonalMessage",
                                "/user/homework/upload",
                                "/user/homework/findNotCommentHomeworkByStudent",
                                "/user/homework/findNotCommentHomeworkByStudentYiBu",
                                "/user/homework/updateNotCommentHomeworkByStudentPre",
                                "/user/homework/delNotCommentHomeworkByStudent",
                                "/user/homework/findAlreadyCommentHomeworkByStudent",
                                "/user/homework/findAlreadyCommentHomeworkByStudentYiBu",
                                "/user/homework/updateNotCommentHomeworkByStudent",
                                "/user/student/addPre",
                                "/user/student/addHomework",
                                "/user/student/uploadHeaderImg"
                                );
                registry.addInterceptor(new TeacherInterceptor())
                        .addPathPatterns(
//                                "/user/teacher/resetPassword",
                                "/user/homework/findById",
                                "/user/homework/commentHomework",
                                "/user/homework/findNotCommentHomeworkByTeacher",
                                "/user/homework/findNotCommentHomeworkByTeacherYiBu",
                                "/user/homework/findAlreadyCommentHomeworkByTeacher",
                                "/user/homework/findAlreadyCommentHomeworkByTeacherYiBu"
                                );
            }
        };
    }
}
