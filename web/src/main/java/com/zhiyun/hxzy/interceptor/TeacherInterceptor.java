package com.zhiyun.hxzy.interceptor;

import com.zhiyun.hxzy.domain.Teacher;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class TeacherInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        Teacher teacher = (Teacher) request.getSession().getAttribute("teacher");
        if (teacher == null || "".equals(teacher)) {
            System.out.println("老师session验证失败:" + request.getRequestURI());
            response.sendRedirect("/user/teacher_login.jsp");
            return false;
        } else {
            System.out.println("老师session验证通过:" + request.getRequestURI());
            return true;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

    }
}
