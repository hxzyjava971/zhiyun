package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.domain.ErrorLog;
import com.zhiyun.hxzy.service.ErrorLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin/errorLog")
public class ErrorLogController {
    @Autowired
    private ErrorLogService errorLogService;

    @RequestMapping("/whole")
    public ModelAndView findAll(@RequestParam(defaultValue = "0") int pageNum,
                                @RequestParam(defaultValue = "5") int pageSize,
                                @RequestParam(defaultValue = ("")) String username,
                                @RequestParam(defaultValue = ("")) String visitTime
    ){
        Page<ErrorLog> errorLogs = errorLogService.findAll (pageNum-1,pageSize,username,visitTime);
        ModelAndView modelAndView=new ModelAndView ();
        modelAndView.getModelMap ().addAttribute ("errorLogs",errorLogs);
        modelAndView.getModelMap ().addAttribute ("username",username);
        modelAndView.getModelMap ().addAttribute ("visitTime",visitTime);
        modelAndView.setViewName ("./admin/errorlog_list");
        return modelAndView;
    }

    @RequestMapping("/findById")
    public ModelAndView findById(Long id){
        ErrorLog byId = errorLogService.findById(id);
        ModelAndView modelAndView=new ModelAndView ();
        modelAndView.getModelMap().addAttribute("log",byId);
        modelAndView.setViewName("admin/errorLog_desc");
        return modelAndView;
    }
}
