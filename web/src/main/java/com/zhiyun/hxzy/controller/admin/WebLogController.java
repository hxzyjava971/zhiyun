package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.domain.WebLog;
import com.zhiyun.hxzy.service.WebLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin/weblog")
public class WebLogController {
    @Autowired
    private WebLogService logService;

    @RequestMapping("/whole")
    public ModelAndView findAll(@RequestParam(defaultValue = "0") int pageNum, @RequestParam(defaultValue = "5") int pageSize,@RequestParam(defaultValue = "") String visitTime){
        Page<WebLog> logs = logService.findAll (pageNum-1,pageSize,visitTime);
        ModelAndView modelAndView=new ModelAndView ();
        modelAndView.getModelMap ().addAttribute ("logs",logs);
        modelAndView.getModelMap ().addAttribute ("visitTime",visitTime);
        modelAndView.setViewName ("./admin/weblog_list");
        return modelAndView;
    }
}
