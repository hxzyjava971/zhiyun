package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.domain.Log;
import com.zhiyun.hxzy.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;

@Controller
@RequestMapping("/admin/log")
public class LogController {
    @Autowired
    private LogService logService;

    @RequestMapping("/whole")
    public ModelAndView findAll(@RequestParam(defaultValue = "0") int pageNum, @RequestParam(defaultValue = "5") int pageSize, String datepicker) throws ParseException {
        Page<Log> logs = null;
        if (datepicker != null && datepicker.length() != 0) {
            logs = logService.search(datepicker, pageNum-1, pageSize);
        } else {
            logs = logService.findAll(pageNum - 1, pageSize);
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.getModelMap().addAttribute("logs", logs);
        modelAndView.getModelMap().addAttribute("datepicker", datepicker);
        modelAndView.setViewName("./admin/log_list");
        return modelAndView;
    }

}
