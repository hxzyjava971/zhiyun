package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.dao.HomeWorkDao;
import com.zhiyun.hxzy.domain.*;
import com.zhiyun.hxzy.service.ClassesService;
import com.zhiyun.hxzy.service.HomeWorkService;
import com.zhiyun.hxzy.service.SchoolService;
import com.zhiyun.hxzy.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/admin/homework")
public class HomeWorkController {
    @Autowired
    private HomeWorkService homeWorkService;
    @Autowired
    private HomeWorkDao homeWorkDao;
    @Autowired
    private SchoolService schoolService;
    @Autowired
    private ClassesService classesService;
    @Autowired
    private StudentService studentService;

    @RequestMapping("/findByTagsId")
    public ModelAndView findByTagsId(Long id) {
        ModelAndView modelAndView = new ModelAndView();
        List<Homework> homeworks = homeWorkService.findbyTags(id);
        modelAndView.getModelMap().addAttribute("homeworks", homeworks);
        modelAndView.setViewName("/admin/homework_list");
        return modelAndView;
    }

    //查询所有作业
    @RequestMapping("/findAll")
    public ModelAndView findAll(@RequestParam(defaultValue = "0") int pageNum,
                                @RequestParam(defaultValue = "5") int pageSize,
                                Long schoolId,
                                Long classId,
                                Long studentId) {
        ModelAndView modelAndView = new ModelAndView();
        Page<Homework> all = homeWorkService.findAll(pageNum - 1, pageSize, studentId);
        modelAndView.getModelMap().addAttribute("homeworks", all);
        if (studentId != null) {
            modelAndView.getModelMap().addAttribute("nowSchool", schoolService.findById(schoolId));
            modelAndView.getModelMap().addAttribute("nowClasses", classesService.findById(classId));
            modelAndView.getModelMap().addAttribute("nowStudents", studentService.getOne(studentId));

            modelAndView.getModelMap().addAttribute("schoolId", schoolId);
            modelAndView.getModelMap().addAttribute("classId", classId);
            modelAndView.getModelMap().addAttribute("studentId", studentId);
        }

        modelAndView.setViewName("/admin/homework_list");

        List<School> all1 = schoolService.findAll();//查询所有学校
        modelAndView.getModelMap().addAttribute("schools", all1);

        List<Classes> all2 = classesService.findAll(schoolId);//查询所有班级
        modelAndView.getModelMap().addAttribute("classes", all2);

        List<Student> all3 = studentService.findAll(classId);//查询所有学生
        modelAndView.getModelMap().addAttribute("students", all3);

        return modelAndView;
    }

    @RequestMapping({"/del"})
    public String del(Long[] ids) {
        homeWorkService.delHomeWork1(ids);
        homeWorkService.del(ids);

        return "redirect:/admin/homework/findAll";
    }
}
