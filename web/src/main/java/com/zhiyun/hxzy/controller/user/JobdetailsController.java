package com.zhiyun.hxzy.controller.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.zhiyun.hxzy.dao.HomeWorkDao;
import com.zhiyun.hxzy.domain.Comments;
import com.zhiyun.hxzy.domain.Homework;
import com.zhiyun.hxzy.domain.Student;
import com.zhiyun.hxzy.domain.queryvo.Result;
import com.zhiyun.hxzy.domain.queryvo.ShouyeHomeworkVO;
import com.zhiyun.hxzy.service.ConcernService;
import com.zhiyun.hxzy.service.CommentsService;
import com.zhiyun.hxzy.service.HomeWorkService;
import com.zhiyun.hxzy.service.JobdetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/user/jobdetails")
public class JobdetailsController {
    @Autowired
    private JobdetailService jobdetailService;
    @Autowired
    private HomeWorkService homeWorkService;
    @Autowired
    private CommentsService commentsService;
    @Autowired
    private HomeWorkDao homeWorkDao;
    @Autowired
    private ConcernService concernService;

    //作业详情查询
    @RequestMapping("/message")
    public ModelAndView message(Long id, HttpSession session) {
        Student student = (Student) session.getAttribute("student");//session中的student是关注者
        ModelAndView modelAndView = new ModelAndView();
        ModelMap modelMap = modelAndView.getModelMap();
        jobdetailService.addClicks(id); // 点击量+1
        ShouyeHomeworkVO studentmessage = jobdetailService.findByStudentmessage(id);
        List<Homework> otherTasks = jobdetailService.findOtherTask(id);
        Long toFollowId = studentmessage.getStudent().getId();//作业对象中的student是被关注者
        modelMap.addAttribute("otherTasks", otherTasks);
        modelMap.addAttribute("studentmessage", studentmessage);
        modelMap.addAttribute("toFollowId", toFollowId);
        if (student != null) {
            modelMap.addAttribute("concern", concernService.judge(student.getId(),toFollowId));
        }
        modelAndView.setViewName("user/zuoyexiangqing");

        return modelAndView;
    }

    @RequestMapping("/otherHomework")
    public @ResponseBody
    List<Homework> otherHomework(Long studentId) {
        List<Homework> homeworks = jobdetailService.findOtherTask(studentId);
        return homeworks;
    }

    // 修改点赞数
    @RequestMapping("/thumbsUp")
    public @ResponseBody
    void thumbsUp(Long homeworkId, int num) { // num+1点赞增加，num减一点赞减少
        jobdetailService.thumbsUp(homeworkId, num);
    }

    // 查询点赞数
    @RequestMapping("/findThumbsUp")
    public @ResponseBody
    int findThumbsUp(Long homeworkId) { // num+1点赞增加，num减一点赞减少
        return jobdetailService.findThumbsUp(homeworkId);
    }

    //添加一条评论
    @RequestMapping("/commentsAdd")
    public @ResponseBody
    Result commentsAdd(Long hid, String content, Long id, String name, String header) throws JsonProcessingException {
        return commentsService.addComments(hid, id, name, header, content);
    }

    //回复评论
    @RequestMapping("/answerAdd")
    public @ResponseBody
    Result answerAdd(Long cid, String content, Long id, String name, String header) throws JsonProcessingException {
        return commentsService.addAnswers(cid, id,name,header, content);
    }

    //删除评论或删除回复
    @RequestMapping("/delCommentsOrAnswer")
    public @ResponseBody
    Result delCommentsOrAnswer(Long id, int choose) {
        Result result = null;
        if (choose == 1) {
            result = commentsService.delComments(id);
        }
        if (choose == 2) {
            result = commentsService.delAnswers(id);
        }
        return result;
    }

    //评论分页
    @RequestMapping("/findCommentsByHid")
    public @ResponseBody
    Page<Comments> findCommentsByHid(Long hid, @RequestParam(defaultValue = "1") int pagenum, @RequestParam(defaultValue = "5") int pagesize) {
        Page<Comments> comments = commentsService.findAll(hid, pagenum, pagesize);
        return comments;
    }
}
