package com.zhiyun.hxzy.controller.user;

import com.zhiyun.hxzy.domain.Classes;
import com.zhiyun.hxzy.domain.School;
import com.zhiyun.hxzy.domain.Student;
import com.zhiyun.hxzy.service.ClassesService;
import com.zhiyun.hxzy.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/user/school")
public class UserSchoolController {
    @Autowired
    private SchoolService schoolService;
    @Autowired
    private ClassesService classesService;
    //根据校区找到班级--注册时使用
    @RequestMapping("/fcbs")
    public @ResponseBody List<Classes> findClassBySchoolId(Long schoolId){
        List<Classes> classesList = schoolService.findById(schoolId).getClassesList();
        return classesList;
    }

    //根据校区名找到班级
    @RequestMapping("/findClass")
    public @ResponseBody List<Classes> findClass(Long schoolName){
        List<Classes> classesList = classesService.findBySchool(schoolName);
        return classesList;
    }

    //根据班级找到学生
    @RequestMapping("/findStudent")
    public @ResponseBody List<Student> findStudent(String className){
        List<Student> students = classesService.findStudentByClassName(className);
        return students;
    }

    //注册--找到所有班级
    @RequestMapping("/allS")
    public @ResponseBody List<School> findAllSchool(){
        return schoolService.findAll();
    }
}
