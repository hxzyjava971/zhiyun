package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.domain.Classes;
import com.zhiyun.hxzy.domain.Dictionary;
import com.zhiyun.hxzy.domain.School;
import com.zhiyun.hxzy.domain.Teacher;
import com.zhiyun.hxzy.domain.queryvo.Result;
import com.zhiyun.hxzy.service.ClassesService;
import com.zhiyun.hxzy.service.SchoolService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@SessionAttributes("id")
@RequestMapping("/admin/classes")
public class ClassesController {
    @Autowired
    private ClassesService classesService;
    @Autowired
    private SchoolService schoolService;

    @RequestMapping("/all")
    public ModelAndView all( @RequestParam(defaultValue = "0") int pageNum, @RequestParam(defaultValue = "5") int pageSize) {
        if (pageNum == 0) {
            pageNum = 1;
        }
        Page<Classes> page = classesService.findAll(pageNum - 1, pageSize);
        List<School> schools = schoolService.findAll();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.getModel().put("classesList", page);
        modelAndView.getModel().put("schools",schools);
        modelAndView.setViewName("/admin/classes_list");
        return modelAndView;
    }

    @RequestMapping("/findByIdAndUpdate")
    public String findByIdAndUpdate(@RequestParam(defaultValue = "0") Long id, Model model, String type) {
        Classes classes = classesService.findById(id);
        List<Dictionary> dictionaries = classesService.findAllDictionary(type);
        model.addAttribute("classes", classes);
        model.addAttribute("schools", schoolService.findAll());
        model.addAttribute("dictionaries", dictionaries);
        return "./admin/classes_add";
    }

    @RequestMapping("/findById")
    public String findById(Model model, String type, Long id) {
        Classes classes = classesService.findById(id);
        List<Dictionary> dictionaries = classesService.findAllDictionary(type);
        model.addAttribute("classes", classes);
        model.addAttribute("schools", this.schoolService.findAll());
        model.addAttribute("dictionaries", dictionaries);
        return "./admin/classes_update";
    }

    @RequestMapping("/update")
    public String update(Classes classes) {
        classesService.modify(classes);
        return "redirect:/admin/classes/all";
    }

    //查询班级没有的老师
    @RequestMapping("/findByNotTeacher")
    public String findByNotTeacher(Long classesId, Model model) {
        Classes classes = classesService.findById(classesId);
        model.addAttribute("classes", classes);
        List<Teacher> teacherList = classesService.findByNotTeacher(classesId);
        model.addAttribute("teacherList", teacherList);
        return "./admin/classes_add_teacher";
    }

    @RequestMapping("/findAllSchoolAndType")
    public String findAllSchoolAndType(Model model, String type) {
        List<School> schools = classesService.findAllSchool();
        List<Dictionary> dictionaries = classesService.findAllDictionary(type);
        model.addAttribute("schools", schools);
        model.addAttribute("dictionaries", dictionaries);
        return "./admin/classes_add";
    }

    //添加老师
    @RequestMapping("/addTeacher")
    public String addTeacher(Long classId, Long[] teacherId) {
        classesService.addTeacher(classId, teacherId);
        return "redirect:/admin/classes/all";
    }

    //添加班级_添加校区_添加课程
    @RequestMapping("/add")
    public String add(Classes classes, Long schoolId) {
        classesService.add(classes, schoolId);
        return "redirect:/admin/classes/all";
    }

    @RequestMapping("/del")
    public @ResponseBody
    Result del(Long[] ids) {
        return classesService.del(ids);
    }

    //查看班级的老师
    @RequestMapping("/findTeacherByClassesId")
    public String findTeacherByClassesId(Long classesId, Model model) {
        Classes classes = classesService.findById(classesId);
        model.addAttribute("id", classesId);
        model.addAttribute("classes", classes);
        return "./admin/classes_info";
    }

    //模糊查询根据校区
    @RequestMapping("/schoolName")
    public ModelAndView findLikeSchoolName(@RequestHeader String referer, String schoolName, @RequestParam(defaultValue = "0") int pageNum, @RequestParam(defaultValue = "5") int pageSize) {
        if (pageNum == 0) {
            pageNum = 1;
        }
        Page<Classes> classesList = classesService.findBySchool(schoolName, pageNum-1, pageSize);
        List<School> schools = schoolService.findAll();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.getModel().put("classesList", classesList);
        modelAndView.getModel().put("schoolName", schoolName);
        modelAndView.getModel().put("schools", schools);
        modelAndView.setViewName("/admin/classes_list");
        return modelAndView;
    }


    @RequestMapping("/delTeacher")
    public String delTeacher(Long teacherId, Model model, HttpServletRequest request) {
        Long classesId = (Long) model.getAttribute("id");
        classesService.delTeacher(teacherId, classesId);
        String referer = request.getHeader("referer");
        return "redirect:"+referer;
    }

    @RequestMapping("/delClassTeacher")
    public String delTeacher(Long teacherId, Long classesId, HttpServletRequest request) {
        classesService.delTeacher(teacherId, classesId);
        String referer = request.getHeader("referer");
        return "redirect:"+referer;
    }

}
