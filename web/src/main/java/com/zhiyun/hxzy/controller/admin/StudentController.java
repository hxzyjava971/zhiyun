package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.domain.Classes;
import com.zhiyun.hxzy.domain.School;
import com.zhiyun.hxzy.domain.Student;
import com.zhiyun.hxzy.service.ClassesService;
import com.zhiyun.hxzy.service.SchoolService;
import com.zhiyun.hxzy.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/admin/student")
public class StudentController {
    @Autowired
    private StudentService studentService;
    @Autowired
    private SchoolService schoolService;
    @Autowired
    private ClassesService classesService;

    //查询所有学生
    @RequestMapping("/findAll")
    public ModelAndView findAll(@RequestParam(defaultValue = "1") int pageNum, @RequestParam(defaultValue = "5") int pageSize,Long classId) {
        ModelAndView modelAndView = new ModelAndView();
        Page<Student> all = studentService.findAll(pageNum-1, pageSize,classId);
        modelAndView.getModelMap().addAttribute("students", all);
        List<School> schools = schoolService.findAll();//查询所有学校
        modelAndView.getModelMap().addAttribute("schools", schools);
        if(classId != null){
            modelAndView.getModelMap().addAttribute("classId", classId);
            modelAndView.getModelMap().addAttribute("nowClasses", classesService.findById(classId));
            modelAndView.getModelMap().addAttribute("nowSchool", classesService.findById(classId).getSchool());
            modelAndView.getModelMap().addAttribute("classes", classesService.findById(classId).getSchool().getClassesList());
        }
        modelAndView.setViewName("/admin/student_list");
        return modelAndView;
    }

    //根据id查询学生
    @RequestMapping("/getOne")
    public ModelAndView getOne(Long stuId) {
        ModelAndView modelAndView = new ModelAndView();
        Student student = studentService.getOne(stuId);
        modelAndView.getModelMap().addAttribute("student", student);
        modelAndView.setViewName("/admin/student-show");
        return modelAndView;
    }


    // 根据班级id查询学生
    @RequestMapping("/findByClassId")
    public @ResponseBody List<Student> findByClassId(Long classId){
        Classes classes = classesService.findById(classId);
        List<Student> studentList = classes.getStudentList();
        return studentList;
    }

}
