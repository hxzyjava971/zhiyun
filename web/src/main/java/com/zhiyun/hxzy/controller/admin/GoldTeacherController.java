package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.domain.GoldTeacher;
import com.zhiyun.hxzy.domain.queryvo.Result;
import com.zhiyun.hxzy.service.GoldTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/goldTeacher")
public class GoldTeacherController {
    @Autowired
    private GoldTeacherService goldTeacherService;

    @RequestMapping("/all")
    public String all(Model model, @RequestParam(defaultValue = "0") int pageNum, @RequestParam(defaultValue = "5") int pageSize) {
        if (pageNum == 0) {
            pageNum = 1;
        }
        model.addAttribute("teachers", goldTeacherService.findAll(pageNum - 1, pageSize));
        return "admin/goldTeacher_list";
    }

    @RequestMapping("/addOrUpdate")
    public String addOrUpdate(GoldTeacher goldTeacher) {
        goldTeacherService.add(goldTeacher);
        return "redirect:/admin/goldTeacher/all";
    }

    @RequestMapping("/updateStatus")
    public String updateStatus(Long id,HttpServletRequest request) {
        goldTeacherService.updateStatus(id);
        String referer = request.getHeader("referer");
        return "redirect:"+referer;
    }

    @RequestMapping("/findOne")
    public ModelAndView findOne(Long id) {
        GoldTeacher goldTeacher = goldTeacherService.findOne(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.getModelMap().addAttribute("goldTeacher",goldTeacher);
        modelAndView.setViewName("/admin/goldTeacher_update");
        return modelAndView;
    }

    @RequestMapping("/del")
    public String del(Long id,HttpServletRequest request) {
        goldTeacherService.del(id);
        String referer = request.getHeader("referer");
        return "redirect:"+referer;
    }

    @RequestMapping("/upload")
    public @ResponseBody Result uploadUimg(MultipartFile file) throws Exception {
        return goldTeacherService.upload(file);
    }


}