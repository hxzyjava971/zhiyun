package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.domain.Dictionary;
import com.zhiyun.hxzy.service.DictionaryService;
import org.apache.catalina.connector.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/admin/dictionary")
public class DictionaryController {
    @Autowired
    private DictionaryService dictionaryService;

    @RequestMapping("/findAll")
    public String findAllDictionary(@RequestParam(defaultValue = "0")int pageNum , @RequestParam(defaultValue = "5")int pageSize,ModelMap map,String type){
        Page<Dictionary> allDictionary = dictionaryService.findAllDictionary(pageNum-1,pageSize,type);
        map.addAttribute("allDictionary",allDictionary);
        map.addAttribute("type",type);
        return "./admin/dictionary_list";
    }

    @RequestMapping("/add")
    public String addDictionary(Dictionary dictionary,HttpServletRequest request){
        dictionaryService.addDictionary(dictionary);
        return "redirect:" + request.getContextPath() + "/admin/dictionary/findAll";
    }

    @RequestMapping("/delete")
    public String deleteDictionary(Long[] ids,HttpServletRequest request){
        String referer = request.getHeader("referer");
        dictionaryService.deleteDictionary(ids);
        return "redirect:" + referer;
    }

    @RequestMapping("/findById")
    public String findByIdDictionary(Long id,ModelMap map){
        Dictionary byIdDictionary = dictionaryService.findByIdDictionary(id);
        map.addAttribute("byIdDictionary",byIdDictionary);
        return "./admin/dictionary_update";

    }
    @RequestMapping("/update")
    public String updateDictionary(Dictionary dictionary,HttpServletRequest request){
        dictionaryService.updateDictionary(dictionary);
        return "redirect:" + request.getContextPath() + "/admin/dictionary/findAll";
    }

}
