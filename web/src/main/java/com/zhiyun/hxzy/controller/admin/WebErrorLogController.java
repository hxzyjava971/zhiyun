package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.domain.ErrorLog;
import com.zhiyun.hxzy.domain.WebErrorLog;
import com.zhiyun.hxzy.service.WebErrorLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin/weberrorLog")
public class WebErrorLogController {
    @Autowired
    private WebErrorLogService errorLogService;

    @RequestMapping("/whole")
    public ModelAndView findAll(@RequestParam(defaultValue = "0") int pageNum,
                                @RequestParam(defaultValue = "5") int pageSize,
                                @RequestParam(defaultValue = "") String visitTime){
        Page<WebErrorLog> errorLogs = errorLogService.findAll (pageNum-1,pageSize,visitTime);
        ModelAndView modelAndView=new ModelAndView ();
        modelAndView.getModelMap ().addAttribute ("errorLogs",errorLogs);
        modelAndView.getModelMap ().addAttribute ("visitTime",visitTime);
        modelAndView.setViewName ("./admin/weberrorlog_list");
        return modelAndView;
    }

    @RequestMapping("/findById")
    public ModelAndView findById(Long id){
        WebErrorLog byId = errorLogService.findById(id);
        ModelAndView modelAndView=new ModelAndView ();
        modelAndView.getModelMap().addAttribute("log",byId);
        modelAndView.setViewName("admin/webErrorLog_desc");
        return modelAndView;
    }
}
