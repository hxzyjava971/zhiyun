package com.zhiyun.hxzy.controller.user;

import com.zhiyun.hxzy.dao.DictionaryDao;
import com.zhiyun.hxzy.domain.Dictionary;
import com.zhiyun.hxzy.domain.UploadQualityWork;
import com.zhiyun.hxzy.domain.queryvo.Result;
import com.zhiyun.hxzy.service.DictionaryService;
import com.zhiyun.hxzy.service.StudentService;
import com.zhiyun.hxzy.service.TeacherUploadQualityWorkService;
import org.apache.catalina.connector.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/user/teacher")
public class TeacherUploadQualityWorkController {
    @Autowired
    private TeacherUploadQualityWorkService teacherUploadQualityWorkService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private DictionaryService dictionaryService;

    @RequestMapping("/openPage")
    public ModelAndView openPage(){
        ModelAndView modelAndView = new ModelAndView();
        List<Dictionary> types = dictionaryService.typeDictionary("精品作业类型");
        modelAndView.getModel().put("types",types);
        modelAndView.setViewName("/user/teacher_upload");
        return modelAndView;
    }

    //上传头图
    @RequestMapping("/uploadHeaderImg")
    public @ResponseBody
    Result uploadHeaderImg(String file) {
        return studentService.uploadHeaderImg(file);
    }

    /**
     * @param upload_quality_work
     * @return
     * @throws Exception
     *///老师上传精品作业
    @RequestMapping({"/addQualityWork"})
    public String add(UploadQualityWork uploadQualityWork) throws Exception {
        teacherUploadQualityWorkService.add(uploadQualityWork);
        return "/user/index";
    }

}
