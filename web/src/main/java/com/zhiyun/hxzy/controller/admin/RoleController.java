package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.domain.Role;
import com.zhiyun.hxzy.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/role")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @RequestMapping("/all")
    public ModelAndView all(@RequestParam(defaultValue = "1") int pageNum, @RequestParam(defaultValue = "5") int pageSize){
        ModelAndView modelAndView=new ModelAndView ();
        ModelMap modelMap = modelAndView.getModelMap ();
        Page<Role> roles = roleService.findAll (pageNum, pageSize);
        modelMap.addAttribute ("roles",roles);
        modelAndView.setViewName ("./admin/role_list");
        return modelAndView;
    }

    @RequestMapping("/add")
    public String add(Role role,HttpServletRequest request ){
        roleService.add (role);
        return "redirect:" + request.getContextPath() + "/admin/role/all";
    }

    @RequestMapping("/findOne")
    public ModelAndView findOne(Long id) {
        ModelAndView modelAndView=new ModelAndView ();
        ModelMap modelMap = modelAndView.getModelMap ();
        Role role = roleService.findId (id);
        modelMap.addAttribute ("role",role);
        modelAndView.setViewName ("admin/role_update");
        return modelAndView;
    }
    @RequestMapping("/update")
    public String updateRole(Role role,HttpServletRequest request){
        roleService.update(role);
        return "redirect:" + request.getContextPath() + "/admin/role/all";
    }

}
