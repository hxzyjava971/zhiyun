package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.domain.Admin;
import com.zhiyun.hxzy.domain.Role;
import com.zhiyun.hxzy.domain.Student;
import com.zhiyun.hxzy.service.AdminService;
import com.zhiyun.hxzy.service.RoleService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.net.PortUnreachableException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;
    @Autowired
    private RoleService roleService;

    @RequestMapping("/all")
    public ModelAndView findAll(@RequestParam(defaultValue = "0") int pageNum, @RequestParam(defaultValue = "5") int pageSize) {
        ModelAndView modelAndView = new ModelAndView();
        Page<Admin> admins = adminService.findAll(pageNum - 1, pageSize);
        ModelMap modelMap = modelAndView.getModelMap();
        modelMap.addAttribute("admins", admins);
        modelAndView.setViewName("admin/admin_list");
        return modelAndView;
    }

    @RequestMapping("/add")
    public String add(Admin admin, HttpServletRequest request) {
        adminService.add(admin);
        return "redirect:" + request.getContextPath() + "/admin/admin/all";
    }

    @RequestMapping("/findOne")
    public ModelAndView findAll(Long id) {
        ModelAndView modelAndView = new ModelAndView();
        Admin admin = adminService.getOne(id);
        List<Role> roles = roleService.findAll();
        List<Role> otherRoles = adminService.findOtherRole(id);
        List<Role> haveRoles = new ArrayList<>();
        // 取出用户不拥有的角色，拿到用户拥有的角色
        for (Role role:roles){
            boolean flag = false;
            for(Role otherRole:otherRoles){
                if(role.getId() == otherRole.getId()){
                    flag = true;
                    break;
                }
            }
            if (!flag){
                haveRoles.add(role);
            }
        }

        ModelMap modelMap = modelAndView.getModelMap();
        modelMap.addAttribute("admin", admin);
        modelMap.addAttribute("otherRoles", otherRoles);
        modelMap.addAttribute("haveRoles", haveRoles);
        modelAndView.setViewName("admin/admin_update");
        return modelAndView;
    }

    @RequestMapping("/update")
    public String update(Admin admin,Long[] roleIds,HttpServletRequest request) {
        adminService.update(admin,roleIds);
        return "redirect:" + request.getContextPath() + "/admin/admin/all";
    }

    @RequestMapping("/delete")
    public String delete(Long[] ids, Integer status, HttpServletRequest request) {
        adminService.delete(ids, status);
        return "redirect:" + request.getContextPath() + "/admin/admin/all";
    }


    @RequestMapping("/admindetail")
    public String adminDetail(Long id, ModelMap map) {
        Admin admin = adminService.adminDetail(id);
        map.addAttribute("adminDetail", admin);
        return "admin/admin_detail";

    }

    @RequestMapping("/repassword")
    public String repassword(Long id,String password,HttpServletRequest request){
        adminService.repassword(id,password);
        String referer = request.getHeader("referer");
        return "redirect:"+referer;
    }

    @RequestMapping("/repassword2")
    public String repassword2(String password,HttpServletRequest request){
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        adminService.repassword(username,password);
        String referer = request.getHeader("referer");
        return "admin/login";
    }

    @RequestMapping("/judgmentUserName")
    public @ResponseBody boolean judgmentUserName(String username){
        return adminService.judgmentUserName(username);
    }
}
