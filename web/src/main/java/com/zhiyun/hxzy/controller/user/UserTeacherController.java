package com.zhiyun.hxzy.controller.user;

import com.zhiyun.hxzy.domain.AutoLogin;
import com.zhiyun.hxzy.domain.queryvo.Result;
import com.zhiyun.hxzy.domain.Teacher;
import com.zhiyun.hxzy.service.AutoLoginService;
import com.zhiyun.hxzy.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.zhiyun.hxzy.service.ClassesService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/user/teacher")
public class UserTeacherController {
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private ClassesService classesService;
    @Autowired
    private AutoLoginService autoLoginService;

    //登录方式1：手机号 密码登录
    @RequestMapping("/passwordLogin")
    public @ResponseBody Result passwordLogin(HttpServletRequest request, String phoneNum1, String password, String autologin) {
        String uuid = null;
        if (autologin != null){
            String userAgent = request.getHeader("User-Agent");//客户端浏览器信息
            String ip = request.getRemoteAddr();//客户端ip地址
            uuid = ip +""+userAgent;
        }
        Teacher teacher = teacherService.findByPhoneAndPassword(phoneNum1, password);
        if (teacher != null) {
            AutoLogin autoLogin1 = autoLoginService.find(uuid);
            if (autoLogin1 == null){
                //将uuid、手机号及密码写入数据库
                autoLoginService.add(new AutoLogin(uuid,teacher.getPhone(),1));
            }else {
                autoLogin1.setPhoneNum(phoneNum1);
                autoLogin1.setIdentity(1);
                autoLoginService.add(autoLogin1);
            }
            request.getSession().setAttribute("teacher", teacher);
            return new Result("登录成功",true);
        } else {
            return new Result("手机或密码错误",false);
        }
    }

    //登录方式2：发送手机验证码
    @RequestMapping("/LoginSendPhoneCode")
    public @ResponseBody
    Result LoginSendPhoneCode(String phoneNum) {
        Result result = teacherService.LoginSendPhoneCode(phoneNum);
        return result;
    }

    //登录方式2：手机号 验证码登录
    @RequestMapping("/phoneCheckCodeLogin")
    public @ResponseBody Result phoneCheckCodeLogin(HttpServletRequest request, String phoneNum, String phoneCheckCode,String autologin) {
        String uuid = null;
        if (autologin != null){
            String userAgent = request.getHeader("User-Agent");//客户端浏览器信息
            String ip = request.getRemoteAddr();//客户端ip地址
            uuid = ip +""+userAgent;
        }
        Result result = teacherService.loginVerifyCode(phoneNum, phoneCheckCode);
        if (result.isResult()) {
            AutoLogin autoLogin1 = autoLoginService.find(uuid);
            if (autoLogin1 == null){
                //将uuid、手机号及密码写入数据库
                autoLoginService.add(new AutoLogin(uuid,phoneNum,1));
            }else {
                autoLogin1.setPhoneNum(phoneNum);
                autoLogin1.setIdentity(1);
                autoLoginService.add(autoLogin1);
            }
            Teacher teacher = teacherService.findByPhoneNum(phoneNum);
            request.getSession().setAttribute("teacher", teacher);
            return result;
        } else {
            return result;
        }
    }

    //老师用手机 找回密码
    @RequestMapping("/findPassword")
    public @ResponseBody Result findPassword(HttpServletRequest request, String phoneNum, String phoneCheckCode) {
        Result result = teacherService.loginVerifyCode(phoneNum, phoneCheckCode);
        if (result.isResult()) {
            request.getSession().setAttribute("phoneNum", phoneNum);
            return result;
        } else {
            return result;
        }
    }

    //老师重置密码
    @RequestMapping("/resetPassword")
    public String resetPassword(String password,String phoneNum) {
        teacherService.resetPassword(password,phoneNum);
        return "user/teacher_forgetpassword3";
    }

    //注销登录
    @RequestMapping("/logout")
    public String logout(HttpServletRequest req){
        //拿到浏览器信息和当前用户手机号并删除自动登录数据库中的数据
        String uuid = null;
        String userAgent = req.getHeader("User-Agent");//客户端浏览器信息
        String ip = req.getRemoteAddr();//客户端ip地址
        uuid = ip +""+userAgent;
        HttpSession session = req.getSession();
        Teacher teacher = (Teacher) session.getAttribute("teacher");
        AutoLogin autoLogin = new AutoLogin();
        autoLogin.setUuid(uuid);
        autoLogin.setPhoneNum(teacher.getPhone());
        autoLoginService.delete(autoLogin);

        session.removeAttribute("teacher");
        return "redirect:/user/teacher_login.jsp";
    }
}
