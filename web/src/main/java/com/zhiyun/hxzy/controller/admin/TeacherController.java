//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.domain.Teacher;
import com.zhiyun.hxzy.domain.queryvo.Result;
import com.zhiyun.hxzy.service.ClassesService;
import com.zhiyun.hxzy.service.SchoolService;
import com.zhiyun.hxzy.service.TeacherService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping({"/admin/teacher"})
public class TeacherController {
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private ClassesService classesService;
    @Autowired
    private SchoolService schoolService;

    public TeacherController() {
    }

    @RequestMapping({"/all"})
    public String all(Model model, @RequestParam(defaultValue = "0") int pageNum, @RequestParam(defaultValue = "5") int pageSize, Long schoolId) {
        if (pageNum == 0) {
            pageNum = 1;
        }

        if (schoolId != null) {
            model.addAttribute("nowSchool", this.schoolService.findById(schoolId));
        }

        model.addAttribute("teachers", this.teacherService.findAll(pageNum - 1, pageSize, schoolId));
        model.addAttribute("schools", this.schoolService.findAll());
        return "./admin/teacher_list";
    }

    @RequestMapping({"/addPre"})
    public String addPre(Model model) {
        model.addAttribute("schools", this.schoolService.findAll());
        return "./admin/teacher_add";
    }

    @RequestMapping({"/updatePre"})
    public String updatePre(Model model, Long id, @RequestParam(defaultValue = "0") int pageNum, @RequestParam(defaultValue = "5") int pageSize) {
        model.addAttribute("teacher", this.teacherService.findById(id));
        model.addAttribute("schools", this.schoolService.findAll());
        model.addAttribute("pageNum", pageNum);
        model.addAttribute("pageSize", pageSize);
        return "./admin/teacher_update";
    }

    @RequestMapping({"/add"})
    public String add(Teacher teacher) throws Exception {
        this.teacherService.add(teacher);
        return "redirect:/admin/teacher/all";
    }

    @RequestMapping({"/modify"})
    public String modify(Teacher teacher, @RequestParam(defaultValue = "0") int pageNum, @RequestParam(defaultValue = "5") int pageSize) {
        this.teacherService.modify(teacher);
        return "redirect:/admin/teacher/all?pageNum=" + pageNum + "&pageSize=" + pageSize;
    }

    @RequestMapping({"/del"})
    public String del(Long[] ids) {
        this.teacherService.del(ids);
        return "redirect:/admin/teacher/all";
    }

    @RequestMapping({"/addClassesPre"})
    public String addClassesPre(Model model, Long teacherId, @RequestParam(defaultValue = "0") int pageNum, @RequestParam(defaultValue = "5") int pageSize) {
        Teacher teacher = this.teacherService.findById(teacherId);
        model.addAttribute("teacher", teacher);
        model.addAttribute("classesList", this.classesService.findClassByNotHaveTeacher(teacherId, teacher.getSchool().getId()));
        model.addAttribute("pageNum", pageNum);
        model.addAttribute("pageSize", pageSize);
        return "./admin/teacher_addClasses";
    }

    @RequestMapping({"/addClasses"})
    public String addClasses(Long teacherId, Long[] classesId, @RequestParam(defaultValue = "0") int pageNum, @RequestParam(defaultValue = "5") int pageSize) {
        if (classesId.length != 0 && classesId != null) {
            this.teacherService.addClasses(teacherId, classesId);
        }

        return "redirect:/admin/teacher/all?pageNum=" + pageNum + "&pageSize=" + pageSize;
    }

    @RequestMapping({"/findClassesListByTeacherId"})
    public String findClassesListByTeacherId(Model model, Long teacherId) {
        Teacher teacher = this.teacherService.findById(teacherId);
        model.addAttribute("teacher", teacher);
        return "./admin/teacher_info";
    }

    @RequestMapping({"/upload"})
    @ResponseBody
    public Result uploadUimg(HttpServletRequest request, MultipartFile file) throws Exception {
        return this.teacherService.uploadUimg(file);
    }
}
