package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.domain.Employment;
import com.zhiyun.hxzy.domain.queryvo.Result;
import com.zhiyun.hxzy.domain.Student;
import com.zhiyun.hxzy.service.EmploymentService;
import com.zhiyun.hxzy.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/employment")
public class EmploymentController {
    @Autowired
    private EmploymentService employmentService;
    @Autowired
    private StudentService studentService;

    //根据id查询就业人信息
    @RequestMapping("/getOne")
    public ModelAndView getOne(Long id) {
        ModelAndView modelAndView = new ModelAndView();
        Employment employment = employmentService.getOne(id);
        modelAndView.getModelMap().addAttribute("employment", employment);
        modelAndView.setViewName("/admin/employment_update");
        return modelAndView;
    }

    //查询所有就业人信息
    @RequestMapping("/all")
    public ModelAndView findAll(@RequestParam(defaultValue = "1") int pageNum, @RequestParam(defaultValue = "5") int pageSize) {
        ModelAndView modelAndView = new ModelAndView();
        Page<Employment> all = employmentService.findAll(pageNum - 1, pageSize);
        modelAndView.getModelMap().addAttribute("employments", all);
        modelAndView.getModelMap().addAttribute("pageSize", pageSize);
        modelAndView.setViewName("/admin/employment_list");
        return modelAndView;
    }

    //新增就业信息
    @RequestMapping("/employmentAdd")
    public String employmentAdd(Employment employment) {
        employmentService.employmentAdd(employment);
        return "redirect:/admin/employment/all";
    }

    //图片上传
    @RequestMapping("/upload")
    public @ResponseBody
    Result uploadUimg(MultipartFile file) {
        return employmentService.updateUimg(file);
    }

    //就业信息修改
    @RequestMapping("/update")
    public String update(Employment employment) {
        employmentService.update(employment);
        return "redirect:/admin/employment/all";
    }

    //删除就业信息
    @RequestMapping("/del")
    public String update(Long id,HttpServletRequest request) {
        employmentService.delById(id);
        return "redirect:/admin/employment/all";
    }

    //修改开启关闭状态
    @RequestMapping("/updateStatus")
    public String updateStatus(Long id, HttpServletRequest request) {
        employmentService.employmentStar(id);
        String referer = request.getHeader("referer");
        return "redirect:"+referer;
    }

}
