package com.zhiyun.hxzy.controller.admin;

import com.zhiyun.hxzy.domain.Classes;
import com.zhiyun.hxzy.domain.School;
import com.zhiyun.hxzy.domain.queryvo.Result;
import com.zhiyun.hxzy.service.ClassesService;
import com.zhiyun.hxzy.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin/school")
public class SchoolController {
    @Autowired
    private SchoolService schoolService;
    @Autowired
    private ClassesService classesService;

    @RequestMapping("/all")
    public String all(Model model,@RequestParam(defaultValue = "0") int pageNum, @RequestParam(defaultValue = "5") int pageSize){
        if (pageNum==0){
            pageNum=1;
        }
        model.addAttribute("schools",schoolService.findAll(pageNum-1,pageSize));
        return "./admin/school_list";
    }

    @RequestMapping("/addPre")
    public String addPre(){
        return "school_add";
    }

    @RequestMapping("/updatePre")
    public String updatePre(Model model, Long id){
        model.addAttribute("school",schoolService.findById(id));
        return "./admin/school_update";
    }

    @RequestMapping("/modify")
    public String modify(School school){
        schoolService.modify(school);
        return "redirect:/admin/school/all";
    }

    @RequestMapping("/del")
    public @ResponseBody Result del(Long[] ids){
      return schoolService.del(ids);
    }

    //根据校区找到班级
    @RequestMapping("/findClassBySchool")
    public String findClassBySchool(Model model, Long schoolId){
        List<Classes> classesList = schoolService.findById(schoolId).getClassesList();
        model.addAttribute("classesList",classesList);
        return "./admin/classes_list";
    }

    //根据校区找到班级
    @RequestMapping("/fcbs")
    public @ResponseBody List<Classes> findClassBySchool(Long schoolId){
        return schoolService.findById(schoolId).getClassesList();
    }

    //校区详情
    @RequestMapping("/particulars")
    public  String particulars(Long id,Model model){
        School school = schoolService.findById(id);
        model.addAttribute("school",school);
        return "./admin/school_show";
    }



}
