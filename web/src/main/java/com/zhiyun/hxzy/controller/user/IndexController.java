package com.zhiyun.hxzy.controller.user;

import com.zhiyun.hxzy.domain.*;
import com.zhiyun.hxzy.domain.queryvo.ShouyeHomeworkVO;
import com.zhiyun.hxzy.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/user/index")
@Controller
public class IndexController {
    @Autowired
    private ShouYeDongTaiShuJuService shouYeDongTaiShuJuService;
    @Autowired
    private GoldTeacherService goldTeacherService;
    @Autowired
    private EmploymentService employmentService;
    @Autowired
    private AutoLoginService autoLoginService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private TeacherService teacherService;

    @RequestMapping("/index")
    public String shouYeDongTaiShuJu(ModelMap map, HttpServletRequest request) throws Exception {
        String uuid = null;
        String userAgent = request.getHeader("User-Agent");//客户端浏览器信息
        String ip = request.getRemoteAddr();//客户端ip地址
        uuid = ip + "" + userAgent;
        AutoLogin autoLogin = autoLoginService.find(uuid);

        //首页轮播图
        List<HeaderImg> headerImgs = shouYeDongTaiShuJuService.lunBoTu();
        map.addAttribute("headerImgs", headerImgs);
        //作业类型
        List<Dictionary> dictionaries = shouYeDongTaiShuJuService.homeWorkType();
        map.addAttribute("dictionaries", dictionaries);
        //首页老师
        List<GoldTeacher> teachers = goldTeacherService.findTeachers();
        map.addAttribute("teacherList", teachers.subList(0, 12));
        //就业明星
        List<Employment> star = employmentService.findStar();
        map.addAttribute("studentList", star.subList(0, 24));

        if (autoLogin != null ){
            if (autoLogin.getIdentity() == 0){
                Student student = studentService.findByPhoneNum(autoLogin.getPhoneNum());
                request.getSession().setAttribute("student", student);
            }else {
                Teacher teacher = teacherService.findByPhoneNum(autoLogin.getPhoneNum());
                request.getSession().setAttribute("teacher",teacher);
            }
        }
        return "user/index";
    }

    @RequestMapping("/yibuleixing")
    public @ResponseBody
    List<ShouyeHomeworkVO> leixingyibu(String content) {
        List<ShouyeHomeworkVO> shouyeHomeworkVOS = shouYeDongTaiShuJuService.homeworkList(content);
        return shouyeHomeworkVOS;
    }

}
