package com.zhiyun.hxzy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "hx_comments")
public class Comments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String content;//评论内容
    private Date time;//评论时间
    @Transient
    private String timeStr;//时间描述
    private Long tourId;//评论人Id
    private String name;
    private String header;
    @ManyToOne
    @JoinColumn(name = "homeworkId",referencedColumnName = "id")
    @JsonIgnore
    private Homework homework;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "commentsId",referencedColumnName = "id")
    private List<Answer> answers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(time);
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }

    public Long getTourId() {
        return tourId;
    }

    public void setTourId(Long tourId) {
        this.tourId = tourId;
    }

    public Homework getHomework() {
        return homework;
    }

    public void setHomework(Homework homework) {
        this.homework = homework;
    }

    @Override
    public String toString() {
        return "Comments{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", time=" + time +
                ", timeStr='" + timeStr + '\'' +
                ", tourId=" + tourId +
                ", homework=" + homework +
                '}';
    }
}
