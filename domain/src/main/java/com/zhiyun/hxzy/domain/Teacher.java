package com.zhiyun.hxzy.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zhiyun.hxzy.util.UploadUtil;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "hx_teacher")
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer age;
    private String sex;
    private String name;
    private String email;
    private String phone;
    private String password;
    //教学年龄
    private Integer teachingAge;
    //教授课程
    private String course;
    //照片(路径)1
    private String picture;
    //简介
    private String introduction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "id",name = "schoolId")
    private School school;//教师所在的校区

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "teacherId",referencedColumnName = "id")
    @JsonIgnore
    private List<Homework> homeworks;

    @ManyToMany
    @JoinTable(name = "hx_teacher_classes",
            joinColumns = {@JoinColumn(name = "teacherId", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "classesId", referencedColumnName = "id")}
    )
    @JsonIgnore
    private List<Classes> classesList;

    public Teacher(Integer age, String sex, String name, String email, String phone, String password, Integer teachingAge, String course, String picture, String introduction) {
        this.age = age;
        this.sex = sex;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.teachingAge = teachingAge;
        this.course = course;
        this.picture = picture;
        this.introduction = introduction;
    }

    public Teacher(Integer age, String sex, String name, String email, String phone, String password, Integer teachingAge, String course, String picture, String introduction, School school) {
        this.age = age;
        this.sex = sex;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.teachingAge = teachingAge;
        this.course = course;
        this.picture = picture;
        this.introduction = introduction;
        this.school = school;
    }

    public Teacher() {
    }

    public List<Homework> getHomeworks() {
        return homeworks;
    }

    public void setHomeworks(List<Homework> homeworks) {
        this.homeworks = homeworks;
    }

    public List<Classes> getClassesList() {
        return classesList;
    }

    public void setClassesList(List<Classes> classesList) {
        this.classesList = classesList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getTeachingAge() {
        return teachingAge;
    }

    public void setTeachingAge(Integer teachingAge) {
        this.teachingAge = teachingAge;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getPicture() {
        return  picture == null || picture.length()==0 ? "/user/image/headerImg.jpg":UploadUtil.getLocalPath()+picture;
    }

    public void setPicture(String picture) {
        this.picture = picture.replace(UploadUtil.getLocalPath(),"");
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
