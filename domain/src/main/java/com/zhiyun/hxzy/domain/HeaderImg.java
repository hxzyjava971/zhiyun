package com.zhiyun.hxzy.domain;

import com.zhiyun.hxzy.util.UploadUtil;

import javax.persistence.*;

@Entity
@Table(name = "hx_headerimg") // 顶部轮播图
public class HeaderImg {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String url;//路径
    private String name;
    private Integer status;//状态：开启/关闭（1/0）是顶部轮播图
    @Transient
    private String statusStr;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return UploadUtil.getLocalPath() + url;
    }

    public void setUrl(String url) {
        this.url = url.replace(UploadUtil.getLocalPath(),"");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStatusStr() {
        return status == 0 ? "关闭":"开启";
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }
}
