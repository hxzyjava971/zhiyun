package com.zhiyun.hxzy.domain;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name="hx_log")
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date visitTime;//访问的时间
    @Transient
    private String visitTimeStr;
    private String username;//用户
    private String ip;//访问ip
    private String url;//访问路径
    private Long executionTime;//访问时长
    private String method;//访问的方法

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Date visitTime) {
        this.visitTime = visitTime;
    }

    public String getVisitTimeStr() {
        SimpleDateFormat sdf=new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
        return sdf.format (visitTime);
    }

    public void setVisitTimeStr(String visitTimeStr) {
        this.visitTimeStr = visitTimeStr;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Long executionTime) {
        this.executionTime = executionTime;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public String toString() {
        return "Log{" + "id=" + id + ", visitTime=" + visitTime + ", visitTimeStr='" + visitTimeStr + '\'' + ", username='" + username + '\'' + ", ip='" + ip + '\'' + ", url='" + url + '\'' + ", executionTime=" + executionTime + ", method='" + method + '\'' + '}';
    }
}
