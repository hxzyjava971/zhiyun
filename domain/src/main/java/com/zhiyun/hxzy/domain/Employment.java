package com.zhiyun.hxzy.domain;

import com.zhiyun.hxzy.util.UploadUtil;

import javax.persistence.*;

@Entity
@Table(name = "hx_employment")
public class Employment {//学员就业信息
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name; // 姓名
    private String company;//公司
    private String position;//职位
    private String workDesc;//薪资描述
    private String header;//就业头像
    private boolean star;// 状态 true 开启 false 关闭
    @Transient
    private String starStr;
    private String info; //详情

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isStar() {
        return star;
    }

    public void setStar(boolean star) {
        this.star = star;
    }

    public String getStarStr() {
        return star?"是":"否";
    }

    public void setStarStr(String starStr) {
        this.starStr = starStr;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getWorkDesc() {
        return workDesc;
    }

    public void setWorkDesc(String workDesc) {
        this.workDesc = workDesc;
    }

    public String getHeader() {
        return UploadUtil.getLocalPath()+header;
    }

    public void setHeader(String header) {
        this.header = header.replace(UploadUtil.getLocalPath(),"");
    }

}
