package com.zhiyun.hxzy.domain;

import javax.persistence.*;

@Entity
@Table(name = "hx_autologin")
public class AutoLogin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String uuid;
    private String phoneNum;
    private Integer identity;//身份信息 1.教师    0.学生

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AutoLogin() {
    }

    public AutoLogin(String uuid, String phoneNum) {
        this.uuid = uuid;
        this.phoneNum = phoneNum;
    }

    public AutoLogin(String uuid, String phoneNum, Integer identity) {
        this.uuid = uuid;
        this.phoneNum = phoneNum;
        this.identity = identity;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public Integer getIdentity() {
        return identity;
    }

    public void setIdentity(Integer identity) {
        this.identity = identity;
    }
}
