package com.zhiyun.hxzy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zhiyun.hxzy.util.UploadUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "hx_homework")
@JsonIgnoreProperties(value = { "hibernateLazyInitializer"})
public class Homework implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String workName;//作业名
//    @Column(columnDefinition ="LONGBLOB")
    @Transient
    @JsonIgnore
    private String instruction; // 作业描述

    private String tags;//标签
    private String content;//内容
    private int type;//类型(个人 0/小组 1)
    @Transient
    private String typeStr;
    private int checked;//是否批改(0-未批改/1-已批改)
    @Transient
    private String cherkedStr;
    private Integer score; // 分数
    private Integer star; // 星级
    private String tagEvaluate;// 标签评价
    private String descEvaluate;// 评价详情
    private String coverImg;//封面图片
    private Date time;//作业提交时间
    @Transient
    private String timeStr;//提交时间描述
    private Date commentTime;//作业批改时间
    @Transient
    private String commentTimeStr;//批改时间描述
    private Integer clicks=0; // 点击量
    private Integer thumbsUp=0; // 点赞量
    private Integer collections=0;//收藏量
    @ManyToOne
    @JoinColumn(name = "teacherId",referencedColumnName = "id")
    private Teacher teacher;//老师

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "homeworkId",referencedColumnName = "id")
    private Set<Comments> commentses;//评论

    @ManyToMany(mappedBy = "homeworks")
    private List<Student> students;//学生

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(time);
    }

    public Integer getCollections() {
        return collections==null?0:collections;
    }

    public void setCollections(Integer collections) {
        this.collections = collections;
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public String getCoverImg() {
        return UploadUtil.getLocalPath()+coverImg;
    }

    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg.replace(UploadUtil.getLocalPath(),"");
    }

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

    public String getCherkedStr() {
        return checked == 0 ? "未批改":"已批改";
    }

    public void setCherkedStr(String cherkedStr) {
        this.cherkedStr = cherkedStr;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getInstruction() {
        if(instruction == null){
            return null;
        }
        return instruction.replace("$imgLocalPath/",UploadUtil.getLocalPath());
    }

    public void setInstruction(String instruction) {
        if (instruction != null){
            this.instruction = instruction.replace(UploadUtil.getLocalPath(),"$imgLocalPath/");
        }
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTypeStr() {
        return type == 0 ? "个人作业":"小组作业";
    }

    public void setTypeStr(String typeStr) {
        this.typeStr = typeStr;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getStar() {
        return star;
    }

    public void setStar(Integer star) {
        this.star = star;
    }

    public String getTagEvaluate() {
        return tagEvaluate;
    }

    public void setTagEvaluate(String tagEvaluate) {
        this.tagEvaluate = tagEvaluate;
    }

    public String getDescEvaluate() {
        return descEvaluate;
    }

    public void setDescEvaluate(String descEvaluate) {
        this.descEvaluate = descEvaluate;
    }

    public Set<Comments> getCommentses() {
        return commentses;
    }

    public void setCommentses(Set<Comments> commentses) {
        this.commentses = commentses;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }


    public Date getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }

    public String getCommentTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return commentTime==null?"":sdf.format(commentTime);
    }

    public void setCommentTimeStr(String commentTimeStr) {
        this.commentTimeStr = commentTimeStr;
    }

    public Integer getClicks() {
        return clicks;
    }
    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }
    public Integer getThumbsUp() {
        return thumbsUp;
    }
    public void setThumbsUp(Integer thumbsUp) {
        this.thumbsUp = thumbsUp;
    }
}
