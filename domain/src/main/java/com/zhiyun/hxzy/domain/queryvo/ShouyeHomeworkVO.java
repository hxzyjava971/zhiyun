package com.zhiyun.hxzy.domain.queryvo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zhiyun.hxzy.domain.Homework;
import com.zhiyun.hxzy.domain.Student;
import com.zhiyun.hxzy.domain.Teacher;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(value = { "hibernateLazyInitializer"})
public class ShouyeHomeworkVO implements Serializable {
    private Homework homework;
    private Student student;
    private String clickVolume;//点击量
    private Integer likeNumInt;//点赞数;
    private Integer sumFans;//粉丝数
    private Integer sumHomework;//作品总数
    private Integer size;
    private List<String> biaoqianpingjia;//标签评价

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getSumHomework() {
        return sumHomework;
    }

    public void setSumHomework(Integer sumHomework) {
        this.sumHomework = sumHomework;
    }

    public Homework getHomework() {
        return homework;
    }

    public void setHomework(Homework homework) {
        this.homework = homework;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getClickVolume() {
        return clickVolume;
    }

    public void setClickVolume(String clickVolume) {
        this.clickVolume = clickVolume;
    }

    public Integer getLikeNumInt() {
        return likeNumInt;
    }

    public void setLikeNumInt(Integer likeNumInt) {
        this.likeNumInt = likeNumInt;
    }

    public Integer getSumFans() {
        return sumFans;
    }

    public void setSumFans(Integer sumFans) {
        this.sumFans = sumFans;
    }

    public List<String> getBiaoqianpingjia() {
        return biaoqianpingjia;
    }

    public void setBiaoqianpingjia(List<String> biaoqianpingjia) {
        this.biaoqianpingjia = biaoqianpingjia;
    }

}
