package com.zhiyun.hxzy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "hx_classes")
@JsonIgnoreProperties(value = { "hibernateLazyInitializer"})
public class Classes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String className;//班级名
    private String type;//班级类型
    @DateTimeFormat(pattern="yyyy年MM月dd日")
    private Date startTime;//开班时间
    @Transient
    private  String startTimeStr;
    @DateTimeFormat(pattern="yyyy年MM月dd日")
    private Date endTime;//结业时间
    @Transient
    private String endTimeStr;
    private String headTeacher;//班主任

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(referencedColumnName = "id",name = "schoolId")
    private School school;

    @JsonIgnore
    @ManyToMany(mappedBy = "classesList")
    private List<Teacher> teacherList;

    @OneToMany(mappedBy = "classes")
    @JsonIgnore
    private List<Student> studentList;

    public String getStartTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        return startTime == null?"":sdf.format(startTime);
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    public String getEndTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        return endTime == null?"":sdf.format(endTime);
    }

    public void setEndTimeStr(String endTimeStr) {
        this.endTimeStr = endTimeStr;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public List<Teacher> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<Teacher> teacherList) {
        this.teacherList = teacherList;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getHeadTeacher() {
        return headTeacher;
    }

    public void setHeadTeacher(String headTeacher) {
        this.headTeacher = headTeacher;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public String toString() {
        return "Classes{" +
                "id=" + id +
                ", className='" + className + '\'' +
                ", type='" + type + '\'' +
                ", startTime=" + startTime +
                ", startTimeStr='" + startTimeStr + '\'' +
                ", endTime=" + endTime +
                ", endTimeStr='" + endTimeStr + '\'' +
                ", headTeacher='" + headTeacher + '\'' +
                ", school=" + school +
                ", teacherList=" + teacherList +
                ", studentList=" + studentList +
                '}';
    }
}

