package com.zhiyun.hxzy.domain;

import javax.persistence.*;

@Entity
@Table(name = "hx_homework_instruction")
public class HomeWorkInstruction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(columnDefinition ="LONGTEXT")
    private String instruction; // 作业描述
    private Long homeworkId;    // 作业Id

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public Long getHomeworkId() {
        return homeworkId;
    }

    public void setHomeworkId(Long homeworkId) {
        this.homeworkId = homeworkId;
    }
}
