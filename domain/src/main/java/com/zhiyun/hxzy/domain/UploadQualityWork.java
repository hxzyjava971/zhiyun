package com.zhiyun.hxzy.domain;


import com.zhiyun.hxzy.util.UploadUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "hx_teacher_Uploadqualitywork")
public class UploadQualityWork implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String workName;//作业名
    private String instruction; // 作业描述
    private String type;//作业类型
    private String coverImg;//封面图片
    private Date time;//作业提交时间
    @Transient
    private String timeStr;//提交时间描述
    private String jobdescription; //作业说明

    private Integer clicks = 0; // 点击量
    private Integer thumbsUp = 0; // 点赞量
    private Integer collection = 0;//收藏量

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getInstruction() {
        if (instruction == null) {
            return null;
        }
        return instruction.replace("$imgLocalPath/", UploadUtil.getLocalPath());
    }

    public void setInstruction(String instruction) {
        if (instruction != null) {
            this.instruction = instruction.replace(UploadUtil.getLocalPath(), "$imgLocalPath/");
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJobdescription() {
        return jobdescription;
    }

    public void setJobdescription(String jobdescription) {
        this.jobdescription = jobdescription;
    }

    public String getCoverImg() {
        return UploadUtil.getLocalPath()+coverImg;
    }

    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg.replace(UploadUtil.getLocalPath(),"");
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return time == null ? "" : sdf.format(time);
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }

    public Integer getThumbsUp() {
        return thumbsUp;
    }

    public void setThumbsUp(Integer thumbsUp) {
        this.thumbsUp = thumbsUp;
    }

    public Integer getCollection() {
        return collection;
    }

    public void setCollection(Integer collection) {
        this.collection = collection;
    }
}
