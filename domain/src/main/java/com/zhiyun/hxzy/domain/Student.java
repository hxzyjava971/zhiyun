package com.zhiyun.hxzy.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.zhiyun.hxzy.util.UploadUtil;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "hx_student")
@JsonIgnoreProperties(value = { "hibernateLazyInitializer"})
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;//姓名
    private Integer age;//年龄
    private String sex;//性别
    private String phoneNum;//电话
    private String email;//邮箱
    private String password;//密码
    private String nickName;//昵称
    @Transient
    private String statusStr;//状态描述
    private String header;// 头像
    private String introduction; // 个性签名

    private Integer homeworkLength;//作品数量
    private Integer fans = 0;//粉丝数量



    @ManyToOne
    @JoinColumn(referencedColumnName = "id",name = "classId")
    private Classes classes;// 班级

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "hx_student_homework",
            joinColumns = {@JoinColumn(name = "sid",referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "hid",referencedColumnName = "id")}
    )
    @JsonIgnore
    private List<Homework> homeworks;//作业

    @OneToMany(mappedBy = "student")
    @JsonIgnore
    private List<Collection> collections;


    public List<Collection> getCollections() {
        return collections;
    }

    public void setCollections(List<Collection> collections) {
        this.collections = collections;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public String getHeader() {
        return header == null || header.length()==0 ? "/user/image/headerImg.jpg":UploadUtil.getLocalPath()+header;
    }

    public void setHeader(String header) {
        this.header = header.replace(UploadUtil.getLocalPath(),"");
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }


    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    public List<Homework> getHomeworks() {
        return homeworks;
    }

    public void setHomeworks(List<Homework> homeworks) {
        this.homeworks = homeworks;
    }

    public Integer getHomeworkLength() {
        return homeworks.size();
    }

    public void setHomeworkLength(Integer homeworkLength) {
        this.homeworkLength = homeworkLength;
    }

    public Integer getFans() {
        return fans;
    }

    public void setFans(Integer fans) {
        this.fans = fans;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", phoneNum='" + phoneNum + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", nickName='" + nickName + '\'' +
                ", classes=" + classes +
                ", homeworks=" + homeworks +
                '}';
    }
}
