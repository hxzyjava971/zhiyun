package com.zhiyun.hxzy.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "hx_concern")
public class Concern implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id",name = "followId")
    private Student follow;//关注者

    @ManyToOne
    @JoinColumn(referencedColumnName = "id",name = "toFollowId")
    private Student toFollow;//被关注者

    public Concern() {
    }

    public Concern(Student follow, Student toFollow) {
        this.follow = follow;
        this.toFollow = toFollow;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Student getFollow() {
        return follow;
    }

    public void setFollow(Student follow) {
        this.follow = follow;
    }

    public Student getToFollow() {
        return toFollow;
    }

    public void setToFollow(Student toFollow) {
        this.toFollow = toFollow;
    }
}
