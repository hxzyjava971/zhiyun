package com.zhiyun.hxzy.domain.queryvo;

import com.zhiyun.hxzy.domain.Concern;

public class ConcernVo {
    private Concern concern;
    private boolean status;

    public Concern getConcern() {
        return concern;
    }

    public void setConcern(Concern concern) {
        this.concern = concern;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
