package com.zhiyun.hxzy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "hx_answers")
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;//回复id
    private String answer;//回复内容
    private Date time;//回复时间
    @Transient
    private String timeStr;//时间描述
    private Long tourId;//回复人Id
    private String name;
    private String header;

    @ManyToOne
    @JoinColumn(name = "commentsId",referencedColumnName = "id")
    @JsonIgnore
    private Comments comments;

    public Answer(String answer, Date time, String timeStr, Long tourId, Comments comments) {
        this.answer = answer;
        this.time = time;
        this.timeStr = timeStr;
        this.tourId = tourId;
        this.comments = comments;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Answer() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getTimeStr() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(time);
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }

    public Long getTourId() {
        return tourId;
    }

    public void setTourId(Long tourId) {
        this.tourId = tourId;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }
}
