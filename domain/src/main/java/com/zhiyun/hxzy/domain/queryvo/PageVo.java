package com.zhiyun.hxzy.domain.queryvo;

import com.zhiyun.hxzy.domain.Concern;
import org.springframework.data.domain.Page;

import java.util.List;

public class PageVo {
    private Page<Concern> concernPage;
    private List<ConcernVo> concernVoList;

    public Page<Concern> getConcernPage() {
        return concernPage;
    }

    public void setConcernPage(Page<Concern> concernPage) {
        this.concernPage = concernPage;
    }

    public List<ConcernVo> getConcernVoList() {
        return concernVoList;
    }

    public void setConcernVoList(List<ConcernVo> concernVoList) {
        this.concernVoList = concernVoList;
    }
}
