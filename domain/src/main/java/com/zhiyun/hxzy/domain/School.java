package com.zhiyun.hxzy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

//校区实体类
@Entity
@Table(name = "hx_school")
@JsonIgnoreProperties(value = { "hibernateLazyInitializer"})
public class School {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String schoolName;//校区名字
    private String schoolCity;//校区城市
    @OneToMany(mappedBy = "school")
    @JsonIgnore
    private List<Classes> classesList;//一个校区对应多个班级


    public List<Classes> getClassesList() {
        return classesList;
    }
    public void setClassesList(List<Classes> classesList) {
        this.classesList = classesList;
    }

    public Long getId() {
        return id;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolCity() {
        return schoolCity;
    }

    public void setSchoolCity(String schoolCity) {
        this.schoolCity = schoolCity;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "School{" +
                "id=" + id +
                ", schoolName='" + schoolName + '\'' +
                ", schoolCity='" + schoolCity + '\'' +
                '}';
    }

}
