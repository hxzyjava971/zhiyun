package com.zhiyun.hxzy.domain.queryvo;

public class Result {
    private Object message;
    private boolean result;



    public Result(Object message, boolean result) {
        this.message = message;
        this.result = result;
    }

    public Result() {
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
