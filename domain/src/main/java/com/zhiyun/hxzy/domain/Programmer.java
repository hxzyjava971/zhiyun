package com.zhiyun.hxzy.domain;

import javax.persistence.*;

//程序员
@Entity
@Table(name = "hx_programmer")
public class Programmer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String pname;
    private String pemail;
    private Integer status;     // 用户状态
    @Transient
    private String statusStr;  // 0代表关闭，1代表开启

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPemail() {
        return pemail;
    }

    public void setPemail(String pemail) {
        this.pemail = pemail;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusStr() {
        return status == 0?"禁用":"可用";
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }
}
