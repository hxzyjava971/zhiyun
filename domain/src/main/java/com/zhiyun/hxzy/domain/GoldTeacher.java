package com.zhiyun.hxzy.domain;

import com.zhiyun.hxzy.util.UploadUtil;
import javax.persistence.*;

// 金牌导师
@Entity
@Table(name = "hx_goldTeacher")
public class GoldTeacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String position;// 职位
    private String introduce;// 介绍
    private String headerImg; // 头像
    private String picture; // 介绍照片
    private boolean status;// 状态 true 开启 false 关闭
    @Transient
    private String statusStr;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getHeaderImg() {
        return UploadUtil.getLocalPath()+headerImg;
    }

    public void setHeaderImg(String headerImg) {
        this.headerImg = headerImg.replace(UploadUtil.getLocalPath(),"");
    }

    public String getPicture() {
        return UploadUtil.getLocalPath()+picture;
    }

    public void setPicture(String picture) {
        this.picture = picture.replace(UploadUtil.getLocalPath(),"");
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatusStr() {
        return status?"开启":"关闭";
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }
}
