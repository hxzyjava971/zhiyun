package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.AutoLogin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AutoLoginDao extends JpaRepository<AutoLogin,String> , JpaSpecificationExecutor<AutoLogin> {
    AutoLogin findByUuid(String uuid);

    AutoLogin findByUuidAndPhoneNum(String uuid,String phoneNum);
}
