package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AdminDao extends JpaRepository<Admin,Long>, JpaSpecificationExecutor<Admin> {
    Admin findByUsername(String username);

    @Query(nativeQuery = true,value = "select * from hx_admin where username = ?")
    List<Admin> findByUsername2(String username);// 根据用户名查询用户，新增用户使用

}
