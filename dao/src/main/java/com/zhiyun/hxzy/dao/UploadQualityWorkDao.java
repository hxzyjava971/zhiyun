package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.UploadQualityWork;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UploadQualityWorkDao  extends JpaRepository<UploadQualityWork, Long>, JpaSpecificationExecutor<UploadQualityWork> {


}
