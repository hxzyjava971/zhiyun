package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RoleDao extends JpaRepository<Role,Long>, JpaSpecificationExecutor<Role> {
    @Query(value = "select * from hx_role where id not in(select roleid from hx_admin_role where adminId=?)",nativeQuery = true)
    List<Role> findOtherRole(Long adminId);
}
