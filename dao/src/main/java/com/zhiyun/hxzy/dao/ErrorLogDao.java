package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.ErrorLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ErrorLogDao extends JpaRepository<ErrorLog,Long>, JpaSpecificationExecutor<ErrorLog> {
}
