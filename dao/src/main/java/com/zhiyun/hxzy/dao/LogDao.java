package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.Log;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import java.util.Date;

public interface LogDao extends JpaRepository<Log,Long>, JpaSpecificationExecutor<Log> {

   @Query(nativeQuery = true,value = "select * from hx_log where visitTime between ? and ?")
   Page<Log> findByVisitTimeAfter(Date start,Date end, PageRequest request);

}
