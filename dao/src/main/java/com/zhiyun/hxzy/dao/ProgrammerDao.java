package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.Programmer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProgrammerDao extends JpaRepository<Programmer,Long>, JpaSpecificationExecutor<Programmer> {
//    @Query(value = "select * from hx_programmer where status = 1",nativeQuery = true)
//    List<Programmer> findAllActiveProgramer();
}
