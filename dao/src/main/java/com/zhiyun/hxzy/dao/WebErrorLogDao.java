package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.WebErrorLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface WebErrorLogDao extends JpaRepository<WebErrorLog,Long>, JpaSpecificationExecutor<WebErrorLog> {
}
