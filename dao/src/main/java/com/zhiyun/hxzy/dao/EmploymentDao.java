package com.zhiyun.hxzy.dao;
import com.zhiyun.hxzy.domain.Employment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmploymentDao extends JpaRepository<Employment,Long>, JpaSpecificationExecutor<Employment>{
    @Query(value ="SELECT * FROM hx_employment WHERE star=1",nativeQuery = true)
    List<Employment> findStar();

}
