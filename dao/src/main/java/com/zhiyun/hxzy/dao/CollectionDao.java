package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;



public interface CollectionDao extends JpaSpecificationExecutor<Collection> , JpaRepository<Collection,Long> {
    //根据学生id和作业id查询判断是否已经收藏
    Collection findByStudentIdAndHomeworkId(Long studentId,Long homeworkId);

    //根据学生id进行分页查询
    Page<Collection> findByStudentId(Long studentId, Pageable pageable);


}
