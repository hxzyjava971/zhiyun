package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.HomeWorkInstruction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HomeWorkInstructionDao extends JpaRepository<HomeWorkInstruction, Long> {
    HomeWorkInstruction findByHomeworkId(Long homeworkId);
}
