package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.HeaderImg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface HeaderImgDao extends JpaRepository<HeaderImg,Long>, JpaSpecificationExecutor<HeaderImg> {
    List<HeaderImg> findByStatus(Integer zt);
}
