package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.Answer;
import com.zhiyun.hxzy.domain.Comments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface AnswerDao extends JpaRepository<Answer,Long>, JpaSpecificationExecutor<Answer> {
    @Transactional
    @Query(value = "delete from hx_answers where commentsId = ?", nativeQuery = true)
    @Modifying
    void deleteByCommentsId(long id);
}
