package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.School;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SchoolDao extends JpaRepository<School,Long>, JpaSpecificationExecutor<School> {
}
