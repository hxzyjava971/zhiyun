package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.Comments;
import com.zhiyun.hxzy.domain.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentsDao extends JpaRepository<Comments, Long>, JpaSpecificationExecutor<Comments> {
    Page<Comments> findAllByHomeworkId(Long id, Pageable pageable);
}
