package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.Classes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ClassesDao extends JpaRepository<Classes,Long>, JpaSpecificationExecutor<Classes> {
    @Query(value ="SELECT * FROM hx_classes WHERE schoolId=?2 and id NOT IN(SELECT classesId FROM hx_teacher_classes WHERE teacherId= ?1)", nativeQuery = true)
    List<Classes> findClassesByNotHaveTeacher(Long teacherId,Long schoolId);
    @Query(value ="SELECT * FROM hx_classes WHERE schoolId IN(SELECT id FROM hx_school WHERE schoolName LIKE ?)", nativeQuery = true)
    Page<Classes> findBySchool(String schoolName, PageRequest pageRequest);
    //模糊查询校区
    @Query(value ="SELECT * FROM hx_classes WHERE schoolId =?", nativeQuery = true)
    List<Classes> findBySchool1(Long schoolName);
    @Query(value = "select * from hx_classes where schoolId=?",nativeQuery = true)
    List<Classes> findBySchoolId(Long id);
    Classes findByClassName(String className);


}
