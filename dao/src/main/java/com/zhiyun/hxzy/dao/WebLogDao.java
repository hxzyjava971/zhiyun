package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.WebLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

public interface WebLogDao extends JpaRepository<WebLog,Long>, JpaSpecificationExecutor<WebLog> {
}
