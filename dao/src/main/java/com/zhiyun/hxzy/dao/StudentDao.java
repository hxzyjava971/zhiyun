package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.Homework;
import com.zhiyun.hxzy.domain.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentDao extends JpaRepository<Student,Long>, JpaSpecificationExecutor<Student> {
    @Query(value = "select * from hx_student where classid=?",nativeQuery = true)
    List<Student> findByClassId(Long id);

    @Query(value = "select * from hx_student where classid=?",nativeQuery = true)
    Page<Student> findByClassId(Long id, PageRequest request);
    Student findByPhoneNum(String phoneNum);
    Student findByPhoneNumAndPassword(String phoneNum,String password);
    @Modifying
    @Query(value = "update hx_student set password=? where phoneNum=?",nativeQuery = true)
    void resetPassword(String password,String phoneNum);
    @Query(value = "select * from hx_student WHERE id in (select sid from hx_student_homework where hid=?)", nativeQuery = true)
    List<Student> findByHid(Long hid);//作业id查询学生

    @Query(nativeQuery = true,value = "select * from hx_student where id = (select sid from hx_student_homework where hid = ?1)")
    Student findByHomeworkId(Long homeworkId);
    //添加关注时增加粉丝数量
    @Modifying
    @Query(nativeQuery = true,value = "update hx_student set fans = (fans + 1) where id = ?")
    void addFans(Long id);

    //取消关注时减少粉丝数量
    @Modifying
    @Query(nativeQuery = true,value = "update hx_student set fans = (fans - 1) where id = ?")
    void deleteFans(Long id);
}
