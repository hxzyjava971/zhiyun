package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.Concern;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;


public interface ConcernDao extends JpaRepository<Concern ,Long>, JpaSpecificationExecutor<Concern> {

    //查询关注(关注者)
    @Query(nativeQuery = true,value = "select * from hx_concern where followId = ?")
    Page<Concern> findByFollowId(Long followId, PageRequest request);

    //查询粉丝(被关注者)
    @Query(nativeQuery = true,value = "select * from hx_concern where toFollowId = ?")
    Page<Concern> findByToFollowId(Long toFollowId,PageRequest request);

    //根据关注者和被关注者查询数据
    @Query(nativeQuery = true,value = "select * from hx_concern where followId = ?1 and toFollowId = ?2")
    Concern findByFollowAndToFollow(Long follow,Long toFollow);
}
