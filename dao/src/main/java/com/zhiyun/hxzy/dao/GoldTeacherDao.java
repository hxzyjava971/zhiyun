package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.Employment;
import com.zhiyun.hxzy.domain.GoldTeacher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoldTeacherDao extends JpaRepository<GoldTeacher, Long>, JpaSpecificationExecutor<GoldTeacher> {
    @Query(value = "SELECT * FROM hx_goldTeacher WHERE status=1", nativeQuery = true)
    List<GoldTeacher> findTeachers();
}
