package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.Dictionary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.Nullable;

import java.util.List;

public interface DictionaryDao extends JpaRepository<Dictionary,Long>, JpaSpecificationExecutor<Dictionary> {
    List<Dictionary> findByType(String type);
    Page<Dictionary> findByTypeLike(PageRequest of, String type);


}
