package com.zhiyun.hxzy.dao;

import com.zhiyun.hxzy.domain.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public  interface TeacherDao extends JpaRepository<Teacher,Long>, JpaSpecificationExecutor<Teacher> {
    @Query(value ="SELECT * FROM hx_teacher WHERE id NOT IN(SELECT teacherId FROM hx_teacher_classes WHERE classesid= ?)", nativeQuery = true)
    List<Teacher> findTeacherByNotInClass(Long id);

    Teacher findByPhoneAndPassword(String phone,String password);

    Teacher findByPhone(String phone);

    //通过手机号 修改老师密码
    @Modifying
    @Query(value = "update hx_teacher set password=? where phone=?",nativeQuery = true)
    void resetPassword(String password,String phone);
}
