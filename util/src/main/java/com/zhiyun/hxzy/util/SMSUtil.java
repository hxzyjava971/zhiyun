package com.zhiyun.hxzy.util;

import com.zhenzi.sms.ZhenziSmsClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SMSUtil {
    @Value("${apiUrl}")
    private String apiUrl;
    @Value("${appId}")
    private String appId;
    @Value("${appSecret}")
    private String appSecret;

    public String sendMessage(String number,String message) throws Exception {
        ZhenziSmsClient client = new ZhenziSmsClient(apiUrl, appId, appSecret);
        Map<String, String> params = new HashMap<String, String>();
        params.put("message", message);
        params.put("number", number);
        return client.send(params);
    }
}
