package com.zhiyun.hxzy.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;

public class Base64ToMultipartUtil {
    public static MultipartFile base64ToMultipart(String base64) {
        String[] baseStrs = base64.split(",");
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] b = new byte[0];
        b = decoder.decode(baseStrs[1]);
        for(int i = 0; i < b.length; ++i) {
            if (b[i] < 0) {
                b[i] += 256;
            }
        }
        return new BASE64DecodedMultipartFile(b, baseStrs[0]);
    }
}
