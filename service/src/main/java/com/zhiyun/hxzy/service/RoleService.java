package com.zhiyun.hxzy.service;

import com.zhiyun.hxzy.dao.RoleDao;
import com.zhiyun.hxzy.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RoleService {
    @Autowired
    private RoleDao roleDao;

    public List<Role> findAll(){
        return roleDao.findAll();
    }
    public Page<Role> findAll(int pageNum,int pageSize){
        Pageable pageable =PageRequest.of (pageNum-1,pageSize);
        Page<Role> all = roleDao.findAll (pageable);
        return all;
    }

    public void add(Role role){
        roleDao.save (role);
    }

    public Role findId(Long id){
        Role one = roleDao.getOne (id);
        return one;
    }

    public void update(Role role){
        roleDao.save (role);
    }


}
