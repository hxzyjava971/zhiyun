package com.zhiyun.hxzy.service;

import com.zhiyun.hxzy.dao.UploadQualityWorkDao;
import com.zhiyun.hxzy.domain.UploadQualityWork;
import com.zhiyun.hxzy.domain.queryvo.Result;
import com.zhiyun.hxzy.util.Base64ToMultipartUtil;
import com.zhiyun.hxzy.util.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

@Service
public class TeacherUploadQualityWorkService {
    @Autowired
    private UploadQualityWorkDao uploadQualityWorkDao;

    public void add(UploadQualityWork uploadQualityWork) {
        uploadQualityWork.setTime(new Date());
        uploadQualityWorkDao.save(uploadQualityWork);
    }
}
