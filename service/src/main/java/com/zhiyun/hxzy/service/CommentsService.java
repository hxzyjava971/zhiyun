package com.zhiyun.hxzy.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zhiyun.hxzy.dao.AnswerDao;
import com.zhiyun.hxzy.dao.CommentsDao;
import com.zhiyun.hxzy.dao.HomeWorkDao;
import com.zhiyun.hxzy.domain.*;
import com.zhiyun.hxzy.domain.queryvo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CommentsService {
    @Autowired
    CommentsDao commentsDao;
    @Autowired
    HomeWorkDao homeWorkDao;
    @Autowired
    AnswerDao answerDao;

    public Page<Comments> findAll(long hid,int pageNum, int pageSize) {
        pageNum -=1;
        if (pageNum < 0) {
            pageNum = 0;
        }
        Pageable pageable = PageRequest.of(pageNum, pageSize, Sort.by(Sort.Direction.DESC, "time"));
        Page<Comments> comments = commentsDao.findAllByHomeworkId(hid,pageable);
        return comments;
    }

    public Result addComments(Long hid, Long id,String name,String header,String content) throws JsonProcessingException {
        try {
            Date date = new Date();
            Comments comments = new Comments();
            comments.setContent(content);
            comments.setTime(date);
            comments.setTourId(id);
            comments.setName(name);
            comments.setHeader(header);
            Homework homework = homeWorkDao.findById(hid).get();
            comments.setHomework(homework);
            commentsDao.save(comments);
            return new Result("评论成功", true);
        } catch (Exception e) {
            return new Result("评论失败，请重试", false);
        }
    }
    public Result addAnswers(Long cid,Long id,String name,String header, String content) {
        try {
            Date date = new Date();
            Answer answer = new Answer();
            answer.setAnswer(content);
            answer.setTime(date);
            answer.setTourId(id);
            answer.setName(name);
            answer.setHeader(header);
            Comments comments1 = commentsDao.findById(cid).get();
            answer.setComments(comments1);
            answerDao.save(answer);
            return new Result("评论成功", true);
        } catch (Exception e) {
            return new Result("评论失败，请重试", false);
        }
    }
    public Result delComments(long id) {
        try{
            answerDao.deleteByCommentsId(id);
            commentsDao.deleteById(id);
            return new Result("删除成功",true);
        }catch (Exception e){
            return new Result("删除失败请重试",true);
        }
    }

    public Result delAnswers(long id) {
        try {
            answerDao.deleteById(id);
            return new Result("删除成功",true);
        }catch (Exception e){
            return new Result("删除失败请重试",true);
        }
    }
}
