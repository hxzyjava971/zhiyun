package com.zhiyun.hxzy.service;

import com.zhiyun.hxzy.dao.AutoLoginDao;
import com.zhiyun.hxzy.domain.AutoLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AutoLoginService {
    @Autowired
    private AutoLoginDao autoLoginDao;

    //添加
    public void add(AutoLogin autoLogin){
        autoLoginDao.save(autoLogin);
    }

    //查询
    public AutoLogin find(String uuid){
        return autoLoginDao.findByUuid(uuid);
    }

    //删除
    public void delete(AutoLogin autoLogin){
        AutoLogin login = autoLoginDao.findByUuidAndPhoneNum(autoLogin.getUuid(), autoLogin.getPhoneNum());
        if (login != null){
            autoLoginDao.delete(login);
        }
    }
}
