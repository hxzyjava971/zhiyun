package com.zhiyun.hxzy.service;

import com.zhiyun.hxzy.dao.GoldTeacherDao;
import com.zhiyun.hxzy.domain.ErrorLog;
import com.zhiyun.hxzy.domain.GoldTeacher;
import com.zhiyun.hxzy.domain.queryvo.Result;
import com.zhiyun.hxzy.util.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@Transactional
public class GoldTeacherService {
    @Autowired
    private GoldTeacherDao goldTeacherDao;
    @Autowired
    private UploadUtil uploadUtil;
    // 新增
    public void add(GoldTeacher goldTeacher){
        goldTeacherDao.save(goldTeacher);
    }
    // 删除
    public void del(Long id){
        goldTeacherDao.delete(goldTeacherDao.getOne(id));
    }
    // 修改状态
    public void updateStatus(Long id) {
        GoldTeacher goldTeacher = goldTeacherDao.getOne(id);
        goldTeacher.setStatus(!goldTeacher.isStatus());
    }

    // 根据id查询
    public GoldTeacher findOne(Long id) {
        return goldTeacherDao.getOne(id);
    }

    // 查询所有
    public Page<GoldTeacher> findAll(int pageNum, int pageSize){
        if(pageNum<0){
            pageNum=0;
        }
        Pageable pageable= PageRequest.of (pageNum,pageSize, Sort.by(Sort.Direction.DESC, "id"));
        return goldTeacherDao.findAll (pageable);
    }

    // 上传
    public Result upload(MultipartFile file) {
        String path = uploadUtil.upload(file);
        return new Result(path,true);
    }

    // 查询所有有效老师
    public List<GoldTeacher> findTeachers(){
        return goldTeacherDao.findTeachers();
    }


}
