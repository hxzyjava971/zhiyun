package com.zhiyun.hxzy.service;

import com.zhiyun.hxzy.dao.ErrorLogDao;
import com.zhiyun.hxzy.dao.LogDao;
import com.zhiyun.hxzy.dao.WebErrorLogDao;
import com.zhiyun.hxzy.dao.WebLogDao;
import com.zhiyun.hxzy.domain.Log;
import com.zhiyun.hxzy.domain.WebLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@Service
@Transactional
public class WebLogService {
    @Autowired
    private WebLogDao webLogDao;
    @Autowired
    private WebErrorLogDao errorLogDao;

    public Page<WebLog> findAll(int pageNum, int pageSize,String visitTime){
        if(pageNum<0){
            pageNum=0;
        }
        Pageable pageable= PageRequest.of (pageNum,pageSize, Sort.by(Sort.Direction.DESC,"id"));
        Specification<WebLog> specification =  new Specification<WebLog>() {
            @Override
            public Predicate toPredicate(Root<WebLog> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                //Predicate predicate = criteriaBuilder.like(root.get("username").as(String.class), "%"+username+"%");
                Predicate predicate2 = criteriaBuilder.like(root.get("visitTime").as(String.class), "%"+visitTime+"%");
                //Predicate allPredicat = criteriaBuilder.and(predicate, predicate2);
                return predicate2;
            }
        };
        Page<WebLog> webLogs = webLogDao.findAll(specification,pageable);
        return webLogs;
    }


}
