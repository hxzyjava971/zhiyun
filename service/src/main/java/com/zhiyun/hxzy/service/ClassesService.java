package com.zhiyun.hxzy.service;
import com.zhiyun.hxzy.dao.ClassesDao;
import com.zhiyun.hxzy.dao.DictionaryDao;
import com.zhiyun.hxzy.dao.SchoolDao;
import com.zhiyun.hxzy.dao.TeacherDao;
import com.zhiyun.hxzy.domain.*;
import com.zhiyun.hxzy.domain.queryvo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ClassesService {
    @Autowired
    private ClassesDao classesDao;
    @Autowired
    private TeacherDao teacherDao;
    @Autowired
    private SchoolDao schoolDao;
    @Autowired
    private DictionaryDao dictionaryDao;

    //添加/修改班级
    public void modify(Classes classes) {
        Classes classes1 = classesDao.getOne(classes.getId());
        School school = classes1.getSchool();
        List<Student> studentList = classes1.getStudentList();
        List<Teacher> teacherList = classes1.getTeacherList();
        classes.setSchool(school);
        classes.setStudentList(studentList);
        classes.setTeacherList(teacherList);
        classesDao.save(classes);
    }

    //删除班级

    public Result del(Long[] id) {
        Result result = new Result();
        try {
            for (Long i:id) {
                classesDao.deleteById(i);
            }
            result.setResult(true);
            return result;
        }catch (Exception e){
            result.setResult(false);
            result.setMessage("删除失败！所选班级还有教师或学生，请核对！");
            return result;
        }
    }

    //查询所有班级
    public Page<Classes> findAll(int pageNum, int pageSize) {
        PageRequest pageable = PageRequest.of(pageNum, pageSize, Sort.Direction.DESC,"id");
        Page<Classes> classesPage = classesDao.findAll(pageable);
        return classesPage;
    }

    public List<Classes> findAll(Long id) {
        return classesDao.findBySchoolId(id);
    }

    //根据id查询
    public Classes findById(Long id) {
        Classes classes = classesDao.findById(id).get();
        return classes;
    }

    //给班级添加老师
    public void addTeacher(Long classesId, Long[] teacherId) {
        for (Long id : teacherId) {
            Teacher teacher = teacherDao.findById(id).get();
            Classes classes = classesDao.findById(classesId).get();
            teacher.getClassesList().add(classes);
            teacherDao.save(teacher);
        }
    }

    //根据id查询班级没有的老师
    public List<Teacher> findByNotTeacher(Long classesId) {
        List<Teacher> teacherList = teacherDao.findTeacherByNotInClass(classesId);
        return teacherList;
    }

    //根据id查询老师没有的班级
    public List<Classes> findClassByNotHaveTeacher(Long teacherId,Long schoolId) {
        return classesDao.findClassesByNotHaveTeacher(teacherId,schoolId);

    }

    public List<School> findAllSchool() {
        List<School> schools = schoolDao.findAll();
        return schools;
    }

    public List<Dictionary> findAllDictionary(String type) {
        type = "课程";
        List<Dictionary> dictionaries = dictionaryDao.findByType(type);
        return dictionaries;
    }

    public void add(Classes classes, Long schoolId) {
        School school = schoolDao.findById(schoolId).get();
        classes.setSchool(school);
        classesDao.save(classes);
    }

    public Page<Classes> findBySchool(String schoolName, int pageNum, int  pageSize) {
        PageRequest pageable = PageRequest.of(pageNum, pageSize, Sort.Direction.DESC,"id");
        if(schoolName == null || schoolName.length() == 0){
            return findAll(pageNum,pageSize);
        }
        Page<Classes> classesList = classesDao.findBySchool("%"+schoolName+"%", pageable);
        System.out.println(schoolName);
        return classesList;
    }

    public List<Classes> findBySchool(Long schoolName) {
        List<Classes> classes = classesDao.findBySchool1(schoolName);
        return classes;
    }

    public List<Student> findStudentByClassName(String className) {
        Classes classes = classesDao.findByClassName(className);
        return classes.getStudentList();
    }

    public void delTeacher(Long teacherId,Long classesId){
        Teacher teacher = teacherDao.findById(teacherId).get();
        Classes classes = classesDao.findById(classesId).get();
        teacher.getClassesList().remove(classes);
        teacherDao.save(teacher);
    }



}
