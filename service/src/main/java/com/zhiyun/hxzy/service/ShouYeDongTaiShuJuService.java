package com.zhiyun.hxzy.service;

import com.zhiyun.hxzy.dao.*;
import com.zhiyun.hxzy.domain.*;
import com.zhiyun.hxzy.domain.queryvo.ShouyeHomeworkVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ShouYeDongTaiShuJuService {
    @Autowired
    private HeaderImgDao headerImgDao;
    @Autowired
    private DictionaryDao dictionaryDao;
    @Autowired
    private HomeWorkDao homeWorkDao;
    @Autowired
    private StudentDao studentDao;

    public List<HeaderImg> lunBoTu() {
        //轮播图
        return headerImgDao.findByStatus(1);
    }

    public List<Dictionary> homeWorkType() {
        //首页作业类型标签
        return dictionaryDao.findByType("内容");
    }

    public List<ShouyeHomeworkVO> homeworkList(String content) {
        //首页作业详情
        List<ShouyeHomeworkVO> list = new ArrayList<>();
        List<Homework> byTags = homeWorkDao.findByContent(content);
        for (Homework bytagss : byTags) {
            if (bytagss.getChecked() == 1 && bytagss.getScore() >= 80 && bytagss.getStudents().size() == 1) {
                ShouyeHomeworkVO shouyeHomeworkVO = new ShouyeHomeworkVO();
                shouyeHomeworkVO.setHomework(bytagss);//作业对象
                List<Student> students = studentDao.findByHid(bytagss.getId());
                if(students.size()>0){
                    shouyeHomeworkVO.setStudent(students.get(0));
                }
                List<Homework> homeworks = homeWorkDao.findByStudentId(students.get(0).getId());
                if(homeworks.size()>0) {
                    shouyeHomeworkVO.setSumHomework(homeworks.size());//作品总数
                }
                list.add(shouyeHomeworkVO);
            }
        }
        return list;
    }

}
