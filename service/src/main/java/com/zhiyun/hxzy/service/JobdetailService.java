package com.zhiyun.hxzy.service;

import com.zhiyun.hxzy.dao.HomeWorkDao;
import com.zhiyun.hxzy.dao.HomeWorkInstructionDao;
import com.zhiyun.hxzy.dao.StudentDao;
import com.zhiyun.hxzy.domain.HomeWorkInstruction;
import com.zhiyun.hxzy.domain.Homework;
import com.zhiyun.hxzy.domain.Student;
import com.zhiyun.hxzy.domain.Teacher;
import com.zhiyun.hxzy.domain.queryvo.ShouyeHomeworkVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class JobdetailService {
    @Autowired
    private HomeWorkDao homeWorkDao;
    @Autowired
    private StudentDao studentDao;
    @Autowired
    private HomeWorkInstructionDao homeWorkInstructionDao;

    // 点击量+1
    public void addClicks(Long id){
        Homework homework = homeWorkDao.getOne(id);
        homework.setClicks(homework.getClicks()+1);
        homeWorkDao.save(homework);
    }

    // 修改点赞
    public void thumbsUp(Long id,int num){
        Homework homework = homeWorkDao.getOne(id);
        homework.setThumbsUp(homework.getThumbsUp()+num);
        homeWorkDao.save(homework);
    }
    // 获得点赞数
    public int findThumbsUp(Long id){
        return homeWorkDao.getOne(id).getThumbsUp();
    }


    public ShouyeHomeworkVO findByStudentmessage(Long id){
        //根据作业id查询学生信息和老师批改作业的信息
        ShouyeHomeworkVO shouyeHomeworkVO=new ShouyeHomeworkVO();
        Homework homework = homeWorkDao.getOne(id);
        HomeWorkInstruction homeWorkInstruction = homeWorkInstructionDao.findByHomeworkId(id);
        homework.setInstruction(homeWorkInstruction.getInstruction());

        Teacher teacher = homework.getTeacher();
        List<Student> students = studentDao.findByHid(id);
        Student student = students.get(0);
        String[] split = homework.getTagEvaluate().split(",");
        List<String> biaoqianpingjia = new ArrayList<>();
        for (String str:split){
            if(str!=null && str.trim().length()!=0 ){
                biaoqianpingjia.add(str);
            }
        }
        shouyeHomeworkVO.setBiaoqianpingjia(biaoqianpingjia);
        shouyeHomeworkVO.setHomework(homework);
        shouyeHomeworkVO.setStudent(student);
        return shouyeHomeworkVO;
    }

    public List<Homework> findOtherTask(Long id){
        Homework homework = homeWorkDao.getOne(id);
        Student student = homework.getStudents().get(0);
        List<Homework> homeworks = student.getHomeworks();
        Collections.shuffle(homeworks);
        homeworks.remove(homework);
        return homeworks;
    }

    public ShouyeHomeworkVO findXiaYe(Long studenId,Long homeworkId ){
        //根据作业id
        ShouyeHomeworkVO shouyeHomeworkVO=new ShouyeHomeworkVO();
        Student student = studentDao.findById(studenId).get();
        Homework homework = homeWorkDao.findById(homeworkId).get();
        List<Homework> homeworks = student.getHomeworks();
        int i = homeworks.indexOf(homework);
        if((i+1)==homeworks.size()) {
            shouyeHomeworkVO.setSize(1);
        }
        if(i+1<homeworks.size()){
            shouyeHomeworkVO.setHomework(homeworks.get(i+1));
        }else{
            shouyeHomeworkVO.setHomework(homeworks.get(i));
        }
        shouyeHomeworkVO.setStudent(student);
        return shouyeHomeworkVO;

    }
    public  ShouyeHomeworkVO findShangYe (Long studenId,Long homeworkId ){
        ShouyeHomeworkVO shouyeHomeworkVO=new ShouyeHomeworkVO();
        Student student = studentDao.findById(studenId).get();
        Homework homework = homeWorkDao.findById(homeworkId).get();
        List<Homework> homeworks = student.getHomeworks();
        int i = homeworks.indexOf(homework);
        if((i-1)==0){
            shouyeHomeworkVO.setSize(0);
        }
        if(i-1>0){
            shouyeHomeworkVO.setHomework(homeworks.get(i-1));
        }else{
            shouyeHomeworkVO.setHomework(homeworks.get(i));
        }
        shouyeHomeworkVO.setStudent(student);
        return shouyeHomeworkVO;


    }





}
