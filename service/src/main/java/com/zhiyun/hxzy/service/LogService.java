package com.zhiyun.hxzy.service;

import com.zhiyun.hxzy.dao.ErrorLogDao;
import com.zhiyun.hxzy.dao.LogDao;
import com.zhiyun.hxzy.domain.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class LogService {
    @Autowired
    private LogDao logDao;
    @Autowired
    private ErrorLogDao errorLogDao;

    public Page<Log> findAll(int pageNum, int pageSize) {
        if (pageNum < 0) {
            pageNum = 0;
        }
        Pageable pageable = PageRequest.of(pageNum, pageSize,Sort.Direction.DESC, "id");
        Page<Log> Logs = logDao.findAll(pageable);
        return Logs;
    }

    public Page<Log> search(String startTime, int pageNum, int pageSize) throws ParseException {
        if (pageNum < 0) {
            pageNum = 0;
        }
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date start = simpleDateFormat.parse(startTime);// 今天0点
        long endTime = start.getTime() + (1000*60*60*24);
        Date end = new Date(endTime);// 第二天0点
        PageRequest pageable = PageRequest.of(pageNum, pageSize);
        Page<Log> page = logDao.findByVisitTimeAfter(start,end, pageable);
        return page;
    }

    public void logAdd(Log log) {
        logDao.save(log);
    }

}
