package com.zhiyun.hxzy.service;

import com.zhiyun.hxzy.dao.WebErrorLogDao;
import com.zhiyun.hxzy.domain.ErrorLog;
import com.zhiyun.hxzy.domain.WebErrorLog;
import com.zhiyun.hxzy.domain.WebLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Optional;

@Service
@Transactional
public class WebErrorLogService {
    @Autowired
    private WebErrorLogDao errorLogDao;

    public void addErrorLog(WebErrorLog errorLog){
        errorLogDao.save(errorLog);
    }
    public WebErrorLog findById(Long id){
        Optional<WebErrorLog> byId = errorLogDao.findById(id);
        return byId.get();
    }
    public Page<WebErrorLog> findAll(int pageNum, int pageSize,String visitTime){
        if(pageNum<0){
            pageNum=0;
        }
        Pageable pageable= PageRequest.of (pageNum,pageSize, Sort.by(Sort.Direction.DESC,"id"));
        Specification<WebErrorLog> specification =  new Specification<WebErrorLog>() {
            @Override
            public Predicate toPredicate(Root<WebErrorLog> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                //Predicate predicate = criteriaBuilder.like(root.get("username").as(String.class), "%"+username+"%");
                Predicate predicate2 = criteriaBuilder.like(root.get("visitTime").as(String.class), "%"+visitTime+"%");
                //Predicate allPredicat = criteriaBuilder.and(predicate, predicate2);
                return predicate2;
            }
        };
        Page<WebErrorLog> errorLogs=errorLogDao.findAll (specification,pageable);
        return errorLogs;
    }
}
