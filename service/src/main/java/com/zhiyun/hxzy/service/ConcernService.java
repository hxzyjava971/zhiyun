package com.zhiyun.hxzy.service;

import com.zhiyun.hxzy.dao.ConcernDao;
import com.zhiyun.hxzy.domain.Concern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ConcernService {
    @Autowired
    private ConcernDao concernDao;

    //添加
    public void add(Concern concern){
        concernDao.save(concern);
    }

    //删除
    public void delete(Long follow,Long toFollow){
        Concern concern = concernDao.findByFollowAndToFollow(follow, toFollow);
        concernDao.delete(concern);
    }

    //判断是否关注
    public boolean judge(Long follow,Long toFollow){
        Concern concern = concernDao.findByFollowAndToFollow(follow, toFollow);
        if (concern != null){
            return true;
        }else {
            return false;
        }
    }

    //查询关注(关注列)
    public Page<Concern> follow(Long follow,int pageNum,int pageSize){
        return concernDao.findByFollowId(follow, PageRequest.of(pageNum-1,pageSize));
    }

    //查询粉丝(被关注列)
    public Page<Concern> toFollow(Long toFollow,int pageNum,int pageSize){
        return concernDao.findByToFollowId(toFollow,PageRequest.of(pageNum-1,pageSize));
    }
}
