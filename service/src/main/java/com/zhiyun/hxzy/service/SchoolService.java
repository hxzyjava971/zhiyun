package com.zhiyun.hxzy.service;

import com.zhiyun.hxzy.dao.ClassesDao;
import com.zhiyun.hxzy.dao.SchoolDao;
import com.zhiyun.hxzy.domain.School;
import com.zhiyun.hxzy.domain.queryvo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SchoolService {
    @Autowired
    private SchoolDao schoolDao;
    @Autowired
    private ClassesDao classesDao;
    public Page<School> findAll(int pageNum,int pageSize){

        PageRequest pageable=PageRequest.of(pageNum,pageSize, Sort.Direction.DESC,"id");
        return schoolDao.findAll(pageable);
    }
    public List<School> findAll(){
        return schoolDao.findAll();
    }

    public School findById(Long id){
        return schoolDao.findById(id).get();
    }

    public void modify(School school){
        schoolDao.save(school);
    }
    //删除校区
    public Result del(Long[] id){
        Result result = new Result();
        try {
            for (Long i:id) {
                System.out.println(i);
                schoolDao.deleteById(i);
            }
            result.setResult(true);
        }catch (Exception e){
            result.setResult(false);
            result.setMessage("删除失败！所选校区还有班级！");
        }
        return result;
    }
}
