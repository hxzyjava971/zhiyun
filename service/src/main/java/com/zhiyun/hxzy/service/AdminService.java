package com.zhiyun.hxzy.service;

import com.zhiyun.hxzy.dao.AdminDao;
import com.zhiyun.hxzy.dao.RoleDao;
import com.zhiyun.hxzy.domain.Admin;
import com.zhiyun.hxzy.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AdminService implements UserDetailsService {
    @Autowired
    private AdminDao adminDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Admin admin = adminDao.findByUsername(username);
        List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
        List<Role> roles = admin.getRoles();
        for (Role role:roles){
            grantedAuthorityList.add(new SimpleGrantedAuthority(role.getRoleName()));
        }
        User user = new User(username, admin.getPassword(),
                admin.getStatus() == 0 ? false:true,
                true,
                true,
                true, grantedAuthorityList);
        return user;
    }

    public Page<Admin> findAll(int pageNum, int pageSize) {
        if (pageNum < 0) {
            pageNum = 0;
        }
        Pageable pageable = PageRequest.of(pageNum, pageSize);
        Page<Admin> all = adminDao.findAll(pageable);
        return all;
    }

    public void add(Admin admin) {
        String encode = passwordEncoder.encode(admin.getPassword());
        admin.setPassword(encode);
        adminDao.save(admin);
    }

    public Admin getOne(Long id) {
        Admin admin = adminDao.getOne(id);
        return admin;
    }

    public void update(Admin admin,Long[] roleIds) {
        Admin one = adminDao.getOne(admin.getId());
        one.setEmail(admin.getEmail());
        one.setPhoneNum(admin.getPhoneNum());
        one.setStatus(admin.getStatus());
        one.setUsername(admin.getUsername());
        ArrayList<Role> roles = new ArrayList<>();
        for(Long roleId:roleIds){
            Role role = roleDao.getOne(roleId);
            roles.add(role);
        }
        one.setRoles(roles);
        adminDao.save(one);
    }

    public void delete(Long[] ids, Integer status) {
        for (Long idss : ids) {
            Admin one = adminDao.getOne(idss);
            one.setStatus(status);
            adminDao.save(one);
        }
    }

    public List<Role> findOtherRole(Long id) {
        List<Role> otherRole = roleDao.findOtherRole(id);
        return otherRole;
    }

    public void addOtherRole(Long adminid, Long[] ids) {
        for (Long idss : ids) {
            Role role = roleDao.getOne(idss);
            Admin admin = adminDao.getOne(adminid);
            admin.getRoles().add(role);
            adminDao.save(admin);
        }
    }

    public Admin adminDetail(Long id) {
        return adminDao.getOne(id);
    }

    public void repassword(Long id,String password){
        Admin admin = adminDao.getOne(id);
        String encode = passwordEncoder.encode(password);
        admin.setPassword(encode);
        adminDao.save(admin);
    }

    public void repassword(String username,String password){
        Admin admin = adminDao.findByUsername(username);
        String encode = passwordEncoder.encode(password);
        admin.setPassword(encode);
        adminDao.save(admin);
    }

    //按用户名查询是否有这个用户
    public boolean judgmentUserName(String userName){
        List<Admin> admins = adminDao.findByUsername2(userName);
        if (admins.size() == 0){
            return true;
        }else {
            return false;
        }
    }
}
