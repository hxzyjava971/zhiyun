package com.zhiyun.hxzy.service;

import com.zhiyun.hxzy.dao.EmploymentDao;
import com.zhiyun.hxzy.domain.Employment;
import com.zhiyun.hxzy.domain.queryvo.Result;
import com.zhiyun.hxzy.util.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

@Service
@Transactional
public class EmploymentService {
    @Autowired
    private EmploymentDao employmentDao;
    @Autowired
    private UploadUtil uploadUtil;

    //查询所有就业人信息
    public Page<Employment> findAll(int pageNum, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNum, pageSize, Sort.by(Sort.Direction.DESC, "id"));
        return employmentDao.findAll(pageRequest);
    }

    //根据id查询就业人信息
    public Employment getOne(Long id) {
        return employmentDao.getOne(id);
    }

    //图片上传
    public Result updateUimg(MultipartFile file) {
        String path = uploadUtil.upload(file);
        return new Result(path, true);
    }

    //新增就业信息
    public void employmentAdd(Employment employment) {
        employmentDao.save(employment);
    }

    //根据id删除就业人信息
    public void delById(Long id) {
        employmentDao.deleteById(id);
    }

    //就业信息修改
    public void update(Employment employment) {
        Employment oldEmployment = employmentDao.getOne(employment.getId());
        employment.setStar(oldEmployment.isStar());
        employmentDao.save(employment);
    }

    //修改开启关闭状态
    public void employmentStar(Long id) {
        Employment employment = employmentDao.getOne(id);
        employment.setStar(!employment.isStar());
    }

    //查询有效就业人信息
    public List<Employment> findStar() {
        return employmentDao.findStar();
    }
}
